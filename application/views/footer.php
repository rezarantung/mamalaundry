<div class="clearfix"></div><br><br>
<footer class="row">
        <p class="col-md-9 col-sm-9 col-xs-12 copyright">Copyright &copy; 2017 <a href="https://www.facebook.com/reza.rantung" target="_blank">Echa</a>. All rights reserved</p>

        <p class="col-md-3 col-sm-3 col-xs-12 powered-by">Version 1.0</p>
    </footer>

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?php echo base_url(); ?>assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js'></script>
<script src='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="<?php echo base_url(); ?>assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?php echo base_url(); ?>assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?php echo base_url(); ?>assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?php echo base_url(); ?>assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?php echo base_url(); ?>assets/js/jquery.history.js"></script> 
<!-- application script for Charisma demo -->
<script src="<?php echo base_url(); ?>assets/js/charisma.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap.css">
 	<link href="<?php echo base_url(); ?>assets/css/dataTables.responsive.css" rel="stylesheet" type="text/css" />
 	
	<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/dataTables.responsive.min.js" type="text/javascript"></script>
	
	<!-- <link href='<?php echo base_url(); ?>assets/css/jquery.alerts.css' rel='stylesheet'>
	<script src="<?php echo base_url(); ?>assets/js/jquery.alerts.js"></script> -->
	
	<!-- <link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script> -->
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.datetimepicker.css"/ >
	<script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.full.min.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/js/jspdf.debug.js"></script>
	
    <script src="<?php echo base_url(); ?>assets/js/jspdf.plugin.autotable.js"></script>
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/PrintArea.css"/ >
	<script src="<?php echo base_url(); ?>assets/js/jquery.PrintArea.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/js/jquery.number.js"></script>
</body>
</html>
