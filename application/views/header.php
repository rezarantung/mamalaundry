<?php 
 	if (!$this->session->userdata('id_user')) {
        redirect('login');
	}
	
	// $name = $this->session->userdata('name');
	// $occup = $this->session->userdata('occupation');
	// $phone = $this->session->userdata('phone');
	//$role = $this->session->userdata('role');
	switch ($this->session->userdata('role')) {
		case 0:
			$role = 'Admin';
			break;
		case 1:
			$role = 'Kasir';
			break;
		case 2:
			$role = 'Pegawai';
			break;	
	} 
    $user = $this->session->userdata('user');
	// $token_name = $this->security->get_csrf_token_name();
	// $token_hash = $this->security->get_csrf_hash();
	
?>
<script>

	onInactive(300000, function () {
	    $('#inactive_warning').modal('toggle');
	}, 600000, function () {
	    $.ajax({
			type: 'post',
			url: "auth/do_logout",
			success: function() {
				location.reload(true);
			}
	    });
	});
	
	function onInactive(warning_ms, waring, logout_ms, logout) {
	
	    var wait = setTimeout(logout, logout_ms);
	    var warning = setTimeout(waring, warning_ms);
	
	    document.onmousemove = document.mousedown = document.mouseup = document.onkeydown = document.onkeyup = document.focus = function () {
	        clearTimeout(wait);
	        clearTimeout(warning);
	        wait = setTimeout(logout, logout_ms);
	        warning = setTimeout(waring, warning_ms);
	    };
	}
	
	function change() {
		var r = confirm("Are you sure want to change your password?","Ok","Cancel");
		if(r){
			var old_password = $('#change_old_password').val();
			var nw_password = $('#change_new_password').val();
			var rw_password = $('#change_rw_password').val();
			var id = <?php echo $this->session->userdata('id_user'); ?>;
			
			if(nw_password != rw_password) {
				$('#change_new_password').val('');
				$('#change_rw_password').val('');
				
				$("#modal_change_pwd_message").html('Password confirm doesn\'t match!');
				$("#modal_change_pwd_message").show();
				setTimeout(function() { $("#modal_change_pwd_message").slideUp(); }, 4000);
			} else {
				$.ajax({
					type: 'post',
					url: "auth/change_password", 
					data: {
						'old_pass': old_password,
						'new_pass': nw_password,
						'id': id
					}, 
					success: function(response){
			        	if(response.success) {
			        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Change Password');
			        		
			        		$('#change_old_password').val('');
							$('#change_new_password').val('');
							$('#change_rw_password').val('');
							
			        		$("#modal_change_pwd_message_success").html(response.message);
							$("#modal_change_pwd_message_success").show();
							setTimeout(function() { $("#modal_change_pwd_message_success").slideUp(); }, 4000);
			        	} else {
			        		$('#change_old_password').val('');
							$('#change_new_password').val('');
							$('#change_rw_password').val('');
			        		
			        		$("#modal_change_pwd_message").html(response.message);
							$("#modal_change_pwd_message").show();
							setTimeout(function() { $("#modal_change_pwd_message").slideUp(); }, 4000);
			        	}
			    	}
			    });
			}
		}
	}

	function logActivity(user, string) {
		$.ajax({
			type: 'post',
			url: "log/set_activity", 
			data: {
				'string' : string,
				'user': user
			},
			success: function(response){
	        	if(response.success) {
	        		console.log(response.message);
	        	} else {
	        		console.log(response.message);
	        	}
	    	},
	    	async: false
	    });
	}
</script>
<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <title>Mama Laundry</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Mama Loundry">
    <meta name="author" content="Echa r">
    
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <!-- The styles -->
    <link id="bs-css" href="<?php echo base_url(); ?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/charisma-app.css" rel="stylesheet">
    <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<?php echo base_url(); ?>assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/jquery.noty.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/elfinder.min.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/elfinder.theme.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/uploadify.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/animate.min.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/jquery-ui.css' rel='stylesheet'>
    

	<!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/jquery.min.js"></script>
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/icon.png">

</head>

<body>
    <!-- topbar starts -->
    <div style="border: none; border-bottom: 1px solid lightgray !important; background: linear-gradient(to bottom, rgba(30, 87, 153, 0.2) 0%, rgba(125, 185, 232, 0) 100%);" class="navbar navbar-default" role="navigation">

        <div class="navbar-inner"> 
            <a style="margin-left: 30px;" class="navbar-brand" href="<?php echo base_url(); ?>about"><img style="width: 150px; height: 30px; margin-top: -5px;" src="<?php echo base_url(); ?>assets/img/mamalaunLogo_.png" alt="MamaLaundry" /></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> <?php echo $user; ?></span>
                    <span class="caret"></span>
                </button> 
                <ul class="dropdown-menu">
                    <!-- <li style="color: blue;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $name; ?></strong></li>
                    <li style="color: blue;">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $occup; ?></li> -->
                    <li class="divider"></li>
                    <li><a href="#" data-toggle="modal" data-target="#change_pwd">Change Password</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>auth/do_logout"><i class="glyphicon glyphicon glyphicon-log-out"></i> LOGOUT</a></li>
                    <li class="divider"></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

        </div>
    </div>
    <!-- topbar ends -->
    
	<div class="modal fade" id="change_pwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Change Password</h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="" method="post">
                    		<div id='modal_change_pwd_message' class="alert alert-danger" style="display: none;"></div>
                    		<div id='modal_change_pwd_message_success' class="alert alert-success" style="display: none;"></div>
                    		<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="change_old_password"></label></span>
		                        <input name="change_old_password" id="change_old_password" type="password" class="form-control" placeholder="Old Password">
		                        <!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>"> -->
		                    </div>
		                    <div class="clearfix"></div><br>
                    		
                    		<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="change_new_password"></label></span>
		                        <input name="change_new_password" id="change_new_password" type="password" class="form-control" placeholder="New Password">
		                        <!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>"> -->
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="change_rw_password"></label></span>
		                        <input name="change_rw_password" id="change_rw_password" type="password" class="form-control" placeholder="Re-type New Password">
		                        <!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>"> -->
		                    </div>
		                    <div class="clearfix"></div>
		            </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
                    <a href="#" class="btn btn-primary" onclick="change();">CHANGE</a>
                </div>
            </div>
        </div>
    </div>
    
	<div class="modal fade" id="inactive_warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <h3>Are you there?</h3>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary" data-dismiss="modal">YES</a>
                </div>
            </div>
        </div>
    </div>