<div class="ch-container">
    <div class="row">
        
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                    	<?php
                    		$role = $this->session->userdata('role');  
                    		if($role == 0) {  ?>
						    <li class="nav-header">Main</li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>dashboard"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>branch"><i class="glyphicon glyphicon-map-marker"></i><span> Branch</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>service"><i class="glyphicon glyphicon-wrench"></i><span> Service</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>customer"><i class="glyphicon glyphicon-shopping-cart"></i><span> Customer</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>employee"><i class="glyphicon glyphicon-briefcase"></i><span> Employee</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>user"><i class="glyphicon glyphicon-user"></i><span> User</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>activity"><i class="glyphicon glyphicon-eye-open"></i><span> Activity</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>transaction"><i class="glyphicon glyphicon-book"></i><span> Transaction</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>order"><i class="glyphicon glyphicon glyphicon-list-alt"></i><span> Order</span></a></li>
	                        <li><a href="<?php echo base_url(); ?>report_expense"><i class="glyphicon glyphicon glyphicon-paperclip"></i> Expense</a></li>
	          				<li class="accordion"><a href="#"><i class="glyphicon glyphicon glyphicon-stats"></i><span> Report</span></a>
	                            <ul class="nav nav-pills nav-stacked">
	                                <li><a href="<?php echo base_url(); ?>report_cash"><i class="glyphicon glyphicon glyphicon-stats"></i> Cash</a></li>
	                                <li><a href="<?php echo base_url(); ?>report_sales"><i class="glyphicon glyphicon glyphicon-stats"></i> Sales</a></li>
	                                <li><a href="<?php echo base_url(); ?>report_salary"><i class="glyphicon glyphicon glyphicon-stats"></i> Salary</a></li>
	                                <li><a href="<?php echo base_url(); ?>report_completeness"><i class="glyphicon glyphicon glyphicon-stats"></i> Completeness</a></li>
	                            </ul>
	                        </li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>settling"><i class="glyphicon glyphicon-ok"></i><span> Completion</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>about"><i class="glyphicon glyphicon glyphicon-question-sign"></i><span> About</span></a></li>     
						<?php	} else if($role == 1) { ?>
							<li class="nav-header">Main</li>
							<?php if(($this->session->userdata('id_branch') == $this->session->userdata('id_branch_emp'))) { ?>
									<li><a class="ajax-link" href="<?php echo base_url(); ?>dashboard"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a></li>
							<?php } ?>
							<li><a class="ajax-link" href="<?php echo base_url(); ?>customer"><i class="glyphicon glyphicon-shopping-cart"></i><span> Customer</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>transaction"><i class="glyphicon glyphicon-book"></i><span> Transaction</span></a></li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>order"><i class="glyphicon glyphicon-list-alt"></i><span> Order</span></a></li>
	                        <li><a href="<?php echo base_url(); ?>report_expense"><i class="glyphicon glyphicon glyphicon-paperclip"></i> Expense</a></li>
	                        <li class="accordion"><a href="#"><i class="glyphicon glyphicon glyphicon-stats"></i><span> Report</span></a>
	                            <ul class="nav nav-pills nav-stacked">
	                                <li><a href="<?php echo base_url(); ?>report_cash"><i class="glyphicon glyphicon glyphicon-stats"></i> Cash</a></li>
	                                <li><a href="<?php echo base_url(); ?>report_sales"><i class="glyphicon glyphicon glyphicon-stats"></i> Sales</a></li>
	                                <li><a href="<?php echo base_url(); ?>report_completeness"><i class="glyphicon glyphicon glyphicon-stats"></i> Completeness</a></li>	                                
	                            </ul>
	                        </li>
	                        <li><a class="ajax-link" href="<?php echo base_url(); ?>about"><i class="glyphicon glyphicon glyphicon-question-sign"></i><span> About</span></a></li> 	
						<?php	} else {?>
							<li><a class="ajax-link" href="<?php echo base_url(); ?>transaction"><i class="glyphicon glyphicon-book"></i><span> Transaction</span></a></li>
							<li><a class="ajax-link" href="<?php echo base_url(); ?>about"><i class="glyphicon glyphicon glyphicon-question-sign"></i><span> About</span></a></li> 	
	                    <?php	} ?>   
                    </ul>
                </div>
            </div>
        </div> 