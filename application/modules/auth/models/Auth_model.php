<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
		
	public function login($username, $password) {
		$sql = "SELECT * FROM user WHERE username = '".$username."' AND password = '".$password."'";
 		$hasil = $this->db->query($sql);
		if(count($hasil->result_array()) > 0) {
			$result = array(
				'success' => true,
				'data' => $hasil->result_array()
			);
		} else {
			$result = array(
				'success' => false,
				'data' => $hasil->result_array()
			);
		}
		return $result; 
	}
	
	public function logout($id) {
		$sql = "UPDATE user
				SET status = 0
				WHERE id_user = ". $id;
 		$update = $this->db->query($sql); 
	}	
	
	public function getProfile($id, $branch) {
		$sql = "UPDATE user
				SET status = 1, last_login = NOW()
				WHERE id_user = ". $id;
 		$update = $this->db->query($sql);
		
		$sql = "SELECT b.name, b.occupation, b.phone_number, a.username, a.role, a.id_branch, b.id_employee, c.address AS branch_address, c.phone_number AS branch_phone, c.name AS branch, c.code
				FROM `user` a
				LEFT JOIN employee b ON a.id_employee = b.id_employee
				LEFT JOIN branch c ON c.id_branch = ".$branch."
				WHERE id_user = ". $id;
 		$update = $this->db->query($sql);
		
		return $update->result_array(); 
	}
    
    public function getBranch($branch) {
        $sql = "SELECT *
                FROM branch
                WHERE id_branch = ". $branch;
        $update = $this->db->query($sql);
        
        return $update->result_array(); 
    }
	
	public function check_user($id, $old_pwd) {
		$sql = "SELECT * FROM user WHERE id_user = '".$id."' AND password = '".$old_pwd."'";
 		$hasil = $this->db->query($sql);
		if(count($hasil->result_array()) > 0) {
			$result = array(
				'success' => true,
				'data' => $hasil->result_array()
			);
		} else {
			$result = array(
				'success' => false,
				'data' => $hasil->result_array()
			);
		}
		return $result; 
	}	
	
	public function change_password($id, $new_pwd) {
		$sql = "UPDATE user
				SET password = '".$new_pwd."'
				WHERE id_user = ".$id;
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}			
	
}	