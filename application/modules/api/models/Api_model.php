<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
		
	public function login($username, $password) {
		$sql = "SELECT * FROM user WHERE username = '".$username."' AND password = '".$password."' AND role = 0";
 		$hasil = $this->db->query($sql);
		if(count($hasil->result_array()) > 0) {
			$result = array(
				'success' => true,
				'data' => $hasil->result_array()
			);
		} else {
			$result = array(
				'success' => false,
				'data' => $hasil->result_array()
			);
		}
		return $result; 
	}
	
	public function getProfile($id) {
		$sql = "SELECT * FROM `user` WHERE id_user = ". $id;
 		$update = $this->db->query($sql);
		
		return $update->result_array(); 
	}	

	public function getCustomer() {
	    $sql = "SELECT DISTINCT customer_phone
				FROM invoice
				WHERE invoice_issue_date = CAST(NOW() AS DATE)";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}

	public function sumCash() {
	    $sql = "SELECT SUM(net_price) AS `sum`
				FROM invoice
				WHERE payment_date = CAST(NOW() AS DATE)";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}

	public function sumSales() {
	    $sql 	= '	SELECT SUM(net_price) AS `sum`
					FROM invoice
					WHERE invoice_issue_date = CAST(NOW() AS DATE)';
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		return $result;
	}

	public function getInvoice_count() {
	   $sql = "SELECT DISTINCT invoice_number
				FROM invoice
				WHERE invoice_issue_date = CAST(NOW() AS DATE)";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}	
	
}	