<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function getCustomer() {
	    $branch = $this->session->userdata('id_branch');
        
		$sql = "SELECT DISTINCT customer_phone
				FROM invoice
				WHERE invoice_issue_date = CAST(NOW() AS DATE) AND id_branch =".$branch;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function sumCash() {
	    $branch = $this->session->userdata('id_branch');
        
		$sql = "SELECT SUM(net_price) AS `sum`
				FROM invoice
				WHERE payment_date = CAST(NOW() AS DATE) AND id_branch =".$branch;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function sumSales() {
	    $branch = $this->session->userdata('id_branch');
        
		$sql 	= '	SELECT SUM(net_price) AS `sum`
					FROM invoice
					WHERE invoice_issue_date = CAST(NOW() AS DATE) AND id_branch ='.$branch;
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		return $result;
	}
	
	public function getInvoice_count() {
	    $branch = $this->session->userdata('id_branch');
        
		$sql = "SELECT DISTINCT invoice_number
				FROM invoice
				WHERE invoice_issue_date = CAST(NOW() AS DATE) AND id_branch =".$branch;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function get_service_list($service) {
	    $branch = $this->session->userdata('id_branch');
        
		$sql = "SELECT *
				FROM service
				WHERE name LIKE '%".$service."%' AND id_branch =".$branch;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}

	public function get_customer_list($customer) {
	    $branch = $this->session->userdata('id_branch');
        
		$sql = "SELECT *
				FROM customer
				WHERE (name LIKE '%".$customer."%' OR customer_phone LIKE '%".$customer."%') AND id_branch =".$branch;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
    
    public function get_invoice_list($invoice) {
        $branch = $this->session->userdata('id_branch');
        
        $sql = "SELECT *
                FROM invoice
                WHERE invoice_number LIKE '%".$invoice."%' AND id_branch = ".$branch;
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }

	public function newInvoice() {
		$sql = "SELECT CONCAT(SUBSTRING_INDEX(invoice_number, '-', 1), '-', MAX(CAST(SUBSTRING(invoice_number,LOCATE('-',invoice_number)+1) AS SIGNED))) AS invoice FROM invoice WHERE id_branch = ".$this->session->userdata('id_branch');
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function checkInvoice($invoice) {
		$sql = "SELECT COUNT(*) AS total FROM invoice WHERE invoice_number = '".$invoice."' AND id_branch = ".$this->session->userdata('id_branch');
 		$check = $this->db->query($sql);
		return $check->result_array(); 
	}	
	
	public function insert_customer($customer_phone, $customer_name, $address) {
	    $branch = $this->session->userdata('id_branch');
		$sql = "INSERT INTO customer (customer_phone, id_branch, name, address) VALUES ('".$customer_phone."', ".$branch.", '".$customer_name."', '".$address."')";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
    
    public function update_customer($customer_phone, $address) {
        $branch = $this->session->userdata('id_branch');
        $sql = "UPDATE customer SET address = '".$address."' WHERE customer_phone = '".$customer_phone."'";
        $add = $this->db->query($sql);
        if($add) {
            return true; 
        } else {
            return false; 
        }
    }
	
	public function get_service_price($id) {
	    $branch = $this->session->userdata('id_branch');
		$sql = "SELECT price, wash_salary, iron_salary
				FROM service
				WHERE id_service = " .$id. " AND id_branch = ".$branch;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function insert_invoice($invoice_number, $customer_phone, $netprice, $status, $discount, $note, $pcs) {
		$payment_date = 'null';	
		if($status == 1) {
			$payment_date = 'NOW()';
			$netprice = $netprice - $discount;
		}
		if(empty($discount)) {
			$discount = 0;
		}
		$sql = "INSERT INTO invoice (invoice_number, customer_phone, id_branch, invoice_issue_date, net_price, discount, note, total_pcs, status, payment_date) 
				VALUES ('".$invoice_number."', '".$customer_phone."', ".$this->session->userdata('id_branch').", NOW(), ".$netprice.", ".$discount.", '".$note."', ".$pcs.", ".$status.", ".$payment_date.")";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}	
    
    public function update_invoice($invoice_number, $customer_phone, $netprice, $note, $pcs) {
        $sql = "UPDATE invoice
                SET net_price = ".$netprice.", customer_phone = '".$customer_phone."', note = '".$note."', total_pcs = ".$pcs."
                WHERE invoice_number = '".$invoice_number."'";
        $add = $this->db->query($sql);
        if($add) {
            return true; 
        } else {
            return false; 
        }
    }
	
	public function check_existing($customer_phone) { 
			
		$sql 	= '	SELECT COUNT(*) AS total
					FROM customer
					WHERE customer_phone = \''. $customer_phone .'\' AND id_branch = '.$this->session->userdata('id_branch');
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->row_array(); 
		
		$return = array(
			'total' => $result['total'],
		);
		
		return $return;
	}
    
    public function check_matching($customer_name, $customer_phone) { 
            
        $sql    = ' SELECT COUNT(*) AS total
                    FROM customer
                    WHERE name = \''. $customer_name .'\' AND customer_phone = \''. $customer_phone .'\' AND id_branch = '.$this->session->userdata('id_branch');
        
        
        $query  =  $this->db->query($sql);
        $result = $query->row_array(); 
        
        $return = array(
            'total' => $result['total'],
        );
        
        return $return;
    }
	
	public function update_voucher($invoice) {
		$sql = "UPDATE voucher
				SET status = 1
				WHERE invoice_number = ".$invoice;
 		$add = $this->db->query($sql);
	}
	
	public function insert_voucher($invoice_number, $customer_phone) {
		$sql = "INSERT INTO voucher (customer_phone, invoice_number, status) VALUES ('".$customer_phone."', ".$invoice_number.", 0)";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
	
	public function insert_order($invoice_number, $id_service, $customer_phone, $quantity, $price, $wash_salary, $iron_salary) {
		$sql = "INSERT INTO `order`(`invoice_number`, `customer_phone`, `id_service`, `id_branch`, `wash_salary_emp`, `iron_salary_emp`, `quantity`, `price`, `order_date`) 
				VALUES ('".$invoice_number."', '".$customer_phone."', ".$id_service.", ".$this->session->userdata('id_branch').", ".$wash_salary.", ".$iron_salary.", ".$quantity.", ".$price.", NOW())";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
    
    public function insert_order_update($invoice_number, $id_service, $customer_phone, $quantity, $price, $wash_salary, $iron_salary, $order_date, $packing_date) {
        $sql = "INSERT INTO `order`(`invoice_number`, `customer_phone`, `id_service`, `id_branch`, `wash_salary_emp`, `iron_salary_emp`, `quantity`, `price`, `order_date`, `packing_date`) 
                VALUES ('".$invoice_number."', '".$customer_phone."', ".$id_service.", ".$this->session->userdata('id_branch').", ".$wash_salary.", ".$iron_salary.", ".$quantity.", ".$price.", '".$order_date."', '".$packing_date."')";
        $add = $this->db->query($sql);
        if($add) {
            return true; 
        } else {
            return false; 
        }
    }
    
    public function delete_order($invoice_number) {
        $sql = "DELETE FROM `order` WHERE invoice_number = '". $invoice_number ."'";
        $delete = $this->db->query($sql);
        if($delete) {
            return true; 
        } else {
            return false; 
        }
    }
    
    public function delete_invoice($invoice_number) {
        $sql = "DELETE FROM invoice WHERE invoice_number = '". $invoice_number ."'";
        $delete = $this->db->query($sql);
        if($delete) {
            return true; 
        } else {
            return false; 
        }
    }
	
    public function get_invoice_data($invoice) {
        $branch = $this->session->userdata('id_branch');
        
        $sql = "SELECT d.invoice_number, d.invoice_issue_date, b.name AS customer, b.customer_phone, b.address, c.name AS service, c.id_service, c.group, c.price, a.quantity, a.quantity * c.price AS total_price, d.note, d.total_pcs, d.net_price, d.discount, a.order_date, a.packing_date, c.wash_salary, c.iron_salary
                FROM `order` a
                LEFT JOIN customer b ON a.customer_phone = b.customer_phone
                LEFT JOIN service c ON a.id_service = c.id_service
                LEFT JOIN invoice d ON a.invoice_number = d.invoice_number
                WHERE a.invoice_number = '".$invoice."' AND a.id_branch = ".$branch." AND b.id_branch = ".$branch."
                GROUP BY a.id_order";
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
}	
