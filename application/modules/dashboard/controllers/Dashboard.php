<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
  public function __construct()
	{
		parent::__construct();			
		if($this->session->userdata('role') == 2) { redirect('order'); }
		if($this->session->userdata('role') != 0) {
			if(($this->session->userdata('id_branch') != $this->session->userdata('id_branch_emp'))) { redirect('transaction'); }		
		}
		$this->load->model('dashboard_model');
	}
	
	public function index()
	{
	    if (!$this->session->userdata('id_user')) {
            redirect('login');
        }
        
		$customer = $this->dashboard_model->getCustomer();
		$sum_cash = $this->dashboard_model->sumCash();
		$sum_sales= $this->dashboard_model->sumSales();
		$invoice = $this->dashboard_model->getInvoice_count();
		
		$data['customer'] = count($customer);
		$data['sum_cash'] = $sum_cash[0]['sum'];
		$data['sum_sales'] = $sum_sales[0]['sum'];
		$data['invoice'] = count($invoice);
		
		//----------------------------------------------------------------------start invoice
        $fix_last_invoice;
        $last_invoice = $this->dashboard_model->newInvoice();
        if($this->session->userdata('id_branch') == 1) {
            if($last_invoice[0]['invoice'] == '') {
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '1001';
            } else {
                $rmax = explode("-", $last_invoice[0]['invoice']);
                $max = $rmax[1] + 1;
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
            }
        } else if($this->session->userdata('id_branch') == 2) {
            if($last_invoice[0]['invoice'] == '') {
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '5001';
            } else {
                $rmax = explode("-", $last_invoice[0]['invoice']);
                $max = $rmax[1] + 1;
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
            }
        } else {
            if($last_invoice[0]['invoice'] == '') {
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '1';
            } else {
                $rmax = explode("-", $last_invoice[0]['invoice']);
                $max = $rmax[1] + 1;
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
            }
        }
        
        $data['newInvoice'] = $fix_last_invoice;
        //-----------------------------------------------------------------------------------
		
		$this->template->load('maintemplate', 'dashboard/views/dashboard_view', $data);
	}

    public function update_dashboard() {
        $customer = $this->dashboard_model->getCustomer();
        $sum_cash = $this->dashboard_model->sumCash();
        $sum_sales= $this->dashboard_model->sumSales();
        $invoice = $this->dashboard_model->getInvoice_count();
        
        $result = array();
        
        array_push($result, 
            array(
                count($customer),
                'Rp.'.number_format($sum_cash[0]['sum']),
                'Rp.'.number_format($sum_sales[0]['sum']),
                count($invoice)
            )
        );
       

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function get_service() {
		$service = $this->input->get('service');
		$data = $this->dashboard_model->get_service_list($service);
		$result = array();
		foreach ( $data as $list ) {
			array_push($result, 
				array(
					$list['id_service'],
					$list['name'],
					$list['group'],
					$list['price'],
					$list['wash_salary'],
					$list['iron_salary']
				)
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function get_customer() {
		$customer = $this->input->get('customer');
		$data = $this->dashboard_model->get_customer_list($customer);
		$result = array();
		foreach ( $data as $list ) {
			array_push($result, 
				array(
					$list['name'],
					$list['customer_phone'],
					$list['address']
				)
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    public function get_invoice() {
        $invoice = $this->input->get('invoice');
        $data = $this->dashboard_model->get_invoice_list($invoice);
        $result = array();
        foreach ( $data as $list ) {
            array_push($result, 
                array(
                    $list['invoice_number']
                )
            );
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function check_invoice() {
		$invoice = $this->input->get('invoice');
		$data = $this->dashboard_model->checkInvoice($invoice);
		$total = '';
		foreach($data as $list) {
			$total = $list['total'];
		}
		
		if($total == 1) {
			$this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Invoice '.$invoice.' is already exist, try again!');	
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($total));
    }
	
	public function add_customer() {
		$customer_name = $this->input->post('customer_name');
        $customer_phone = $this->input->post('customer_phone');
		$address = $this->input->post('address');
        
		$exist = $this->dashboard_model->check_existing($customer_phone);
		if($exist['total'] == 0) {
			$insert_customer = $this->dashboard_model->insert_customer($customer_phone, $customer_name, $address);
            
            if($insert_customer) {
                $result = array(
                    'success' => true
                );
            } else {
                $this->session->set_flashdata('message_type', 'error');
                $this->session->set_flashdata('messages', 'Failed, Please try again!'); 
                
                $result = array(
                    'success' => false
                );
            }
		} else {
		    $update_customer = $this->dashboard_model->update_customer($customer_phone, $address);
		}
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function get_service_price() {
		$id = $this->input->get('id');
		$data = $this->dashboard_model->get_service_price($id);
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
		    $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Failed, Please try again!'); 
            
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function add_invoice() {
		$invoice_number = $this->input->post('invoice_number');
		$netprice = $this->input->post('net_price');
        $customer_phone = $this->input->post('customer_phone');
		$status = $this->input->post('status');
		$discount = $this->input->post('discount');
		$note = $this->input->post('note');
        
		$insert_invoice = $this->dashboard_model->insert_invoice($invoice_number, $customer_phone, $netprice, $status, $discount, $note);
        
        if($insert_invoice) {
            $result = array(
                'success' => true
            );
        } else {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Failed, Please try again!'); 
            
            $result = array(
                'success' => false
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    public function update_invoice() {
        $invoice_number = $this->input->post('invoice_number');
        $netprice = $this->input->post('net_price');
        $customer_phone = $this->input->post('customer_phone');
        $note = $this->input->post('note');
        $pcs = $this->input->post('pcs');
        $discount = $this->input->post('discount');
        
        $netprice = $netprice - $discount;
        
        $update_invoice = $this->dashboard_model->update_invoice($invoice_number, $customer_phone, $netprice, $note, $pcs);
        
        if($update_invoice) {
            $result = array(
                'success' => true
            );
        } else {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Failed, Please try again!'); 
            
            $result = array(
                'success' => false
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function update_voucher() {
		$invoice = $this->input->post('invoice');
		
		$update = $this->dashboard_model->update_voucher($invoice);
    }
	
	public function add_order() {
		$invoice_number = $this->input->post('invoice_number');
		$id_service = $this->input->post('id_service');
        $customer_phone = $this->input->post('customer_phone');
		$quantity = $this->input->post('quantity');
		$price = $this->input->post('price');
        $wash_salary = $this->input->post('wash_salary');
		$iron_salary = $this->input->post('iron_salary');
        
		$insert_order = $this->dashboard_model->insert_order($invoice_number, $id_service, $customer_phone, $quantity, $price, $wash_salary, $iron_salary);
		// if($insert_order) {
			// $this->session->set_flashdata('message_type', 'success');
            // $this->session->set_flashdata('messages', 'Invoice '.$invoice_number.' saved!');	
		// }
		$fix_last_invoice;
        $last_invoice = $this->dashboard_model->newInvoice();
        if($this->session->userdata('id_branch') == 1) {
            if($last_invoice[0]['invoice'] == '') {
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '1001';
            } else {
                $rmax = explode("-", $last_invoice[0]['invoice']);
                $max = $rmax[1] + 1;
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
            }
        } else if($this->session->userdata('id_branch') == 2) {
            if($last_invoice[0]['invoice'] == '') {
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '5001';
            } else {
                $rmax = explode("-", $last_invoice[0]['invoice']);
                $max = $rmax[1] + 1;
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
            }
        } else {
            if($last_invoice[0]['invoice'] == '') {
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '0';
            } else {
                $rmax = explode("-", $last_invoice[0]['invoice']);
                $max = $rmax[1] + 1;
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
            }
        }
		
		if($insert_order) {
			$result = array(
				'success' => true,
				'new_invoice' => $fix_last_invoice
			);
		} else {
		    $result = array(
                'success' => false
            );
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function delete_order() {
        $invoice_number = $this->input->post('invoice_number');
        $delete_order = $this->dashboard_model->delete_order($invoice_number);
        
        if($delete_order) {
            $result = array(
                'success' => true
            );
        } else {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Failed, Please try again!'); 
            
            $result = array(
                'success' => false
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    public function delete_invoice() {
        $invoice_number = $this->input->post('invoice_number');
        $delete_order = $this->dashboard_model->delete_invoice($invoice_number);
        
        if($delete_order) {
            $result = array(
                'success' => true
            );
        } else {
            $this->session->set_flashdata('message_type', 'error');
            $this->session->set_flashdata('messages', 'Failed, Please try again!'); 
            
            $result = array(
                'success' => false
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function update_order() {
        $invoice_number = $this->input->post('invoice_number');
        $id_service = $this->input->post('id_service');
        $customer_phone = $this->input->post('customer_phone');
        $quantity = $this->input->post('quantity');
        $price = $this->input->post('price');
        $wash_salary = $this->input->post('wash_salary');
        $iron_salary = $this->input->post('iron_salary');
        $order_date = $this->input->post('order_date');
        $packing_date = $this->input->post('packing_date');
        
        $insert_order = $this->dashboard_model->insert_order_update($invoice_number, $id_service, $customer_phone, $quantity, $price, $wash_salary, $iron_salary, $order_date, $packing_date); 
       
        $fix_last_invoice;
        $last_invoice = $this->dashboard_model->newInvoice();
        if($this->session->userdata('id_branch') == 1) {
            if($last_invoice[0]['invoice'] == '') {
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '1001';
            } else {
                $rmax = explode("-", $last_invoice[0]['invoice']);
                $max = $rmax[1] + 1;
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
            }
        } else if($this->session->userdata('id_branch') == 2) {
            if($last_invoice[0]['invoice'] == '') {
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '5001';
            } else {
                $rmax = explode("-", $last_invoice[0]['invoice']);
                $max = $rmax[1] + 1;
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
            }
        } else {
            if($last_invoice[0]['invoice'] == '') {
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '0';
            } else {
                $rmax = explode("-", $last_invoice[0]['invoice']);
                $max = $rmax[1] + 1;
                $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
            }
        }
        
        if($insert_order) {
            $result = array(
                'success' => true,
                'new_invoice' => $fix_last_invoice
            );
        } else {
            $result = array(
                'success' => false
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
    public function get_invoice_data() {
        $invoice = $this->input->get('invoice');
        $data = $this->dashboard_model->get_invoice_data($invoice);
        $result = array();
        
        if(count($data) > 0) {
            foreach ( $data as $list ) {
                array_push($result, 
                    array(
                        true,
                        $list['invoice_number'],
                        date('d M Y', strtotime($list['invoice_issue_date'])),
                        $list['customer'],
                        $list['customer_phone'],
                        $list['address'],
                        $list['service'],
                        number_format($list['price']),
                        $list['price'],
                        $list['quantity'],
                        number_format($list['total_price']),
                        $list['price']*$list['quantity'],//$list['total_price'],
                        $list['note'],
                        'Rp.'.number_format($list['net_price'] + $list['discount']),
                        $list['id_service'],
                        $list['group'],
                        $list['discount'],
                        $list['order_date'],
                        $list['packing_date'],
                        $list['wash_salary'],
                        $list['iron_salary'],
                        $list['total_pcs']
                    )
                );
            }
        } else {
            array_push($result, 
                array(
                    false
                )
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function submit_order_invoice() {
        $transaction = $this->input->post('transaction');
        
        $insert_customer = false;
        $update_customer = false;
        
        $match = $this->dashboard_model->check_matching($transaction[0][0], $transaction[0][1]);
        $exist = $this->dashboard_model->check_existing($transaction[0][1]);
        
        if($match['total'] == 0 && $exist['total'] != 0) {
            $result = array(
                'success' => false,
                'message' => 'The customer phone is already exist with different name. Please check your input!'
            );
        } else {
            if($exist['total'] == 0) {
                $insert_customer = $this->dashboard_model->insert_customer($transaction[0][1], $transaction[0][0], $transaction[0][2]);
            } else {
                $update_customer = $this->dashboard_model->update_customer($transaction[0][1], $transaction[0][2]);
            }
            
            if($insert_customer || $update_customer) {
                $insert_invoice = $this->dashboard_model->insert_invoice($transaction[0][4], $transaction[0][1], $transaction[0][3], $transaction[0][5], $transaction[0][6], str_replace("'","\\'", $transaction[0][7]), $transaction[0][9]); 
            }
            
            $valid = 0;
            if($insert_invoice) {
                $orders = count($transaction[0][8]);
                for($i = 0; $i < $orders; $i++) {
                    $insert_order = $this->dashboard_model->insert_order($transaction[0][8][$i][2], $transaction[0][8][$i][1], $transaction[0][8][$i][0], $transaction[0][8][$i][3], $transaction[0][8][$i][4], $transaction[0][8][$i][5], $transaction[0][8][$i][6]);  
                    $valid++;
                }
            }
            
            $fix_last_invoice;
            $last_invoice = $this->dashboard_model->newInvoice();
            if($this->session->userdata('id_branch') == 1) {
                if($last_invoice[0]['invoice'] == '') {
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '1001';
                } else {
                    $rmax = explode("-", $last_invoice[0]['invoice']);
                    $max = $rmax[1] + 1;
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
                }
            } else if($this->session->userdata('id_branch') == 2) {
                if($last_invoice[0]['invoice'] == '') {
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '5001';
                } else {
                    $rmax = explode("-", $last_invoice[0]['invoice']);
                    $max = $rmax[1] + 1;
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
                }
            } else {
                if($last_invoice[0]['invoice'] == '') {
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '0';
                } else {
                    $rmax = explode("-", $last_invoice[0]['invoice']);
                    $max = $rmax[1] + 1;
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
                }
            }
            
            if($valid === $orders) {
                $result = array(
                    'success' => true,
                    'new_invoice' => $fix_last_invoice
                );
            } else {
                $result = array(
                    'success' => false,
                    'message' => 'Submit Invoice '+$transaction[0][4]+' Failed. Please try again!'
                );
            }
            
            $valid = 0;
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function submit_order_invoice_change() {
        $transaction = $this->input->post('transaction');
        
        $insert_customer = false;
        $update_customer = false;
        
        $match = $this->dashboard_model->check_matching($transaction[0][0], $transaction[0][1]);
        $exist = $this->dashboard_model->check_existing($transaction[0][1]);
        
        if($match['total'] == 0 && $exist['total'] != 0) {
            $result = array(
                'success' => false,
                'message' => 'The customer phone is already exist with different name. Please check your input!'
            );
        } else {
            if($exist['total'] == 0) {
                $insert_customer = $this->dashboard_model->insert_customer($transaction[0][1], $transaction[0][0], $transaction[0][2]);
            } else {
                $update_customer = $this->dashboard_model->update_customer($transaction[0][1], $transaction[0][2]);
            }
            
            if($insert_customer || $update_customer) {
                $update_invoice = $this->dashboard_model->update_invoice($transaction[0][4], $transaction[0][1], $transaction[0][3], str_replace("'","\\'", $transaction[0][7]), $transaction[0][9]);
            }
            
            $valid = 0;
            if($update_invoice) {
                $delete_order = $this->dashboard_model->delete_order($transaction[0][4]);

                $orders = count($transaction[0][8]);
                for($i = 0; $i < $orders; $i++) {
                    $insert_order = $this->dashboard_model->insert_order_update($transaction[0][8][$i][2], $transaction[0][8][$i][1], $transaction[0][8][$i][0], $transaction[0][8][$i][3], $transaction[0][8][$i][4], $transaction[0][8][$i][5], $transaction[0][8][$i][6], $transaction[0][8][$i][7], $transaction[0][8][$i][8]);  

                    $valid++;
                }
            }
            
            $fix_last_invoice;
            $last_invoice = $this->dashboard_model->newInvoice();
            if($this->session->userdata('id_branch') == 1) {
                if($last_invoice[0]['invoice'] == '') {
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '1001';
                } else {
                    $rmax = explode("-", $last_invoice[0]['invoice']);
                    $max = $rmax[1] + 1;
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
                }
            } else if($this->session->userdata('id_branch') == 2) {
                if($last_invoice[0]['invoice'] == '') {
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '5001';
                } else {
                    $rmax = explode("-", $last_invoice[0]['invoice']);
                    $max = $rmax[1] + 1;
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
                }
            } else {
                if($last_invoice[0]['invoice'] == '') {
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . '0';
                } else {
                    $rmax = explode("-", $last_invoice[0]['invoice']);
                    $max = $rmax[1] + 1;
                    $fix_last_invoice = $this->session->userdata('branch_code') . '-' . $max;
                }
            }
            
            if($valid === $orders) {
                $result = array(
                    'success' => true,
                    'new_invoice' => $fix_last_invoice
                );
            } else {
                $result = array(
                    'success' => false,
                    'message' => 'Submit Invoice '+$transaction[0][4]+' Failed. Please try again!'
                );
            }
            
            $valid = 0;
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
