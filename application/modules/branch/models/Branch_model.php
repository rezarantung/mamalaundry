<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Branch_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function check_existing($name) { 
			
		$sql 	= '	SELECT COUNT(*) AS total
					FROM branch
					WHERE name = \''. $name .'\'';
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->row_array(); 
		
		$return = array(
			'total' => $result['total'],
		);
		
		return $return;
	}
    
    public function check_existing1($code) { 
            
        $sql    = ' SELECT COUNT(*) AS total
                    FROM branch
                    WHERE code = \''. $code .'\'';
        
        
        $query  =  $this->db->query($sql);
        $result = $query->row_array(); 
        
        $return = array(
            'total' => $result['total'],
        );
        
        return $return;
    }
	
	public function getBranch($id) {
		$sql = "SELECT *
				FROM branch
				WHERE id_branch = ".$id;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function list_branch($params = array()) { 
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (name LIKE \'%'.$params['filter'].'%\' OR address LIKE \'%'.$params['filter'].'%\' OR phone_number LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT *
					FROM branch
					WHERE 1=1 '.$where.'
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM branch
									WHERE 1=1 '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total'],
		);
		
		return $return;
	}
	
	public function add($name, $code, $address, $phone) {
		$sql = "INSERT INTO branch (name, code, address, phone_number)
				VALUES ('".$name."', '".$code."', '".$address."', '".$phone."')";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}	
	
	public function update($name, $code, $address, $phone, $id) {
		$sql = "UPDATE branch
				SET name = '".$name."', code = '".$code."', address = '".$address."', phone_number = '".$phone."'  
				WHERE id_branch = ".$id;
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
	
	public function delete($id) {
		$sql = "DELETE FROM branch WHERE id_branch = ". $id;
 		$delete = $this->db->query($sql);
		if($delete) {
			return true; 
		} else {
			return false; 
		}
	}

}	
