<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends CI_Controller {
  public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('role') != 0) { redirect('dashboard'); }		
		$this->load->model('branch_model');

	}
	
	public function index()
	{
		$this->template->load('maintemplate', 'branch/views/branch_view');
	}
	
	public function get_branch() {
		$id = $this->input->post('id');
		$data = $this->branch_model->getBranch($id);
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function list_branch() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('name', 'code', 'address', 'phone_number');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->branch_model->list_branch($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			$disabled = '';
			// if($v['id_branch'] === $this->session->userdata('id_branch')) {
				// $disabled = 'disabled="disabled"';
			// }
			array_push($data, 
				array(
					$v['name'],
					$v['code'],
					$v['address'],
					$v['phone_number'],
					'<a class="btn btn-primary btn-xs" href="#" title"Edit" onclick="edit_('.$v['id_branch'].')" '.$disabled.'><i class="glyphicon glyphicon-edit"></i></a>'
					.' '.
					'<a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_('.$v['id_branch'].', \''.$v['name'].'\')" '.$disabled.'><i class="glyphicon glyphicon-trash"></i></a>'
					
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_branch() {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('code', 'Prefix', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('phone', 'Phone number', 'required|numeric');

        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $name = $this->input->post('name');
            $code = $this->input->post('code');
            $address = $this->input->post('address');
            $phone = $this->input->post('phone');
			
			$exist = $this->branch_model->check_existing($name);
            $exist1 = $this->branch_model->check_existing1($code);
			if($exist['total'] != 0) {
				$result = array(
					'success' => false,
					'message' => $name.' is already exist!'
				);
			} else if($exist1['total'] != 0) {
                $result = array(
                    'success' => false,
                    'message' => $code.' is already exist!'
                );
            } else {
				$do_submit = $this->branch_model->add($name, $code, $address, $phone);
	            if ($do_submit) {
	            	$result = array(
						'success' => true,
						'message' => 'Branch added!'
					);
	            } else {
	                $result = array(
						'success' => false,
						'message' => 'Failed to add new branch!'
					);
	            }
			}  
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function update_branch() {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('code', 'Prefix', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('phone', 'Phone number', 'required|numeric');

        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $name = $this->input->post('name');
            $code = $this->input->post('code');
            $address = $this->input->post('address');
            $phone = $this->input->post('phone');
            $id = $this->input->post('id');
			
			// $exist = $this->branch_model->check_existing($name);
			// if($exist['total'] != 0) {
				// $result = array(
					// 'success' => false,
					// 'message' => $name.' is already exist!'
				// );
			// } else {
				$do_update = $this->branch_model->update($name, $code, $address, $phone, $id);
	            if ($do_update) {
	            	$result = array(
						'success' => true,
						'message' => 'Branch updated!'
					);
	            } else {
	                $result = array(
						'success' => false,
						'message' => 'Failed to update branch!'
					);
	            }	
			//}
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function delete_branch() {
        
        $id = $this->input->post('id');
        
        $do_delete = $this->branch_model->delete($id);
        if ($do_delete) {
        	$result = array(
				'success' => true,
				'message' => 'Branch deleted!'
			);
        } else {
            $result = array(
				'success' => false,
				'message' => 'Failed to delete branch!'
			);
        }
        
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
}
