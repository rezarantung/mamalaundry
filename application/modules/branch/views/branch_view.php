<script>
	var table

	$(document).ready(function(){
		table = $('#branch_list').DataTable({
			"processing": true,
			"serverSide": true,
	        "ajax": {
	        	'type' : 'get',
	        	'url': 'branch/list_branch'     
	        },
	        "columnDefs": [{
				"targets": [2,3,4],
				"orderable": false
			}],
            "order": []
	    });

	});
	
	function submit() {
		var r = confirm("Are you sure want to submit?","Ok","Cancel");
		if(r){
			var name = $('#name').val();
			var code = $('#code').val();
			var address = $('#address').val();
			var phone = $('#phone').val();
			
			$.ajax({
				type: 'post',
				url: "branch/add_branch", 
				data: {
					'name': name.toUpperCase(),
					'code': code.toUpperCase(),
					'address': address.toUpperCase(),
					'phone': phone.toUpperCase()
				}, 
				success: function(response){
		        	if(response.success) {
		        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Create Branch : ' + '"' + name + '"');
		        		
		        		$('#name').val('');
		        		$('#code').val('');
						$('#address').val('');
						$('#phone').val('');
						
		        		$('#addBranch').modal('toggle');
		        		table.ajax.reload();
		        		$("#alert_branch_list").html(response.message);
						$("#alert_branch_list").show();
						setTimeout(function() { $("#alert_branch_list").slideUp(); }, 4000);
		        	} else {
		        		$("#modal_message").html(response.message);
						$("#modal_message").show();
						setTimeout(function() { $("#modal_message").slideUp(); }, 4000);
		        	}
		    	}
		    });	
		}
	}
	
	function delete_(id, branch) {
		var r = confirm("Are you sure want to delete \""+branch+"\"?","Ok","Cancel");
			if(r){
				$.ajax({
					type: 'post',
					url: "branch/delete_branch", 
					data: { 'id': id }, 
					success: function(response){
			        	if(response.success) {
			        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Delete Branch : ' + '"' + branch + '"');
			        		table.ajax.reload();
			        		$("#alert_branch_list").html(response.message);
							$("#alert_branch_list").show();
							setTimeout(function() { $("#alert_branch_list").slideUp(); }, 4000);
			        	} else {
			        		table.ajax.reload();
			        		$("#danger_branch_list").html(response.message);
							$("#danger_branch_list").show();
							setTimeout(function() { $("#danger_branch_list").slideUp(); }, 4000);
			        	}
			    	}
			    });
			}
	}	
	
	function edit_(id) {
		$('#editBranch').modal('toggle');
		$.ajax({
			type: 'post',
			url: "branch/get_branch", 
			data: { 'id': id }, 
			success: function(response){
	        	if(response.success) {
	        		$('#edit_name').val(response.data[0].name);
	        		$('#edit_code').val(response.data[0].code);
					$('#edit_address').val(response.data[0].address);
					$('#edit_phone').val(response.data[0].phone_number);
					$('#edit_id_branch').val(id);
	        	} else {
	        		console.log(response.message);
	        	}
	    	}
	    });
	}	
	
	function update() {
		var r = confirm("Are you sure want to update?","Ok","Cancel");
		if(r){
			var name = $('#edit_name').val();
			var code = $('#edit_code').val();
			var address = $('#edit_address').val();
			var phone = $('#edit_phone').val();
			var id = $('#edit_id_branch').val();
			
			$.ajax({
				type: 'post',
				url: "branch/update_branch", 
				data: {
					'id': id,
					'name': name.toUpperCase(),
					'code': code.toUpperCase(),
					'address': address.toUpperCase(),
					'phone': phone.toUpperCase()
				}, 
				success: function(response){
		        	if(response.success) {
		        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Update Branch : ' + '"' + name + '"');
		        		
		        		$('#edit_name').val('');
		        		$('#edit_code').val('');
						$('#edit_address').val('');
						$('#edit_phone').val('');
		        		
		        		$('#editBranch').modal('toggle');
		        		table.ajax.reload();
		        		$("#alert_branch_list").html(response.message);
						$("#alert_branch_list").show();
						setTimeout(function() { $("#alert_branch_list").slideUp(); }, 4000);
		        	} else {
		        		$("#edit_modal_message").html(response.message);
						$("#edit_modal_message").show();
						setTimeout(function() { $("#edit_modal_message").slideUp(); }, 4000);
		        	}
		    	}
		    });
		}
	}

</script>
<style type="text/css">
	input[type=text], input[type=tel], input[type=hidden], textarea {
        text-transform: uppercase !important;
    }
    
    .input-group-addon {
        min-width: 100px;
        text-align: left;
        font-weight: lighter;
    }
</style>
<div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Branch</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Branch List</h2>
				<div class="pull-right">
					<a class="btn btn-primary btn-xs" href="#" data-toggle="modal" data-target="#addBranch"><i class="glyphicon glyphicon-plus" data-toggle="tooltip" title="" data-original-title="Add"></i> Add</a>
                </div>
            </div>
            <div id="alert_branch_list" class="alert alert-success" style="display: none; text-align: center;"></div>
            <div id="danger_branch_list" class="alert alert-danger" style="display: none; text-align: center;"></div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
                    <table id="branch_list" width="100%" class="table table-striped">
                        <thead>
                        	<tr>
                        		<th>NAME</th>
                        		<th>PREFIX</th>
                        		<th>ADDRESS</th>
                        		<th>PHONE NUMBER</th>
                        		<th>ACTION</th>
                    		</tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->

	<div class="modal fade" id="addBranch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">×</button>
	                    <h3>Add Branch</h3>
	                </div>
	                <div class="modal-body">
	                    <form class="form-horizontal" action="" method="post">
                    		<div id='modal_message' class="alert alert-danger" style="display: none;"></div>
	                    		
                    		<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="name">NAME</label></span>
		                        <input name="name" id="name" type="text" class="form-control" placeholder="Name">
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
                                <span class="input-group-addon"><label for="code">PREFIX</label></span>
                                <input name="code" id="code" type="text" class="form-control" placeholder="Prefix">
                            </div>
                            <div class="clearfix"></div><br>
		
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="address">ADDRESS</label></span>
		                        <input name="address" id="address" type="text" class="form-control" placeholder="Address">
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="phone">PHONE</label></span>
		                        <input name="phone" id="phone" type="tel" class="form-control" placeholder="Phone number">
		                    </div>
		                    <div class="clearfix"></div>
		
		           	 	</form>
	                </div>
	                <div class="modal-footer">
	                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
	                    <a href="#" class="btn btn-primary" onclick="submit();">SAVE</a>
	                </div>
	            </div>
	        </div>
	    </div>
	    
	    <div class="modal fade" id="editBranch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">×</button>
	                    <h3>Edit Branch</h3>
	                </div>
	                <div class="modal-body">
	                    <form class="form-horizontal" action="" method="post">
                    		<div id='edit_modal_message' class="alert alert-danger" style="display: none;"></div>
	                    		
                    		<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_name">NAME</label></span>
		                        <input name="edit_name" id="edit_name" type="text" class="form-control" placeholder="Name">
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
                                <span class="input-group-addon"><label for="edit_code">PREFIX</label></span>
                                <input name="edit_code" id="edit_code" type="text" class="form-control" placeholder="Prefix">
                            </div>
                            <div class="clearfix"></div><br>
		
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_address">ADDRESS</label></span>
		                        <input name="edit_address" id="edit_address" type="text" class="form-control" placeholder="Address">
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_phone">PHONE</label></span>
		                        <input name="edit_phone" id="edit_phone" type="tel" class="form-control" placeholder="Phone number">
		                    </div>
		                    <div class="clearfix"></div>
							<input name="edit_id_branch" id="edit_id_branch" type="hidden" >
		           	 	</form>
	                </div>
	                <div class="modal-footer">
	                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
	                    <a href="#" class="btn btn-primary" onclick="update();">UPDATE</a>
	                </div>
	            </div>
	        </div>
	    </div>
    