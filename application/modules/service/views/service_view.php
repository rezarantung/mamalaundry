<script>
	var table

	$(document).ready(function(){
		table = $('#service_list').DataTable({
			"processing": true,
			"serverSide": true,
	        "ajax": {
	        	'type' : 'get',
	        	'url': 'service/list_service'     
	        },
	        "columnDefs": [{
				"targets": [5],
				"orderable": false
			}],
            "order": []
	    });
	    
	    $('#price').number( true, 0 );
	    $('#wash_salary').number( true, 0 );
	    $('#iron_salary').number( true, 0 );
	    
	    $('#edit_price').number( true, 0 );
        $('#edit_wash_salary').number( true, 0 );
        $('#edit_iron_salary').number( true, 0 );
	    
	});
	
	function submit() {
		var r = confirm("Are you sure want to submit?","Ok","Cancel");
		if(r){
			var name = $('#name').val();
			var price = $('#price').val();
			var group = $('#group').val();
			var wash = $('#wash_salary').val();
			var iron = $('#iron_salary').val();
			
			$.ajax({
				type: 'post',
				url: "service/add_service", 
				data: {
					'name': name.toUpperCase(),
					'price': price,
					'group': group,
					'wash': wash,
					'iron': iron
				}, 
				success: function(response){
		        	if(response.success) {
		        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Create Service : ' + '"' + name + '"');
		        		
		        		$('#name').val('');
						$('#price').val('');
						$("#group").val('');
						$('#wash_salary').val('');
						$('#iron_salary').val('');
						
		        		$('#addService').modal('toggle');
		        		table.ajax.reload();
		        		$("#alert_service_list").html(response.message);
						$("#alert_service_list").show();
						setTimeout(function() { $("#alert_service_list").slideUp(); }, 4000);
		        	} else {
		        		$("#modal_message").html(response.message);
						$("#modal_message").show();
						setTimeout(function() { $("#modal_message").slideUp(); }, 4000);
		        	}
		    	}
		    });	
		}
	}
	
	function delete_(id, service) {
		var r = confirm("Are you sure want to delete \""+service+"\"?","Ok","Cancel");
			if(r){
				$.ajax({
					type: 'post',
					url: "service/delete_service", 
					data: { 'id': id }, 
					success: function(response){
			        	if(response.success) {
			        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Delete Service : ' + '"' + service + '"');
			        		table.ajax.reload();
			        		$("#alert_service_list").html(response.message);
							$("#alert_service_list").show();
							setTimeout(function() { $("#alert_service_list").slideUp(); }, 4000);
			        	} else {
			        		table.ajax.reload();
			        		$("#danger_service_list").html(response.message);
							$("#danger_service_list").show();
							setTimeout(function() { $("#danger_service_list").slideUp(); }, 4000);
			        	}
			    	}
			    });
			}
	}	
	
	function edit_(id) {
		$('#editService').modal('toggle');
		$.ajax({
			type: 'post',
			url: "service/get_service", 
			data: { 'id': id }, 
			success: function(response){
	        	if(response.success) {
	        		$('#edit_name').val(response.data[0].name);
					$('#edit_price').val(response.data[0].price);
					$('#edit_group').val(response.data[0].group);
					$('#edit_wash_salary').val(response.data[0].wash_salary);
					$('#edit_iron_salary').val(response.data[0].iron_salary);
					$('#edit_id_service').val(id);
	        	} else {
	        		console.log(response.message);
	        	}
	    	}
	    });
	}	
	
	function update() {
		var r = confirm("Are you sure want to update?","Ok","Cancel");
		if(r){
			var name = $('#edit_name').val();
			var price = $('#edit_price').val();
			var id = $('#edit_id_service').val();
			var group = $('#edit_group').val();
			var wash = $('#edit_wash_salary').val();
			var iron = $('#edit_iron_salary').val();
			
			$.ajax({
				type: 'post',
				url: "service/update_service", 
				data: {
					'id': id,
					'name': name.toUpperCase(),
					'price': price,
					'group': group,
					'wash': wash,
					'iron': iron
				}, 
				success: function(response){
		        	if(response.success) {
		        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Update Service : ' + '"' + name + '"');
		        		
		        		$('#edit_name').val('');
						$('#edit_price').val('');
						$('#edit_group').val('');
						$('#edit_wash_salary').val('');
						$('#edit_iron_salary').val('');	
		        		
		        		$('#editService').modal('toggle');
		        		table.ajax.reload();
		        		$("#alert_service_list").html(response.message);
						$("#alert_service_list").show();
						setTimeout(function() { $("#alert_service_list").slideUp(); }, 4000);
		        	} else {
		        		$("#edit_modal_message").html(response.message);
						$("#edit_modal_message").show();
						setTimeout(function() { $("#edit_modal_message").slideUp(); }, 4000);
		        	}
		    	}
		    });	
		}
	}

</script>
<style type="text/css">
    input[type=text], input[type=tel], input[type=hidden], textarea {
        text-transform: uppercase !important;
    }
    
    input[type=number] {
        -moz-appearance:textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    
    .input-group-addon {
        min-width: 150px;
        text-align: left;
        font-weight: lighter;
    }
</style>
<div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Service</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Service List</h2>
				<div class="pull-right">
					<a class="btn btn-primary btn-xs" href="#" data-toggle="modal" data-target="#addService"><i class="glyphicon glyphicon-plus" data-toggle="tooltip" title="" data-original-title="Add"></i> Add</a>
                </div>
            </div>
            <div id="alert_service_list" class="alert alert-success" style="display: none; text-align: center;"></div>
            <div id="danger_service_list" class="alert alert-danger" style="display: none; text-align: center;"></div>
            <div class="box-content row">
            	<div class="col-lg-12 col-md-12">
            		*GROUP 1 : <strong>Iron only</strong><br/>
            		*GROUP 2 : <strong>Wash only</strong><br/>
            		*GROUP 3 : <strong>Not paid</strong><br/>
            		*GROUP 4 : <strong>Wash and Iron</strong><br/>
        		</div>
        		<div class="clearfix"></div><br>
                <div class="col-lg-12 col-md-12">
                    <table id="service_list" width="100%" class="table table-striped">
                        <thead>
                        	<tr>
                        		<th>NAME</th>
                        		<th>PRICE</th>
                        		<th>*GROUP</th>
                        		<th>WASH SALARY</th>
                        		<th>IRON SALARY</th>
                        		<th>ACTION</th>
                    		</tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->

	<div class="modal fade" id="addService" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">×</button>
	                    <h3>Add Service</h3>
	                </div>
	                <div class="modal-body">
	                    <form class="form-horizontal" action="" method="post">
                    		<div id='modal_message' class="alert alert-danger" style="display: none;"></div>
	                    		
                    		<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="name">SERVICE</label></span>
		                        <input name="name" id="name" type="text" class="form-control" placeholder="Service Name">
		                    </div>
		                    <div class="clearfix"></div><br>
		
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="price">PRICE</label></span>
		                        <input name="price" id="price" type="text" step="500" class="form-control" placeholder="Set Price">
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="group">GROUP</label></span>
		                        <select name="group" id="group" class="form-control">
		                        	<option value="">-- Select Group --</option>
		                        	<option value="1">GROUP 1</option>
		                        	<option value="2">GROUP 2</option>
		                        	<option value="3">GROUP 3</option>
		                        	<option value="4">GROUP 4</option>
		                        </select>
		                    </div>
		                    <div class="clearfix"></div><br/>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="wash_salary">WASH SALARY</label></span>
		                        <input name="wash_salary" id="wash_salary" type="text" step="50" class="form-control" placeholder="Set Wash Salary">
		                    </div>
		                    <div class="clearfix"></div><br>
							
							<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="iron_salary">IRON SALARY</label></span>
		                        <input name="iron_salary" id="iron_salary" type="text" step="50" class="form-control" placeholder="Set Iron Salary">
		                    </div>
		                    <div class="clearfix"></div>
		           	 	</form>
	                </div>
	                <div class="modal-footer">
	                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
	                    <a href="#" class="btn btn-primary" onclick="submit();">SAVE</a>
	                </div>
	            </div>
	        </div>
	    </div>
	    
	    <div class="modal fade" id="editService" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">×</button>
	                    <h3>Edit Service</h3>
	                </div>
	                <div class="modal-body">
	                    <form class="form-horizontal" action="" method="post">
                    		<div id='edit_modal_message' class="alert alert-danger" style="display: none;"></div>
	                    		
                    		<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_name">SERVICE</label></span>
		                        <input name="edit_name" id="edit_name" type="text" class="form-control" placeholder="Service Name">
		                    </div>
		                    <div class="clearfix"></div><br>
		
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_price">PRICE</label></span>
		                        <input name="edit_price" id="edit_price" type="text" step="500" class="form-control" placeholder="Set Price">
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_group">GROUP</label></span>
		                        <select name="edit_group" id="edit_group" class="form-control">
		                        	<option value="">-- Select Group --</option>
		                        	<option value="1">GROUP 1</option>
		                        	<option value="2">GROUP 2</option>
		                        	<option value="3">GROUP 3</option>
		                        	<option value="4">GROUP 4</option>
		                        </select>
		                    </div>
		                    <div class="clearfix"></div><br/>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_wash_salary">WASH SALARY</label></span>
		                        <input name="edit_wash_salary" id="edit_wash_salary" type="text" step="50" class="form-control" placeholder="Set Wash Salary">
		                    </div>
		                    <div class="clearfix"></div><br>
							
							<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_iron_salary">IRON SALARY</label></span>
		                        <input name="edit_iron_salary" id="edit_iron_salary" type="text" step="50" class="form-control" placeholder="Set Iron Salary">
		                    </div>
		                    <div class="clearfix"></div>
							<input name="edit_id_service" id="edit_id_service" type="hidden" >
		           	 	</form>
	                </div>
	                <div class="modal-footer">
	                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
	                    <a href="#" class="btn btn-primary" onclick="update();">UPDATE</a>
	                </div>
	            </div>
	        </div>
	    </div>
    