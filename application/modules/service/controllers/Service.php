<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {
  public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('role') != 0) { redirect('dashboard'); }		
		$this->load->model('service_model');

	}
	
	public function index()
	{
		$this->template->load('maintemplate', 'service/views/service_view');
	}
	
	public function get_service() {
		$id = $this->input->post('id');
		$data = $this->service_model->getService($id);
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function list_service() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 2;}; 
		$order_fields = array('name', 'price', '`group`', 'wash_salary', 'iron_salary');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->service_model->list_service($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			$wash_salary = '-';
			if($v['wash_salary'] == null || $v['wash_salary'] == 0) {
				$wash_salary = '-';
			} else {
				$wash_salary = 'Rp.'.number_format($v['wash_salary']);
			}
			$iron_salary = '-';
			if($v['iron_salary'] == null || $v['iron_salary'] == 0) {
				$iron_salary = '-';
			} else {
				$iron_salary = 'Rp.'.number_format($v['iron_salary']);
			}
			array_push($data, 
				array(
					$v['name'],
					'Rp.'.number_format($v['price']),
					'GROUP '.$v['group'],
					$wash_salary,
					$iron_salary,
					'<a class="btn btn-primary btn-xs" href="#" title"Edit" onclick="edit_('.$v['id_service'].')"><i class="glyphicon glyphicon-edit"></i></a>'
					.' '.
					'<a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_('.$v['id_service'].', \''.$v['name'].'\')"><i class="glyphicon glyphicon-trash"></i></a>'
					
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_service() {
	    $group_ = $this->input->post('group');
        
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Service Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required|numeric');
		$this->form_validation->set_rules('group', 'Group', 'required');
        
        switch ($group_) {
            case 1:
                $this->form_validation->set_rules('wash', 'Wash Salary', 'numeric');
                $this->form_validation->set_rules('iron', 'Iron Salary', 'required|numeric');
                break;
            case 2:
                $this->form_validation->set_rules('wash', 'Wash Salary', 'required|numeric');
                $this->form_validation->set_rules('iron', 'Iron Salary', 'numeric');
                break;
            case 3:
                $this->form_validation->set_rules('wash', 'Wash Salary', 'numeric');
                $this->form_validation->set_rules('iron', 'Iron Salary', 'numeric');
                break;
            case 4:
                $this->form_validation->set_rules('wash', 'Wash Salary', 'required|numeric');
                $this->form_validation->set_rules('iron', 'Iron Salary', 'required|numeric');
                break;
        }

        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $name = $this->input->post('name');
            $price = $this->input->post('price');
			$group = $this->input->post('group');
			$wash = $this->input->post('wash');
			$iron = $this->input->post('iron');
			
			$exist = $this->service_model->check_existing($name);
			if($exist['total'] != 0) {
				$result = array(
					'success' => false,
					'message' => $name.' is already exist!'
				);
			} else {
				$do_submit = $this->service_model->add($name, $price, $group, $wash, $iron);
	            if ($do_submit) {
	            	$result = array(
						'success' => true,
						'message' => 'Service added!'
					);
	            } else {
	                $result = array(
						'success' => false,
						'message' => 'Failed to add new service!'
					);
	            }
			}
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function update_service() {
	    $group_ = $this->input->post('group');
        
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Service Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required|numeric');
		$this->form_validation->set_rules('group', 'Group', 'required');
		
        switch ($group_) {
            case 1:
                $this->form_validation->set_rules('wash', 'Wash Salary', 'numeric');
                $this->form_validation->set_rules('iron', 'Iron Salary', 'required|numeric');
                break;
            case 2:
                $this->form_validation->set_rules('wash', 'Wash Salary', 'required|numeric');
                $this->form_validation->set_rules('iron', 'Iron Salary', 'numeric');
                break;
            case 3:
                $this->form_validation->set_rules('wash', 'Wash Salary', 'numeric');
                $this->form_validation->set_rules('iron', 'Iron Salary', 'numeric');
                break;
            case 4:
                $this->form_validation->set_rules('wash', 'Wash Salary', 'required|numeric');
                $this->form_validation->set_rules('iron', 'Iron Salary', 'required|numeric');
                break;
        }

        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $name = $this->input->post('name');
            $price = $this->input->post('price');
			$group = $this->input->post('group');
			$wash = $this->input->post('wash');
			$iron = $this->input->post('iron');
            $id = $this->input->post('id');
			
			// $exist = $this->service_model->check_existing($name);
			// if($exist['total'] != 0) {
				// $result = array(
					// 'success' => false,
					// 'message' => $name.' is already exist!'
				// );
			// } else {
				$do_update = $this->service_model->update($name, $price, $group, $wash, $iron, $id);
	            if ($do_update) {
	            	$result = array(
						'success' => true,
						'message' => 'Service updated!'
					);
	            } else {
	                $result = array(
						'success' => false,
						'message' => 'Failed to update service!'
					);
	            }
			//}
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function delete_service() {
        
        $id = $this->input->post('id');
        
        $do_delete = $this->service_model->delete($id);
        if ($do_delete) {
        	$result = array(
				'success' => true,
				'message' => 'Service deleted!'
			);
        } else {
            $result = array(
				'success' => false,
				'message' => 'Failed to delete service!'
			);
        }
        
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
}
