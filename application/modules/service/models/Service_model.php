<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function check_existing($name) { 
		$branch = $this->session->userdata('id_branch');	
		$sql 	= '	SELECT COUNT(*) AS total
					FROM service
					WHERE name = \''. $name .'\' AND id_branch = '.$branch;
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->row_array(); 
		
		$return = array(
			'total' => $result['total'],
		);
		
		return $return;
	}
	
	public function getService($id) {
		$sql = "SELECT *
				FROM service
				WHERE id_service = ".$id;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function list_service($params = array()) { 
		$branch = $this->session->userdata('id_branch');
        	
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND name LIKE \'%'.$params['filter'].'%\'';
		}
		
		$sql 	= '	SELECT *
					FROM service
					WHERE 1=1 AND id_branch = '.$branch.' '.$where.'
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM service
									WHERE 1=1 AND id_branch = '.$branch.' '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total'],
		);
		
		return $return;
	}
 	
	public function add($name, $price, $group, $wash, $iron) {
	    $branch = $this->session->userdata('id_branch');
		$fwash = 'null';	
		$firon = 'null';
		if(!empty($wash) || $wash != 0) {
			$fwash = $wash;
		}
		if(!empty($iron) || $iron != 0) {
			$firon = $iron;
		}
        
        switch ($group) {
            case 1:
                $fwash = 'null';
                break;
            case 2:
                $firon = 'null';
                break;
            case 3:
                $fwash = 'null';    
                $firon = 'null';
                break;
            case 4:
                //Nothing
                break;
        }
		
		$sql = "INSERT INTO service (id_branch, name, price, `group`, wash_salary, iron_salary)
				VALUES (".$branch.", '".$name."', '".$price."', ".$group.", '".$fwash."', '".$firon."')";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}	
	
	public function update($name, $price, $group, $wash, $iron, $id) {
	    $fwash = 'null';	
		$firon = 'null';
		if(!empty($wash) || $wash != 0) {
			$fwash = $wash;
		}
		if(!empty($iron) || $iron != 0) {
			$firon = $iron;
		}
        
        switch ($group) {
            case 1:
                $fwash = 'null';
                break;
            case 2:
                $firon = 'null';
                break;
            case 3:
                $fwash = 'null';    
                $firon = 'null';
                break;
            case 4:
                //Nothing
                break;
        }
		
		$sql = "UPDATE service
				SET name = '".$name."', price = ".$price.", `group` = ".$group.", wash_salary = ".$fwash.", iron_salary = ".$firon."
				WHERE id_service = ". $id;
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
	
	public function delete($id) {
		$sql = "DELETE FROM service WHERE id_service = ". $id;
 		$delete = $this->db->query($sql);
		if($delete) {
			return true; 
		} else {
			return false; 
		}
	}

}	
