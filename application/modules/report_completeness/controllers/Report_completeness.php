<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Report_completeness extends CI_Controller {
  public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('role') == 2) { redirect('dashboard'); }
				
		$this->load->model('report_completeness_model');

	}
	
	public function index()
	{
		$branch = $this->report_completeness_model->getBranch();
        $branch_report = $this->report_completeness_model->getBranchReport();
		
		$options = '<option value="">All</option>';
		foreach ($branch as $list) {
			$options .= '<option value="'.$list['id_branch'].'">'.$list['name'].'</option>';
		}
		$data['branch'] = $options;
        $data['branch_report'] = count($branch_report) > 1 ? 'All' : $branch_report[0]['name'];
		
		$this->template->load('maintemplate', 'report_completeness/views/report_completeness_view', $data);
	}
	
	public function list_completeness() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 1;}; 
		$order_fields = array('order_date', 'a.invoice_number', 'customer_name', 'service_name', 'quantity', 'c.price', 'grand', 'd.net_price', 'payment_date');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_column == 1 ? "MAX(CAST(SUBSTRING(".$order_fields[$order_column].",LOCATE('-',".$order_fields[$order_column].")+1) AS SIGNED))" : $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->report_completeness_model->list_completeness($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
		    
            $payment = date('d M Y', strtotime($v['payment_date']));
            if($v['payment_date'] == null || $v['payment_date'] == '0000-00-00') {
                $payment = '-';
            }
            
			array_push($data, 
				array(
					date('d M Y', strtotime($v['order_date'])),
					$v['invoice_number'],
					'<a style="cursor: pointer;" title"Detail" onclick="customer_datail(\''.$v['customer_phone'].'\', \''.$v['address'].'\', \''.$v['customer_name'].'\')">'.$v['customer_name'].'</a>',
					//$v['customer_phone'],
					$v['service_name'],
					$v['quantity'],
					'Rp.'.number_format($v['price']),
					'Rp.'.number_format($v['quantity'] * $v['price']),
					'Rp.'.number_format($v['net_price']),
					$payment
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function list_completeness_filter() {
	    $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');
		$branch = $this->input->post('branch');
		
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
        if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
        if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 1;}; 
        $order_fields = array('order_date', 'a.invoice_number', 'customer_name', 'service_name', 'quantity', 'c.price', 'grand', 'd.net_price', 'payment_date');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_column == 1 ? "MAX(CAST(SUBSTRING(".$order_fields[$order_column].",LOCATE('-',".$order_fields[$order_column].")+1) AS SIGNED))" : $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
        $params['start']        = $startdate != '' ? date('Y-m-d', strtotime($startdate)) : '';
        $params['end']          = $enddate != '' ? date('Y-m-d', strtotime($enddate)) : '';
		$params['branch'] 		= $branch;
		
		$list = $this->report_completeness_model->list_completeness_filter($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
		    
            $payment = date('d M Y', strtotime($v['payment_date']));
            if($v['payment_date'] == null || $v['payment_date'] == '0000-00-00') {
                $payment = '-';
            }
            
			array_push($data, 
                array(
                    date('d M Y', strtotime($v['order_date'])),
                    $v['invoice_number'],
                    '<a style="cursor: pointer;" title"Detail" onclick="customer_datail(\''.$v['customer_phone'].'\', \''.$v['address'].'\', \''.$v['customer_name'].'\')">'.$v['customer_name'].'</a>',
                    //$v['customer_phone'],
                    $v['service_name'],
                    $v['quantity'],
                    'Rp.'.number_format($v['price']),
                    'Rp.'.number_format($v['quantity'] * $v['price']),
                    'Rp.'.number_format($v['net_price']),
                    $payment
                )
            );
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

    public function data_filter() {
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $branch = $this->input->get('branch');
        
        $branch_name = 'All';
        
        $id_branch = $this->session->userdata('id_branch_emp'); 
        if($this->session->userdata('role') != 0) {
             $branch = $id_branch;
        }
        
        if($branch != '') {
            $get_branch_name = $this->report_completeness_model->getBranchName($branch);  
            $branch_name = $get_branch_name[0]['name']; 
        }
        
        $result = array(
            'start' => date("d M Y", strtotime($startdate)),
            'end' => date("d M Y", strtotime($enddate)),
            'branch' => $branch_name
        );
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    public function list_completeness_pdf() {
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $branch = $this->input->get('branch');
        
        $params['start']        = $startdate != '' || $startdate != null ? date('Y-m-d', strtotime($startdate)) : '' ;
        $params['end']          = $enddate != '' || $enddate != null ? date('Y-m-d', strtotime($enddate)) : '' ;
        $params['branch']       = $branch;
        
        $list = $this->report_completeness_model->list_completeness_pdf($params);
        
        $data = array();            
        foreach ( $list['data'] as $k => $v ) {
            
            $payment = date('d M Y', strtotime($v['payment_date']));
            if($v['payment_date'] == null || $v['payment_date'] == '0000-00-00') {
                $payment = '-';
            }
            
            array_push($data, 
                array(
                    $v['invoice_number'],
                    $v['customer_name'],
                    $v['customer_phone'],
                    $v['service_name'],
                    $v['quantity'],
                    number_format($v['price']),
                    number_format($v['net_price']),
                    $payment,
                    date('d M Y', strtotime($v['order_date']))
                )
            );
        }
        
        $result["data"] = $data;
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
