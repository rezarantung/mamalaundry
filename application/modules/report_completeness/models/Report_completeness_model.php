<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Report_completeness_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function list_completeness($params = array()) {
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND b.id_branch = '.$this->session->userdata('id_branch');
		if($this->session->userdata('role') != 0) {
			 $branch .= ' AND a.id_branch = '.$id_branch;
		}
        
        if($this->session->userdata('role') == 0) {
            $branch = '';  
        }
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (a.invoice_number LIKE \'%'.$params['filter'].'%\' OR b.name LIKE \'%'.$params['filter'].'%\' OR b.customer_phone LIKE \'%'.$params['filter'].'%\' OR c.name LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT a.*, d.invoice_number, b.name AS customer_name, b.*, c.name AS service_name, c.price, d.net_price, d.payment_date, d.net_price, (a.quantity * c.price) AS grand
                    FROM `order` a
                    LEFT JOIN customer b ON a.customer_phone = b.customer_phone
                    LEFT JOIN service c ON a.id_service = c.id_service
                    LEFT JOIN invoice d ON a.invoice_number = d.invoice_number
					WHERE 1=1 '.$where.' '.$branch.' AND (d.pickup_date IS NULL OR d.pickup_date = \'0000-00-00\') AND (a.packing_date IS NOT NULL AND a.packing_date != \'0000-00-00\')
					GROUP BY a.id_order
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM `order` a
                                    LEFT JOIN customer b ON a.customer_phone = b.customer_phone
                                    LEFT JOIN service c ON a.id_service = c.id_service
                                    LEFT JOIN invoice d ON a.invoice_number = d.invoice_number
									WHERE 1=1 AND (d.pickup_date IS NULL OR d.pickup_date = \'0000-00-00\') AND (a.packing_date IS NOT NULL AND a.packing_date != \'0000-00-00\') '.$branch.' '.$where.'
									GROUP BY a.id_order')->num_rows();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total,
			'total' => $total
		);
		
		return $return;
	}

	public function list_completeness_filter($params = array()) {
		$id_branch = $this->session->userdata('id_branch');   
        
        $branch = '';
        if($this->session->userdata('role') != 0) {
             $branch = 'AND a.id_branch = '.$id_branch;
        }
        
		if(!empty($params['branch'])) {
			 $branch = 'AND a.id_branch = '.$params['branch'];
		}
        
        $filter = '';
        if(!empty($params['start']) && !empty($params['end'])) {
            if(strtotime($params['start']) <= strtotime($params['end'])) {
                $fstartdate = $params['start'];
                $fenddate = $params['end'];
                
                $filter = 'AND d.invoice_issue_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND d.invoice_issue_date = \''.$fstartdate.'\'';
                }   
            }
        }
		
		$where = '';
        if(!empty($params['filter'])) {
            $where = 'AND (a.invoice_number LIKE \'%'.$params['filter'].'%\' OR b.name LIKE \'%'.$params['filter'].'%\' OR b.customer_phone LIKE \'%'.$params['filter'].'%\' OR c.name LIKE \'%'.$params['filter'].'%\')';
        }
		
		$sql 	= '	SELECT a.*, d.invoice_number, b.name AS customer_name, b.*, c.name AS service_name, c.price, d.net_price, d.payment_date, d.net_price, (a.quantity * c.price) AS grand
                    FROM `order` a
                    LEFT JOIN customer b ON a.customer_phone = b.customer_phone
                    LEFT JOIN service c ON a.id_service = c.id_service
                    LEFT JOIN invoice d ON a.invoice_number = d.invoice_number
                    WHERE 1=1 '.$filter.' '.$where.' '.$branch.' AND (d.pickup_date IS NULL OR d.pickup_date = \'0000-00-00\') AND (a.packing_date IS NOT NULL AND a.packing_date != \'0000-00-00\')
					GROUP BY a.id_order
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM `order` a
                                    LEFT JOIN customer b ON a.customer_phone = b.customer_phone
                                    LEFT JOIN service c ON a.id_service = c.id_service
                                    LEFT JOIN invoice d ON a.invoice_number = d.invoice_number
                                    WHERE 1=1 AND (d.pickup_date IS NULL OR d.pickup_date = \'0000-00-00\') AND (a.packing_date IS NOT NULL AND a.packing_date != \'0000-00-00\') '.$filter.' '.$branch.' '.$where.'
                                    GROUP BY a.id_order')->num_rows();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total,
			'total' => $total
		);
		
		return $return;
	}
	
	public function getBranch() {
		$sql = "SELECT * FROM branch";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
    
    public function getBranchReport() {
        $id_branch = $this->session->userdata('id_branch'); 
        
        $branch = '';
        if($this->session->userdata('role') != 0) {
             $branch = 'WHERE id_branch = '.$id_branch;
        }
        
        $sql = "SELECT name FROM branch ". $branch;
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
    
    public function getBranchName($branch) {
        $sql = "SELECT name FROM branch WHERE id_branch = ".$branch;
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
    
    public function list_completeness_pdf($params = array()) {
        $id_branch = $this->session->userdata('id_branch');   
        
        $branch = 'AND b.id_branch = '.$this->session->userdata('id_branch');
        if($this->session->userdata('role') != 0) {
             $branch .= ' AND a.id_branch = '.$id_branch;
        }
        
        if($this->session->userdata('role') == 0) {
            $branch = '';  
        }
        
        if(!empty($params['branch'])) {
             $branch = 'AND a.id_branch = '.$params['branch'];
        }
        
        $filter = '';
        if(!empty($params['start']) && !empty($params['end'])) {
            if(strtotime($params['start']) <= strtotime($params['end'])) {
                $fstartdate = $params['start'];
                $fenddate = $params['end'];
                
                $filter = 'AND d.invoice_issue_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND d.invoice_issue_date = \''.$fstartdate.'\'';
                }   
            }
        }
        
        $sql    = ' SELECT a.*, d.invoice_number, b.name AS customer_name, b.*, c.name AS service_name, c.price, d.net_price, d.payment_date, d.net_price, (a.quantity * c.price) AS grand
                    FROM `order` a
                    LEFT JOIN customer b ON a.customer_phone = b.customer_phone
                    LEFT JOIN service c ON a.id_service = c.id_service
                    LEFT JOIN invoice d ON a.invoice_number = d.invoice_number
                    WHERE 1=1 '.$filter.' '.$branch.' AND (d.pickup_date IS NULL OR d.pickup_date = \'0000-00-00\') AND (a.packing_date IS NOT NULL AND a.packing_date != \'0000-00-00\')
                    GROUP BY a.id_order
                    ORDER BY MAX(CAST(SUBSTRING(a.invoice_number,LOCATE("-",a.invoice_number)+1) AS SIGNED)) ASC';
        
        
        $query  =  $this->db->query($sql);
        $result = $query->result_array(); 
        
        $return = array(
            'data' => $result
        );
        
        return $return;
    }
}	
