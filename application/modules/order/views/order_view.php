<script>
	$(function() {
		$( "#service_" ).autocomplete({
	      source: function(request, response) {
	        $.ajax({
	        	type: 'get',
	          	url: "order/get_service",
	          	dataType: "json",
	          	data: {
				   	service: request.term
				},
				success: function( data ) {
					response($.map( data, function( item ) {
						return {
							label: item[1],
							value: item[1],
							id: item[0],
							group: item[2],
							price: item[3],
							wash_salary: item[4],
							iron_salary: item[5]
						}
					}));
				}
	        });
	      },
	      minLength: 1,
	      select: function( event, ui ) {
	      	$('#id_service').val(ui.item.id);
			$('#typegroup_').val(ui.item.group);
			$('#price_').val(ui.item.price);
			$('#washsalary_').val(ui.item.wash_salary);
			$('#ironsalary_').val(ui.item.iron_salary);
	      },
	      appendTo: ".pmodal"
	    });
	});

	var table;
	var date_issue;

	$(document).ready(function(){
		table = $('#order_list').DataTable({
			"processing": true,
			"serverSide": true,
	        "ajax": {
	        	'type' : 'get',
	        	'url': 'order/list_order'     
	        },
	        "columnDefs": [{
				"targets": [6],
				"orderable": false
			}],
            "order": []
	    });
	    
	    $('#startdate').datetimepicker({
	    	format:'d-m-Y',
	    	timepicker: false,
	    	scrollInput : false
	    });
	    $('#enddate').datetimepicker({
	    	format:'d-m-Y',
	    	timepicker: false,
	    	scrollInput : false
	    });
	    
	    $('#packdate').datetimepicker({
	    	format:'d-m-Y',
	    	timepicker: false,
	    	scrollInput : false
	    }); 
	});
	
	function packed_(id, invoice, service) {
		var r = confirm("Are you sure?","Ok","Cancel");
		if(r){
			logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Packing order with invoice number : '+ invoice + ' - ' + service);
			
			$.ajax({
				type: 'post',
				url: "order/update_packing", 
				data: { 
					'id': id,
					'invoice': invoice
				}, 
				success: function(response){
		        	if(response.success) {
		        		table.ajax.reload();
		        		$("#alert_order_list").html(response.message);
						$("#alert_order_list").show();
						setTimeout(function() { $("#alert_order_list").slideUp(); }, 4000);
		        	} else {
		        		$("#danger_list").html(response.message);
						$("#danger_list").show();
						setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
		        	}
		    	}
		    });	
		}
	}
	
	function filter() {
		var start = $('#startdate').val();
		var end = $('#enddate').val();
		var category = $('#category').val();
		var qty = $('.fqty_filter').val();
		
		var x = start.split('-');
        var y = end.split('-');
        var rstart = x[2]+'-'+x[1]+'-'+x[0];
        var rend = y[2]+'-'+y[1]+'-'+y[0];
		
		if(start != '' || end != '') {
            if(start == '' || end == '') {
                $("#danger_list").html('Please check your input!');
                $("#danger_list").show();
                setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
            }
        } 
        
        if(start != '' && end != '') {
            if(Date.parse(rstart) > Date.parse(rend)) {
                $("#danger_list").html('Startdate should be lower than Enddate!');
                $("#danger_list").show();
                setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
            }
        } 
		
		$('#order_list').DataTable().destroy();
		table = $('#order_list').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
	        	'type' : 'post',
	        	'url': 'order/list_order_filter', 
	        	'data': {
					'startdate': start,
					'enddate': end,
					'category': category,
					'qty': qty
				}     
	        },
	        "columnDefs": [{
				"targets": [6],
				"orderable": false
			}],
            "order": []
	    });
	}
	
	function edit_(id, old_price, invoice, pack, service, quantity, paid) {
	    $('#service_').val(service);
        $('#qty_').val(quantity);
        $('#id_order').val(id);
        $('#old_price').val(old_price);
        $('#invoice_').val(invoice);
        $('#packdate').val(pack);
	    
	    if(paid === 1) {
	        var r = confirm("The order is already paid. Are you sure want to edit?","Ok","Cancel");
            if(r){
                $('#editOrder').modal('toggle');
            }
	    } else {
	        $('#editOrder').modal('toggle');
	    }
	}
	
	function UpdateOrder() {
		var r = confirm("Are you sure want to update order?","Ok","Cancel");
		if(r){
		    var order_id = $('#id_order').val();
			var service_id = $('#id_service').val();
			var quatity = $('#qty_').val();
			var typegroup = $('#typegroup_').val();
			var wash_salary = $('#washsalary_').val();
			var iron_salary = $('#ironsalary_').val();
			var price = $('#price_').val();
			var old_price = $('#old_price').val();
			var invoice = $('#invoice_').val();
			var packdate = $('#packdate').val();
			
			logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Update order with invoice number: ' + invoice);
			
			if(wash_salary == null || wash_salary == 0 || wash_salary == '') {
				wash_salary = 0;
			}
			if(iron_salary == null || iron_salary == 0 || iron_salary == '') {
				iron_salary = 0;
			}
			
			var success = 0;
				
			if((service_id != '' && quatity != '')) {
				fqty = quatity;
				if(quatity < 1 && typegroup != 3 && quatity != '') {
					fqty = 1;
				}
				
				if(service_id != '' && quatity != '') {
					
					price = price * fqty;
					
					if(wash_salary != null || wash_salary != '') {
						wash_salary = wash_salary * fqty;
					}
					if(iron_salary != null || iron_salary != '') {
						iron_salary = iron_salary * fqty;
					}
					
					//----------------------------------------------------------------------------------invoice
				    var net_price = 0;
				    
				    
					$.ajax({
						type: 'get',
						url: "order/get_netPrice", 
						data: {
							'invoice': invoice
						},
						success : function(response) {
							net_price = response.data[0].net_price - old_price;
							net_price = net_price + price;
						},
						async: false
				    });
				    
				    net_price = net_price/100;
				    if(net_price < 50) {
				    	net_price = Math.floor(net_price);
				    } else {
				    	net_price = Math.round(net_price);
				    }
				    net_price = net_price*100;
				    
				    $.ajax({
						type: 'post',
						url: "order/update_invoice", 
						data: {
							'net_price': net_price,
							'invoice_number': invoice
						},
						success: function(response){
				        	if(response.success) {
				        		success++;
				        	}
				   		 },
						async: false
				    });
						
					//----------------------------------------------------------------------------------order
					$.ajax({
						type: 'post',
						url: "order/update_order", 
						data: {
							'id_order': order_id,
							'id_service': service_id,
							'quantity': quatity,
							'price': price,
							'wash_salary': wash_salary,
							'iron_salary': iron_salary
						},
						success: function(response){
				        	if(response.success) {
				        		success++;
				        	}
				    },
						async: false
				    });
				    price = 0;
				}			
			}
			
			$.ajax({
				type: 'post',
				url: "order/edit_packing", 
				data: { 
					'id': order_id,
					'packdate': packdate,
					'invoice': invoice
				}, 
				success: function(response){
		        	if(response.success) {
		        		success++;
		        	}
		    	},
				async: false
		    });	
			
			if(success >= 1) {
				$('#editOrder').modal('toggle');
	    		table.ajax.reload();
	    		$("#alert_order_list").html('Order Updated!');
				$("#alert_order_list").show();
				setTimeout(function() { $("#alert_order_list").slideUp(); }, 4000);
			}
			success = 0;
		}
		
	}
	
	function qty_validation() {
        var value = $('#qty_').val();
        var i = value.split('.');
        
        if(i[1].length <= 3) {
            $('#qty_validation_').val(value);
        }
 
        valid();    
    }
    
    function valid() {
        var y = $('#qty_validation_').val();
        $('#qty_').val(y);
    }
    
    function qty_validation_filter() {
        var value = $('#qty_filter').val();
        var i = value.split('.');
        
        if(i[1].length <= 3) {
            $('#qty_validation_filter').val(value);
        }
 
        valid_filter();    
    }
    
    function valid_filter() {
        var y = $('#qty_validation_filter').val();
        $('#qty_filter').val(y);
    }
</script>
<style type="text/css">
    input[type=text], input[type=tel], input[type=hidden], textarea {
        text-transform: uppercase !important;
    }
    
    input[type=number] {
        -moz-appearance:textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
<div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Order</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Customer Order</h2>
            </div>
            <div id="danger_list" class="alert alert-danger" style="display: none; text-align: center;"></div>
            <div id="alert_order_list" class="alert alert-success" style="display: none; text-align: center;"></div>
            <div class="box-content row">
            	<div class="col-lg-12 col-md-12">
            		
                        <div class="col-lg-2 col-md-2">
                        	<input name="startdate" id="startdate" type="text" class="form-control" placeholder="Start Date">
                        </div>
                    
                        <div class="col-lg-2 col-md-2">
                        	<input name="enddate" id="enddate" type="text" class="form-control" placeholder="End Date">
                        </div>
                        
                        <div class="col-lg-2 col-md-2">
                            <input name="qty_filter" id="qty_filter" type="number" step="0.001" oninput="qty_validation_filter();" class="fqty_filter form-control" placeholder="Qty.">
                            <input name="qty_validation_filter" id="qty_validation_filter" type="hidden">
                        </div>
                        
                        <div class="col-lg-3 col-md-3">
                            <select name="category" id="category" class="form-control">
                                <option value="0">All</option>
                                <option value="1">Express</option>
                                <option value="2">Belum dipacking</option> 
                                <!-- <option value="3">Undone</option>  -->
                            </select>
                        </div>
                        
                        <div class="col-lg-3 col-md-3">
                        	<input name="filter" id="filter" type="submit" class="form-control btn btn-primary" value="FILTER" onclick="filter();">
                        </div>
                        <div class="clearfix"></div><br>
                    
            	</div>
                <div class="col-lg-12 col-md-12">
                    <table id="order_list" width="100%" class="table table-striped">
                        <thead>
                        	<tr>
                        	    <th>ISSUE DATE</th>
                        		<th>INVOICE</th>
                        		<th>CUSTOMER</th>
                        		<th>SERVICE</th>
                        		<th>QTY.</th>
                        		<th>PACKING DATE</th>
                        		<th>ACTION</th>
                    		</tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->
    
	<div class="modal fade" id="editOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-sm">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">×</button>
	                    <h3>Edit Order</h3>
	                </div>
	                <div class="modal-body pmodal">
	            		<div id='edit_modal_message' class="alert alert-danger" style="display: none;"></div>
	            		
	            		<label class="label-control">SERVICE:</label>
	            		<input name="service_" id="service_" type="text" class="form-control" placeholder="Service">	
	            		<input name="qty_" id="qty_" type="number" step="0.001" oninput="qty_validation();" class="form-control" placeholder="Qty.">
			            <input name="qty_validation_" id="qty_validation_" type="hidden">
			            <input name="id_order" id="id_order" type="hidden">	
			            <input name="id_service" id="id_service" type="hidden">	
			            <input type="hidden" name="typegroup_" id="typegroup_">
			            <input type="hidden" name="price_" id="price_">
			            <input type="hidden" name="washsalary_" id="washsalary_">
			            <input type="hidden" name="ironsalary_" id="ironsalary_">
			            <input type="hidden" name="old_price" id="old_price">
			            <input type="hidden" name="invoice_" id="invoice_">
			            <div class="clearfix"></div><br>
			            
			            <label class="label-control">PACKING DATE:</label>
			            <input name="packdate" id="packdate" type="text" class="form-control" placeholder="Packing Date">
	                </div>
	                <div class="modal-footer">
	                    <a href="#" class="btn btn-default" data-dismiss="modal">CANCEL</a>
	                    <button class="btn btn-primary" onclick="UpdateOrder();">CHANGE</button>
	                </div>
	            </div>
	        </div>
	    </div>