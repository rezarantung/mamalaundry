<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function list_order($params = array()) {
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND a.id_branch = '.$id_branch.' AND b.id_branch = '.$id_branch;
		// if($this->session->userdata('role') != 0) {
			 // $branch = 'AND a.id_branch = '.$id_branch;
		// }
		 
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (a.invoice_number LIKE \'%'.$params['filter'].'%\' OR b.name LIKE \'%'.$params['filter'].'%\' OR c.name LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT b.name AS customer_name, a.*, c.name AS service_name, c.group, d.payment_date
					FROM `order` a
					LEFT JOIN customer b ON a.customer_phone = b.customer_phone
					LEFT JOIN service c ON a.id_service = c.id_service
					LEFT JOIN invoice d ON a.invoice_number = d.invoice_number
					WHERE 1=1 '.$where.' '.$branch.'
					GROUP BY a.id_order
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM `order` a
									LEFT JOIN customer b ON a.customer_phone = b.customer_phone
									LEFT JOIN service c ON a.id_service = c.id_service
                                    LEFT JOIN invoice d ON a.invoice_number = d.invoice_number
									WHERE 1=1 '.$branch.' '.$where.'
									GROUP BY a.id_order')->num_rows();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total,
			'total' => $total,
		);
		
		return $return;
	}

	public function list_order_filter($params = array()) {
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND a.id_branch = '.$id_branch.' AND b.id_branch = '.$id_branch;
		// if($this->session->userdata('role') != 0) {
			 // $branch = 'AND a.id_branch = '.$id_branch;
		// }
		 
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (a.invoice_number LIKE \'%'.$params['filter'].'%\' OR b.name LIKE \'%'.$params['filter'].'%\' OR c.name LIKE \'%'.$params['filter'].'%\')';
		}
		
		if(!empty($params['start']) && !empty($params['end'])) {
		    if(strtotime($params['start']) <= strtotime($params['end'])) {
                $fstartdate = $params['start'];
                $fenddate = $params['end'];
                
                $where .= 'AND a.order_date BETWEEN CAST(\''.$params['start'].'\' AS DATETIME) AND CAST(\''.$params['end'].'\' AS DATETIME)';
                if($fstartdate == $fenddate) {
                    $filter = 'AND a.order_date = \''.$fstartdate.'\'';
                }   
            }
		}
        
        $category = '';
        if($params['category'] == 1) {
            $category = 'AND c.name LIKE \'%exp%\' AND (a.packing_date IS NULL OR a.packing_date = \'0000-00-00\')';
        } else if($params['category'] == 2) {
            $category = 'AND (a.packing_date IS NULL OR a.packing_date = \'0000-00-00\')';
        } else if($params['category'] == 3) {
            $category = 'AND d.pickup_date IS NULL';
        }
        
        $qty = '';
        if($params['qty'] != null) {
            $qty = 'AND a.quantity LIKE \'%'.$params['qty'].'%\'';
        }
        
		$sql 	= '	SELECT b.name AS customer_name, a.*, c.name AS service_name, c.group, d.payment_date
					FROM `order` a
					LEFT JOIN customer b ON a.customer_phone = b.customer_phone
					LEFT JOIN service c ON a.id_service = c.id_service
					LEFT JOIN invoice d ON a.invoice_number = d.invoice_number
					WHERE 1=1 '.$where.' '.$branch.' '.$category.' '.$qty.'
					GROUP BY a.id_order
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM `order` a
									LEFT JOIN customer b ON a.customer_phone = b.customer_phone
									LEFT JOIN service c ON a.id_service = c.id_service
									LEFT JOIN invoice d ON a.invoice_number = d.invoice_number
									WHERE 1=1 '.$branch.' '.$category.' '.$qty.' '.$where.'
									GROUP BY a.id_order')->num_rows();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total,
			'total' => $total,
		);
		
		return $return;
	}
	
	public function update_packing($id) {
		
		$sql = "UPDATE `order`
				SET packing_date = NOW()
				WHERE id_order = ".$id;
 		$add = $this->db->query($sql);
        
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
    
    public function check_packing($invoice) {
        
        $sql = "SELECT COUNT(*) AS total
                FROM `order` 
                WHERE (packing_date IS NULL OR packing_date = '0000-00-00') AND invoice_number = '".$invoice."'";
        $count = $this->db->query($sql)->row_array();
        
        $return = array('total' => $count['total']);
        
        return $return;
    }

    public function done_packing($invoice) {
        
        $sql = "UPDATE invoice
                SET packing_status = 1
                WHERE invoice_number = '".$invoice."'";
        $update = $this->db->query($sql);
        
        if($update) {
            return true; 
        } else {
            return false; 
        }
    }
    
    public function undone_packing($invoice) {
        
        $sql = "UPDATE invoice
                SET packing_status = 0
                WHERE invoice_number = '".$invoice."'";
        $update = $this->db->query($sql);
        
        if($update) {
            return true; 
        } else {
            return false; 
        }
    }
	
	public function edit_packing($id, $packdate, $invoice) {
		if($packdate == 'null') {
		    $sql1 = "UPDATE invoice
            SET packing_status = 0, pickup_date = 'null'
            WHERE invoice_number = '".$invoice."'";
            $update1 = $this->db->query($sql1);
            
            $sql2 = "UPDATE `order`
                    SET packing_date = '".$packdate."'
                    WHERE id_order = ".$id;
            $update2 = $this->db->query($sql2);
            
            if($update1 && $update2) {
                return true; 
            } else {
                return false; 
            }
		} else {
		    $sql = "UPDATE `order`
                    SET packing_date = '".$packdate."'
                    WHERE id_order = ".$id;
            $update = $this->db->query($sql);
            
            if($update) {
                return true; 
            } else {
                return false; 
            }
		}
	}
	
	public function update_invoice($invoice, $net_price) {
		
		$sql = "UPDATE invoice
				SET net_price = ".$net_price."
				WHERE invoice_number = '".$invoice."'";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
	
	public function update_order($id_order, $id_service, $quantity, $price, $wash_salary, $iron_salary) {
		
		$sql = "UPDATE `order`
				SET id_service = ".$id_service.", quantity = ".$quantity.", price = ".$price.", wash_salary_emp = ".$wash_salary.", iron_salary_emp = ".$iron_salary."
				WHERE id_order = ".$id_order;
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
	
	public function get_service_list($service) {
		$sql = "SELECT *
				FROM service
				WHERE name LIKE '%".$service."%'";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function get_netprice($invoice) {
		$sql = "SELECT net_price
				FROM invoice
				WHERE invoice_number = '".$invoice."'";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
}	
