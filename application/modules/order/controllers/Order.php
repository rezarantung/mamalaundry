<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
  public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('role') == 2) { redirect('transaction'); }		
		$this->load->model('order_model');

	}
	
	public function index()
	{
		$this->template->load('maintemplate', 'order/views/order_view');
	}
	
	public function list_order() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 1;}; 
		$order_fields = array('order_date', 'a.invoice_number', 'customer_name', 'service_name', 'quantity', 'packing_date');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_column == 1 ? "MAX(CAST(SUBSTRING(".$order_fields[$order_column].",LOCATE('-',".$order_fields[$order_column].")+1) AS SIGNED))" : $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->order_model->list_order($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			$packing_date = $v['packing_date'] == '' || $v['packing_date'] == null || $v['packing_date'] == '0000-00-00' ? '' : date('d M Y', strtotime($v['packing_date']));
			
			$action = '';
			
			if($this->session->userdata('role') == 1) {
				if($packing_date == '') {
					$action .= ' '.'<a class="btn btn-primary btn-xs" href="#" title"Packed" onclick="packed_('.$v['id_order'].', \''.$v['invoice_number'].'\', \''.$v['service_name'].'\')">Packing</a>';
				}
				
			}
			
            $payment_status = 0;
            if($v['payment_date'] != null && $v['payment_date'] != '0000-00-00') {
                $payment_status = 1;
            }
            
			if($this->session->userdata('role') == 0) {
				$action .= ' '.'<a class="btn btn-info btn-xs" href="#" title"Edit" onclick="edit_('.$v['id_order'].', '.$v['price'].', \''.$v['invoice_number'].'\', \''.$packing_date.'\', \''.$v['service_name'].'\', \''.$v['quantity'].'\', '.$payment_status.')"><i class="glyphicon glyphicon-edit"></i></a>';
			}
			
			if($this->session->userdata('role') == 1) {
				if($this->session->userdata('id_branch') != $this->session->userdata('id_branch_emp')) {
					$action = '-';
				}
			}
			
			array_push($data, 
				array(
				    date('d M Y', strtotime($v['order_date'])),
				    $v['invoice_number'],
					$v['customer_name'],
					$v['service_name'],
					$v['quantity'],
					$packing_date,
					$action
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function list_order_filter() {
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');
        $category = $this->input->post('category');
        $qty = $this->input->post('qty');
		
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    =  $category == 1 || $category == 2 ? 'asc' : 'desc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 1;}; 
		$order_fields = array('order_date', 'a.invoice_number', 'customer_name', 'service_name', 'quantity', 'packing_date');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_column == 1 ? "MAX(CAST(SUBSTRING(".$order_fields[$order_column].",LOCATE('-',".$order_fields[$order_column].")+1) AS SIGNED))" : $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		$params['start'] 		= $startdate != '' ? date('Y-m-d', strtotime($startdate)) : '';
		$params['end'] 			= $enddate != '' ? date('Y-m-d', strtotime($enddate)) : '';
        $params['category']     = $category;
        $params['qty']          = $qty;
		
		$list = $this->order_model->list_order_filter($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			$packing_date = $v['packing_date'] == '' || $v['packing_date'] == null || $v['packing_date'] == '0000-00-00' ? '' : date('d M Y', strtotime($v['packing_date']));
			
			$action = '';
			
			if($this->session->userdata('role') == 1) {
				if($packing_date == '') {
					$action .= ' '.'<a class="btn btn-primary btn-xs" href="#" title"Packed" onclick="packed_('.$v['id_order'].', \''.$v['invoice_number'].'\', \''.$v['service_name'].'\')">Packing</a>';
				}
				
			}
            
            $payment_status = 0;
            if($v['payment_date'] != null && $v['payment_date'] != '0000-00-00') {
                $payment_status = 1;
            }
			
			if($this->session->userdata('role') == 0) {
				$action .= ' '.'<a class="btn btn-info btn-xs" href="#" title"Edit" onclick="edit_('.$v['id_order'].', '.$v['price'].', \''.$v['invoice_number'].'\', \''.$packing_date.'\', \''.$v['service_name'].'\', \''.$v['quantity'].'\', '.$payment_status.')"><i class="glyphicon glyphicon-edit"></i></a>';
			}
			
			if($this->session->userdata('role') == 1) {
				if($this->session->userdata('id_branch') != $this->session->userdata('id_branch_emp')) {
					$action = '-';
				}
			}
			
			array_push($data, 
				array(
				    date('d M Y', strtotime($v['order_date'])),
				    $v['invoice_number'],
					$v['customer_name'],
					$v['service_name'],
					$v['quantity'],
					$packing_date,
					$action
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function update_packing() {
        $id = $this->input->post('id');
        $invoice = $this->input->post('invoice');
        
		$do_update = $this->order_model->update_packing($id);
        $unpacked = $this->order_model->check_packing($invoice);
        
        if($unpacked['total'] == 0) {
            $packed = $this->order_model->done_packing($invoice);
        }
        
        if ($do_update) {
        	$result = array(
				'success' => true,
				'message' => 'Order update!'
			);
        } else {
            $result = array(
				'success' => false,
				'message' => 'Failed to update order!'
			);
        }
			
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function edit_packing() {
        $id = $this->input->post('id');
		$packdate = $this->input->post('packdate') != '' ? date('Y-m-d', strtotime($this->input->post('packdate'))) : '';
        $invoice = $this->input->post('invoice');
		
		if($packdate == '') { $packdate = 'null'; }
        
		$do_update = $this->order_model->edit_packing($id, $packdate, $invoice);
        $unpacked = $this->order_model->check_packing($invoice);
        
        if($unpacked['total'] == 0) {
            $packed = $this->order_model->done_packing($invoice);
        } else {
            $unpack = $this->order_model->undone_packing($invoice);
        }
        
        if ($do_update) {
        	$result = array(
				'success' => true,
				'message' => 'Order update!'
			);
        } else {
            $result = array(
				'success' => false,
				'message' => 'Failed to update order!'
			);
        }
			
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function update_invoice() {
        $invoice = $this->input->post('invoice_number');
		$net_price = $this->input->post('net_price');
		
		$do_update = $this->order_model->update_invoice($invoice, $net_price);
        if ($do_update) {
        	$result = array(
				'success' => true,
				'message' => 'Invoice update!'
			);
        } else {
            $result = array(
				'success' => false,
				'message' => 'Failed to update invoice!'
			);
        }
			
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function update_order() {
        $id_order = $this->input->post('id_order');
		$id_service = $this->input->post('id_service');
		$quantity = $this->input->post('quantity');
		$price = $this->input->post('price');
		$wash_salary = $this->input->post('wash_salary');
		$iron_salary = $this->input->post('iron_salary');
		
		$do_update = $this->order_model->update_order($id_order, $id_service, $quantity, $price, $wash_salary, $iron_salary);
        if ($do_update) {
        	$result = array(
				'success' => true,
				'message' => 'Order update!'
			);
        } else {
            $result = array(
				'success' => false,
				'message' => 'Failed to update order!'
			);
        }
			
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function get_service() {
		$service = $this->input->get('service');
		$data = $this->order_model->get_service_list($service);
		$result = array();
		foreach ( $data as $list ) {
			array_push($result, 
				array(
					$list['id_service'],
					$list['name'],
					$list['group'],
					$list['price'],
					$list['wash_salary'],
					$list['iron_salary']
				)
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function get_netPrice() {
		$invoice = $this->input->get('invoice');
		$data = $this->order_model->get_netprice($invoice);
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
