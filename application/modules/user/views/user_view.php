<script>
	var table

	$(document).ready(function(){
		table = $('#user_list').DataTable({
			"processing": true,
			"serverSide": true,
	        "ajax": {
	        	'type' : 'get',
	        	'url': 'user/list_user'     
	        },
	        "columnDefs": [{
				"targets": [2,8],
				"orderable": false
			}],
            "order": []
	    });

	});
	
	function submit() {
		var r = confirm("Are you sure want to submit?","Ok","Cancel");
		if(r){
			$employee = $('#id_employee').val();
			$username = $('#username').val();
			$password = $('#password').val();
			$rw_password = $('#rw_password').val();
			$role = $('#id_role').val();
			$branch = $('#id_branch').val();
			
			if($password != $rw_password) {
				$password = $('#password').val('');
				$rw_password = $('#rw_password').val('');
				
				$("#modal_message").html('Password confirm doesn\'t match!');
				$("#modal_message").show();
				setTimeout(function() { $("#modal_message").slideUp(); }, 4000);
			} else {
				$.ajax({
					type: 'post',
					url: "user/add_user", 
					data: {
						'username': $username.toUpperCase(),
						'password': $password,
						'employee': $employee,
						'role': $role,
						'branch': $branch
					}, 
					success: function(response){
			        	if(response.success) {
			        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Create User : ' + '"' + $username + '"');
			        		
			        		$('#id_employee').val('');
							$('#username').val('');
							$('#password').val('');
							$('#rw_password').val('');
							$('#id_role').val('');
							$('#id_branch').val('');
			        		
			        		$('#addUser').modal('toggle');
			        		table.ajax.reload();
			        		$("#alert_user_list").html(response.message);
							$("#alert_user_list").show();
							setTimeout(function() { $("#alert_user_list").slideUp(); }, 4000);
			        	} else {
			        		$("#modal_message").html(response.message);
							$("#modal_message").show();
							setTimeout(function() { $("#modal_message").slideUp(); }, 4000);
			        	}
			    	}
			    });
			}
		}
	}
	
	function delete_(id, user) {
		var r = confirm("Are you sure want to delete \""+user+"\"?","Ok","Cancel");
			if(r){
				//console.log('testtt');
				$.ajax({
					type: 'post',
					url: "user/delete_user", 
					data: { 'id': id }, 
					success: function(response){
			        	if(response.success) {
			        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Delete User : ' + '"' + user + '"');
			        		table.ajax.reload();
			        		$("#alert_user_list").html(response.message);
							$("#alert_user_list").show();
							setTimeout(function() { $("#alert_user_list").slideUp(); }, 4000);
			        	} else {
			        		table.ajax.reload();
			        		$("#danger_user_list").html(response.message);
							$("#danger_user_list").show();
							setTimeout(function() { $("#danger_user_list").slideUp(); }, 4000);
			        	}
			    	}
			    });
			}
	}	
	
	function edit_(id) {
		$.ajax({
			type: 'post',
			url: "user/get_user", 
			data: { 'id': id }, 
			success: function(response){
	        	if(response.success) {
	        		$('#edit_username').val(response.data[0].username);
					$('#edit_id_role').val(response.data[0].role);
					$('#edit_id_user').val(id);
					$('#edit_password').val(response.data[0].password.substring(32, 1000));
					$('#edit_id_branch').val(response.data[0].id_branch);
	        	} else {
	        		console.log(response.message);
	        	}
	    	},
	    	async: false
	    });
	    $('#editUser').modal('toggle');
	}	
	
	function update() {
		var r = confirm("Are you sure want to update?","Ok","Cancel");
		if(r){
			var username = $('#edit_username').val();
			var id = $('#edit_id_user').val();
			var role = $('#edit_id_role').val();
			var branch = $('#edit_id_branch').val();
			var password = $('#edit_password').val();
            //var rw_password = $('#edit_rw_password').val();
			
			//if(password != '' || rw_password != '') {
			if(password != '') {
			    // if(password != rw_password) {
			        // $('#edit_password').val('');
                    // $('#edit_rw_password').val('');
//                     
                    // $("#edit_modal_message").html('Password confirm doesn\'t match!');
                    // $("#edit_modal_message").show();
                    // setTimeout(function() { $("#edit_modal_message").slideUp(); }, 4000);
			    // } else {
			        $.ajax({
                        type: 'post',
                        url: "user/update_user", 
                        data: {
                            'id': id,
                            'username': username.toUpperCase(),
                            'role': role,
                            'branch': branch,
                            'password': password
                        }, 
                        success: function(response){
                            if(response.success) {
                                logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Update User : ' + '"' + username + '"');
                                
                                $('#edit_username').val('');
                                $('#edit_id_user').val('');
                                $('#edit_id_role').val('');
                                $('#edit_id_branch').val('');
                                $('#edit_password').val('');
                                $('#edit_rw_password').val('');
                                
                                $('#editUser').modal('toggle');
                                table.ajax.reload();
                                $("#alert_user_list").html(response.message);
                                $("#alert_user_list").show();
                                setTimeout(function() { $("#alert_user_list").slideUp(); }, 4000);
                            } else {
                                $("#edit_modal_message").html(response.message);
                                $("#edit_modal_message").show();
                                setTimeout(function() { $("#edit_modal_message").slideUp(); }, 4000);
                            }
                        }
                    }); 
			    //}
            } else {
                $.ajax({
                    type: 'post',
                    url: "user/update_user", 
                    data: {
                        'id': id,
                        'username': username.toUpperCase(),
                        'role': role,
                        'branch': branch,
                        'password': ''
                    }, 
                    success: function(response){
                        if(response.success) {
                            logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Update User : ' + '"' + username + '"');
                            
                            $('#edit_username').val('');
                            $('#edit_id_user').val('');
                            $('#edit_id_role').val('');
                            $('#edit_id_branch').val('');
                            $('#edit_password').val('');
                            $('#edit_rw_password').val('');
                            
                            $('#editUser').modal('toggle');
                            table.ajax.reload();
                            $("#alert_user_list").html(response.message);
                            $("#alert_user_list").show();
                            setTimeout(function() { $("#alert_user_list").slideUp(); }, 4000);
                        } else {
                            $("#edit_modal_message").html(response.message);
                            $("#edit_modal_message").show();
                            setTimeout(function() { $("#edit_modal_message").slideUp(); }, 4000);
                        }
                    }
                });   
            }
		}
	}
</script>
<style type="text/css">
    input[type=text], input[type=tel], input[type=hidden], textarea {
        text-transform: uppercase !important;
    }
    
    .input-group-addon {
        min-width: 175px;
        text-align: left;
        font-weight: lighter;
    }
</style>
<div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">User</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> User List</h2>
				<div class="pull-right">
					<a class="btn btn-primary btn-xs" href="#" data-toggle="modal" data-target="#addUser"><i class="glyphicon glyphicon-plus" data-toggle="tooltip" title="" data-original-title="Add"></i> Add</a>
                </div>
            </div>
            <div id="alert_user_list" class="alert alert-success" style="display: none; text-align: center;"></div>
            <div id="danger_user_list" class="alert alert-danger" style="display: none; text-align: center;"></div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
                    <table id="user_list" width="100%" class="table table-striped">
                        <thead>
                        	<tr>
                        		<th>NAME</th>
                        		<th>OCCUPATION</th>
                        		<th>PHONE NUMBER</th>
                        		<th>BRANCH</th>
                        		<th>ROLE</th>
                        		<th>USERNAME</th>
                        		<th>STATUS</th>
                        		<th>LAST LOGIN</th>
                        		<th>ACTION</th>
                    		</tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->

	<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Add User</h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="" method="post">
                    		<div id='modal_message' class="alert alert-danger" style="display: none;"></div>
                    		<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="id_employee">EMPLOYEE</label></span>
		                        <select name="id_employee" id="id_employee" class="form-control">
		                        	<?php echo $employee; ?>  
		                        </select>
		                    </div>
		                    <div class="clearfix"></div><br>
		                
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="username">USERNAME</label></span>
		                        <input name="username" id="username" type="text" class="form-control" placeholder="Username">
		                    </div>
		                    <div class="clearfix"></div><br>
		
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="password">PASSWORD</label></span>
		                        <input name="password" id="password" type="password" class="form-control" placeholder="PASSWORD">
		                        <!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>"> -->
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="rw_password">RETYPE PASSWORD</label></span>
		                        <input name="rw_password" id="rw_password" type="password" class="form-control" placeholder="RE-TYPE PASSWORD">
		                        <!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>"> -->
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="role">ROLE</label></span>
		                        <select name="id_role" id="id_role" class="form-control">
		                        	<option value="">-- Select Role --</option>
		                        	<option value="0">ADMIN</option>
		                        	<option value="1">KASIR</option>
		                        	<option value="2">PEGAWAI</option>
		                        </select>
		                    </div>
		                    <div class="clearfix"></div><br>
							
							<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="id_branch">BRANCH</label></span>
		                        <select name="id_branch" id="id_branch" class="form-control">
		                        	<?php echo $branch; ?>  
		                        </select>
		                    </div>
		            </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
                    <a href="#" class="btn btn-primary" onclick="submit();">SAVE</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Edit User</h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="" method="post">
                    		<div id='edit_modal_message' class="alert alert-danger" style="display: none;"></div>
                    		
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="username">USERNAME</label></span>
		                        <input name="edit_username" id="edit_username" type="text" class="form-control" placeholder="Username">
		                    </div>
		                    <div class="clearfix"></div><br>
		
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="role">ROLE</label></span>
		                        <select name="edit_id_role" id="edit_id_role" class="form-control">
		                        	<option value="">-- Select Role --</option>
		                        	<option value="0">ADMIN</option>
		                        	<option value="1">KASIR</option>
		                        	<option value="2">PEGAWAI</option>
		                        </select>
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="id_branch">BRANCH</label></span>
		                        <select name="edit_id_branch" id="edit_id_branch" class="form-control">
		                        	<?php echo $branch; ?>  
		                        </select>
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
                                <span class="input-group-addon"><label for="edit_password">PASSWORD</label></span>
                                <input name="edit_password" id="edit_password" type="text" class="form-control" placeholder="NEW PASSWORD">
                                <!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>"> -->
                            </div>
                            <div class="clearfix"></div>
                            <!-- 
                            <div class="input-group input-group-md">
                                <span class="input-group-addon"><label for="edit_rw_password">RETYPE PASSWORD</label></span>
                                <input name="edit_rw_password" id="edit_rw_password" type="password" class="form-control" placeholder="RE-TYPE NEW PASSWORD">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            </div>
                            <div class="clearfix"></div><br>
                            		                     -->
							<input name="edit_id_user" id="edit_id_user" type="hidden" >
		            </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
                    <a href="#" class="btn btn-primary" onclick="update();">UPDATE</a>
                </div>
            </div>
        </div>
    </div>
    