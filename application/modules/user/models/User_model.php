<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function check_existing($username) { 
			
		$sql 	= '	SELECT COUNT(*) AS total
					FROM `user`
					WHERE username = \''. $username .'\'';
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->row_array(); 
		
		$return = array(
			'total' => $result['total'],
		);
		
		return $return;
	}
	
	public function list_user($params = array()) { 
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (a.username LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT b.name AS emp_name, b.occupation, b.phone_number AS emp_phone, a.role, a.username, a.status, a.last_login, a.id_user, c.name AS branch, c.*
					FROM employee b
					RIGHT JOIN `user` a ON a.id_employee = b.id_employee 
					LEFT JOIN branch c ON a.id_branch = c.id_branch 
					WHERE 1=1 '.$where.'
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM employee b
									RIGHT JOIN `user` a ON a.id_employee = b.id_employee 
									LEFT JOIN branch c ON a.id_branch = c.id_branch 
									WHERE 1=1 '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total'],
		);
		
		return $return;
	}
	
	public function getEmployee() {
		$sql = "SELECT b.name, b.id_employee, a.status
				FROM employee b
				LEFT JOIN `user` a ON a.id_employee = b.id_employee
				WHERE a.status IS NULL";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function getBranch() {
		$sql = "SELECT * FROM branch";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function getUser($id) {
		$sql = "SELECT *
				FROM `user`
				WHERE id_user = ".$id;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function add($username, $password, $employee, $role, $branch) {
		if(empty($branch)) { $branch = 'null'; }
        if(empty($employee)) { $employee = 'null'; }
		$sql = "INSERT INTO `user` (id_employee, id_branch, username, password, role, status, last_login)
				VALUES ('".$employee."', ".$branch.", '".$username."', '".$password."', '".$role."', 0, '0000-00-00 00:00:00')";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}	
	
	public function update($username, $id, $role, $branch, $password) {
	        
	    $new_password = '';    
	    if($password != '') {
	        $new_password = ', password = \''.$password.'\'';
	    }    
	    
		if(empty($branch)) { $branch = 'null'; }
		
		$sql = "UPDATE `user`
				SET username = '".$username."', role = ".$role.", id_branch = ".$branch." ".$new_password."
				WHERE id_user = ".$id;
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
	
	public function delete($id) {
		$sql = "DELETE FROM `user` WHERE id_user = ". $id;
 		$delete = $this->db->query($sql);
		if($delete) {
			return true; 
		} else {
			return false; 
		}
	}

}	
