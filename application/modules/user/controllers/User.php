<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
  public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('role') != 0) { redirect('dashboard'); }
					
		$this->load->model('user_model');

	}
	
	public function index()
	{
	    $branch = $this->user_model->getBranch();
        $employee = $this->user_model->getEmployee();
        
	    $options = '<option value="">-- Select Branch --</option>';
        foreach ($branch as $list) {
            $options .= '<option value="'.$list['id_branch'].'">'.$list['name'].'</option>';
        }
        
        $options1 = '<option value="">-- Select Employee --</option>';
        foreach ($employee as $list) {
            $options1 .= '<option value="'.$list['id_employee'].'">'.$list['name'].'</option>';
        }
        
        $data['branch'] = $options;  
        $data['employee'] = $options1;   
           
		$this->template->load('maintemplate', 'user/views/user_view', $data);
	}
	
	public function get_user() {
		$id = $this->input->post('id');
		$data = $this->user_model->getUser($id);
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function list_user() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 4;}; 
		$order_fields = array('b.name', 'b.occupation', 'b.phone_number', 'c.id_branch', 'a.role', 'a.username', 'a.status', 'a.last_login');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		//var_dump($params);
		
		$list = $this->user_model->list_user($params);
		
		$result["recordsTotal"] = $list['total'];;
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
		    $last_login = $v['last_login'] == '0000-00-00 00:00:00' ? '-' : date('d M Y H:i:s', strtotime($v['last_login']));
            
			switch ($v['role']) {
				case 0:
					$role = 'Admin';
					break;
				case 1:
					$role = 'Kasir';
					break;
				case 2:
					$role = 'Pegawai';
					break;
			}
			switch ($v['status']) {
				case 0:
					$status = '<span class="label-danger label label-default">Offline</span>';
					break;
				case 1:
					$status = '<span class="label-success label label-default">Online</span>';
					break;
			}
			if($v['status'] == 0) {
				$action = '<a class="btn btn-primary btn-xs" href="#" title"Edit" onclick="edit_('.$v['id_user'].')"><i class="glyphicon glyphicon-edit"></i></a>'
				.' '.
				'<a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_('.$v['id_user'].', \''.$v['username'].'\')"><i class="glyphicon glyphicon-trash"></i></a>';	
			} else if($v['status'] == 1) {
				$action = '<a class="btn btn-primary btn-xs" href="#" title"Edit" onclick="edit_('.$v['id_user'].')" disabled="disabled"><i class="glyphicon glyphicon-edit"></i></a>'
				.' '.
				'<a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_('.$v['id_user'].', \''.$v['username'].'\')" disabled="disabled"><i class="glyphicon glyphicon-trash"></i></a>';
			}
			array_push($data, 
				array(
					$v['emp_name'],
					$v['occupation'],
					$v['emp_phone'],
					$v['branch'],
					$role,
					$v['username'],
					$status,
					$last_login,
					$action
					
				)
			);
		}
		
		$result["data"] = $data;
		//var_dump($result);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_user() {
	    $role = $this->input->post('role');
        
        $this->load->library('form_validation');
		
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('role', 'Role', 'required');
        
        if($role == 1) {
            $this->form_validation->set_rules('branch', 'Branch', 'required');
        }

        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $employee = $this->input->post('employee');
            $role = $this->input->post('role');
			$branch = $this->input->post('branch');
			
			$exist = $this->user_model->check_existing($username);
			if($exist['total'] != 0) {
				$result = array(
					'success' => false,
					'message' => $username.' is already exist!'
				);
			} else {
				$salt = sha1(md5($password));
				$password = md5($password.$salt).$password;
	            
	            $do_submit = $this->user_model->add($username, $password, $employee, $role, $branch);
	            if ($do_submit) {
	            	$result = array(
						'success' => true,
						'message' => 'User added!'
					);
	            } else {
	                $result = array(
						'success' => false,
						'message' => 'Failed to add new user!'
					);
	            }
			}
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function update_user() {
	    $role = $this->input->post('role');
        
        $this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('role', 'Role', 'required');
        
        if($role == 1) {
            $this->form_validation->set_rules('branch', 'Branch', 'required');
        }
        
        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $username = $this->input->post('username');
            $id = $this->input->post('id');
            $role = $this->input->post('role');
			$branch = $this->input->post('branch');
            $password = $this->input->post('password') != '' ? $this->input->post('password') : '';
			
            if($password != '') {
                $salt = sha1(md5($password));
                $password = md5($password.$salt).$password;
            }
            
			// $exist = $this->user_model->check_existing($username);
			// if($exist['total'] != 0) {
				// $result = array(
					// 'success' => false,
					// 'message' => $username.' is already exist!'
				// );
			// } else {
				$do_update = $this->user_model->update($username, $id, $role, $branch, $password);
	            if ($do_update) {
	            	$result = array(
						'success' => true,
						'message' => 'User updated!'
					);
	            } else {
	                $result = array(
						'success' => false,
						'message' => 'Failed to update user!'
					);
	            }
			//}
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function delete_user() {
        
        $id = $this->input->post('id');
        
        $do_delete = $this->user_model->delete($id);
        if ($do_delete) {
        	$result = array(
				'success' => true,
				'message' => 'User deleted!'
			);
        } else {
            $result = array(
				'success' => false,
				'message' => 'Failed to delete user!'
			);
        }
        
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
}
