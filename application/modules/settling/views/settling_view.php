<script>
	var table;
	var date_issue;

	$(document).ready(function(){
		table = $('#settling_list').DataTable({
			"processing": true,
			"serverSide": true,
	        "ajax": {
	        	'type' : 'get',
	        	'url': 'settling/list_settling'     
	        },
	        "columnDefs": [{
				"targets": 7,
				"width": "150px",
				"orderable": false
			}],
			"order": []
	    });
	    
	    $('#startdate').datetimepicker({
	    	format:'d-m-Y',
	    	timepicker: false
	    });
	    
	    $('#enddate').datetimepicker({
	    	format:'d-m-Y',
	    	timepicker: false
	    });
	    
	});
	
	function detail_(invoice, note, pcs) {
		$.ajax({
			type: 'get',
			url: "settling/get_detail", 
			data: {
				'invoice': invoice
			}, 
			success: function(response){
	        	if(response.success) {
	        	    $('#detail_note').text(note);
	        	    $('#detail_pcs').text(pcs);
	        		$('#detail_order').find('tbody').html('');
	        		var data = response.data;
	        		data.forEach(function(item) {
	        			// var pack_stts = '';
	        			// if(item.packing_date == null || item.packing_date == '' || item.packing_date == '0000-00-00') {
	        				// pack_stts = '-';
	        			// } else {
	        				// pack_stts = item.packing_date;
	        			// }
	        			
	        			$('#detail_order').find('tbody').append('<tr><td>'+item.name+'</td><td>'+item.quantity+' kg</td><td>Rp.'+item.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+'</td><td>Rp.'+item.total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+'</td></tr>');
	        		})
	        		$('#detail_review_price').text('Rp.'+(parseInt(data[0]['net_price'])+parseInt(data[0]['discount'])).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
	        		$('#detail_asign_wash_name').text(response.wash_emp);
	        		$('#detail_asign_wash_date').text(response.wash_date);
	        		$('#detail_asign_iron_name').text(response.iron_emp);
	        		$('#detail_asign_iron_date').text(response.iron_date);
	        		
	        		$('#detailOrder').modal('toggle');
	        	} else {
	        		$('#detail_order').find('tbody').html('');
	        		$('#detail_order').find('tbody').append('<tr><td colspan="2">No Data</td></tr>');
	        		$('#detailOrder').modal('toggle');
	        	}
	    	}
	    });
	}
	
	function filter() {
		var start = $('#startdate').val();
		var end = $('#enddate').val();
		var total_net = $('#total_net').val();
		
		var x = start.split('-');
		var y = end.split('-');
		var rstart = x[2]+'-'+x[1]+'-'+x[0];
		var rend = y[2]+'-'+y[1]+'-'+y[0];
		
		if(start != '' || end != '') {
            if(start == '' || end == '') {
                $("#danger_list").html('Please check your input!');
                $("#danger_list").show();
                setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
            }
        } 
        
        if(start != '' && end != '') {
            if(Date.parse(rstart) > Date.parse(rend)) {
                $("#danger_list").html('Startdate should be lower than Enddate!');
                $("#danger_list").show();
                setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
            }
        }
		
		$('#settling_list').DataTable().destroy();
		table = $('#settling_list').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
	        	'type' : 'get',
        		'url': 'settling/list_settling_filter',
	        	'data': {
					'startdate': start,
					'enddate': end,
					'total_net': total_net
				}     
	        },
	        "columnDefs": [{
				"targets": 7,
                "width": "150px",
                "orderable": false
			}],
			"order": []
	    });
	}
	
	function customer_datail(phone, address, name) {
	    $('#detail_name').text(name);
	    $('#detail_phone').text(phone);
	    $('#detail_address').text(address);
	    $('#detailCustomer').modal('toggle');
	}
	
	function delete_all() {
	    var r = confirm("Are you sure want clear transactions?","Ok","Cancel");
        if(r){
            var start = $('#startdate').val();
            var end = $('#enddate').val();
            var total_net = $('#total_net').val();
            
            $.ajax({
                type: 'post',
                url: "settling/clear_trx", 
                data: {
                    'startdate': start,
                    'enddate': end,
                    'total_net': total_net
                }, 
                success: function(response){
                    if(response.success) {
                        logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Clear Transactions');
                        
                        table.ajax.reload();
                        $("#alert_list").html("Transactions Deleted!");
                        $("#alert_list").show();
                        setTimeout(function() { $("#alert_list").slideUp(); }, 4000);
                    } else {
                        //$("#danger_list").html(response.message);
                        $("#danger_list").html("Clear Transactions Failed!");
                        $("#danger_list").show();
                        setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
                    } 
                },
                async: false
            });
        }
	}
	
	function delete_(invoice) {
        var r = confirm("Are you sure want to delete \""+invoice+"\"?","Ok","Cancel");
        if(r){
            $.ajax({
                type: 'post',
                url: "settling/delete_trx", 
                data: { 'invoice': invoice }, 
                success: function(response){
                    if(response.success) {
                        logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Delete Transactions : ' + invoice);
                        
                        table.ajax.reload();
                        $("#alert_list").html("Transactions Deleted!");
                        $("#alert_list").show();
                        setTimeout(function() { $("#alert_list").slideUp(); }, 4000);
                    } else {
                        //$("#danger_list").html(response.message);
                        $("#danger_list").html("Delete Transactions Failed!");
                        $("#danger_list").show();
                        setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
                    } 
                }
            });
        }
    }   
</script>
<style type="text/css">
    input[type=text], input[type=tel], input[type=hidden], textarea {
        text-transform: uppercase !important;
    }
    
    input[type=number] {
        -moz-appearance:textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
<div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Completion</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Settling</h2>
                <a class="btn btn-danger btn-xs pull-right" href="#" onclick="delete_all();"><i class="glyphicon glyphicon-remove"></i> Delete All</a>
            </div>
            <div id="alert_list" class="alert alert-success" style="display: none; text-align: center;"></div>
            <div id="danger_list" class="alert alert-danger" style="display: none; text-align: center;"></div>
            
            <div class="box-content row">
            	<div class="col-lg-12 col-md-12">
        		    <div class="col-lg-3 col-md-3">
                        <input name="startdate" id="startdate" type="text" class="form-control" placeholder="Start Date">
                    </div>
                
                    <div class="col-lg-3 col-md-3">
                        <input name="enddate" id="enddate" type="text" class="form-control" placeholder="End Date">
                    </div>
                    
                    <div class="col-lg-3 col-md-3">
                        <input name="total_net" id="total_net" type="text" class="form-control" placeholder="Total">
                    </div>
                    
                    <div class="col-lg-3 col-md-3">
                        <input name="filter" id="filter" type="submit" class="form-control btn btn-primary" value="FILTER" onclick="filter();">
                    </div> 
                                      
                    <div class="clearfix"></div><br>                  
            	</div>
            	<div class="col-lg-12 col-md-12">
                    <table id="settling_list" width="100%" class="table table-striped">
                        <thead>
                        	<tr>
                        	    <th>ISSUE DATE</th>
                        		<th>INVOICE</th>
                        		<th>CUSTOMER</th>
                        		<th>TOTAL</th>
                        		<th>TOTAL PCS.</th>
                        		<th>TGL. BAYAR</th>
                        		<th>TGL. AMBIL</th>
                        		<th>ACTION</th>
                    		</tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->

	<div class="modal fade" id="detailOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Detail Invoice</h3>
                </div>
                <div class="modal-body">
                    <table id="detail_order" width="100%" class="table table-striped">
                        <thead>
                        	<tr>
                        		<th>SERVICE</th>
                        		<th>QUANTITY</th>
                        		<th>PRICE</th>
                        		<th>TOTAL</th>
                    		</tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-lg-offset-6 col-md-offset-6">
                            <label for="review_price" class="label-control">GRAND TOTAL&nbsp;:</label>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <label id="detail_review_price" class="label-control">0</label>
                        </div>
                    </div>  
                    <div class="clearfix"></div><br> 
                    <div class="col-md-12 col-lg-12">TOTAL PCS. : <span id="detail_pcs"></span></div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 col-lg-12">NOTE : <span id="detail_note" style="word-break: break-word;"></span></div>
                    <div class="clearfix"></div><br> 
                    
                    <table id="detail_asign_order" width="100%" class="table">
                        <tbody>
                            <tr>
                            	<td>CUCI</td>
                            	<td id="detail_asign_wash_name">-</td>
                            	<td id="detail_asign_wash_date">-</td>
                            </tr>  
                            <tr>
                                <td>SETRIKA</td>
                                <td id="detail_asign_iron_name">-</td>
                                <td id="detail_asign_iron_date">-</td>
                            </tr>  
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="detailCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Detail Customer</h3>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 col-lg-12">NAME&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="detail_name"></span></div><br/><br/>
                    <div class="col-md-12 col-lg-12">PHONE&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="detail_phone" style="word-break: break-word;"></span></div><div class="clearfix"></div><br/>
                    <div class="col-md-12 col-lg-12">ADDRESS&emsp;&emsp;: <br/><span id="detail_address" style="word-break: break-word;"></span></div><br/><br/>
                	<div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
                </div>
            </div>
        </div>
    </div>