<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settling extends CI_Controller {
  public function __construct()
	{
		parent::__construct();				
		$this->load->model('settling_model');

	}
	
	public function index()
	{
		$this->template->load('maintemplate', 'settling/views/settling_view');
	}
	
	public function list_settling() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];} else{$order_dir = 'desc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 1;}; 
        $order_fields = array('invoice_issue_date', 'invoice_number', 'customer_name', 'net_price', 'total_pcs', 'payment_date', 'pickup_date', '');
        
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_column == 1 ? "MAX(CAST(SUBSTRING(".$order_fields[$order_column].",LOCATE('-',".$order_fields[$order_column].")+1) AS SIGNED))" : $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->settling_model->list_settling($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$no = 1;
		$data = array();
        $fgroup1 = 0; $fgroup2 = 0; $fgroup3 = 0; $fgroup4 = 0;
        			
		foreach ( $list['data'] as $k => $v ) {
		    $pickupdate = $v['pickup_date'] == '' || $v['pickup_date'] == 0 || $v['pickup_date'] == '0000-00-00' || $v['pickup_date'] == null ? '' : date('d M Y', strtotime($v['pickup_date']));
            $paymentdate = $v['payment_date'] == '' || $v['payment_date'] == 0 || $v['payment_date'] == '0000-00-00' || $v['payment_date'] == null ? '' : date('d M Y', strtotime($v['payment_date']));
            
            $detail = '<a class="btn btn-warning btn-xs" href="#" title"Detail" onclick="detail_(\''.$v['invoice_number'].'\', \''.$v['note'].'\', '.$v['total_pcs'].')">Detail</a>';
            $delete = '<a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_(\''.$v['invoice_number'].'\')"><i class="glyphicon glyphicon-trash"></i></a>';
            
			array_push($data,  
                array(
                    date('d M Y', strtotime($v['invoice_issue_date'])),
                    $v['invoice_number'],
                    '<a style="cursor: pointer;" title"Detail" onclick="customer_datail(\''.$v['customer_phone'].'\', \''.$v['address'].'\', \''.$v['customer_name'].'\')">'.$v['customer_name'].'</a>',
                    'Rp.'.number_format($v['net_price'] + $v['discount']),
                    $v['total_pcs'],
                    $paymentdate,
                    $pickupdate,
                    $detail.' '.$delete
                )
            );
			
			$no++;
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function list_settling_filter() {
		$startdate = $this->input->get('startdate');
		$enddate = $this->input->get('enddate');
        $total_net = $this->input->get('total_net');  	
		
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 1;}; 
		$order_fields = array('invoice_issue_date', 'invoice_number', 'customer_name', 'net_price', 'total_pcs', 'payment_date', 'pickup_date', '');
        
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_column == 1 ? "MAX(CAST(SUBSTRING(".$order_fields[$order_column].",LOCATE('-',".$order_fields[$order_column].")+1) AS SIGNED))" : $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		$params['start'] 		= $startdate != '' ? date('Y-m-d', strtotime($startdate)) : '';
		$params['end'] 			= $enddate != '' ? date('Y-m-d', strtotime($enddate)) : '';
        $params['total_net']    = $total_net;
		
		$list = $this->settling_model->list_settling_filter($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$no = 1;
		$data = array();
        $fgroup1 = 0; $fgroup2 = 0; $fgroup3 = 0; $fgroup4 = 0;
        			
		foreach ( $list['data'] as $k => $v ) {
			$pickupdate = $v['pickup_date'] == '' || $v['pickup_date'] == 0 || $v['pickup_date'] == '0000-00-00' || $v['pickup_date'] == null ? '' : date('d M Y', strtotime($v['pickup_date']));
            $paymentdate = $v['payment_date'] == '' || $v['payment_date'] == 0 || $v['payment_date'] == '0000-00-00' || $v['payment_date'] == null ? '' : date('d M Y', strtotime($v['payment_date']));
            
            $detail = '<a class="btn btn-warning btn-xs" href="#" title"Detail" onclick="detail_(\''.$v['invoice_number'].'\', \''.$v['note'].'\', '.$v['total_pcs'].')">Detail</a>';
            $delete = '<a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_(\''.$v['invoice_number'].'\')"><i class="glyphicon glyphicon-trash"></i></a>';
            
            array_push($data, 
                array(
                    date('d M Y', strtotime($v['invoice_issue_date'])),
                    $v['invoice_number'],
                    '<a style="cursor: pointer;" title"Detail" onclick="customer_datail(\''.$v['customer_phone'].'\', \''.$v['address'].'\', \''.$v['customer_name'].'\')">'.$v['customer_name'].'</a>',
                    'Rp.'.number_format($v['net_price'] + $v['discount']),
                    $v['total_pcs'],
                    $paymentdate,
                    $pickupdate,
                    $detail.' '.$delete
                )
            );        
            
			$no++;
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

    public function get_detail() {
		$invoice = $this->input->get('invoice');
		
		$data = $this->settling_model->get_detail($invoice);
		
        $washdate = $data[0]['wash_date'] == '' || $data[0]['wash_date'] == 0 || $data[0]['wash_date'] == '0000-00-00' || $data[0]['wash_date'] == null ? '-' : date('d-m-Y', strtotime($data[0]['wash_date'])) ;
        $irondate = $data[0]['iron_date'] == '' || $data[0]['iron_date'] == 0 || $data[0]['iron_date'] == '0000-00-00' || $data[0]['iron_date'] == null ? '-' : date('d-m-Y', strtotime($data[0]['iron_date'])) ;  
        
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data,
				'wash_emp' => $data[0]['wash_emp'] == null || $data[0]['wash_emp'] == '' ? '-' : $data[0]['wash_emp'],
				'iron_emp' => $data[0]['iron_emp'] == null || $data[0]['iron_emp'] == '' ? '-' : $data[0]['iron_emp'],
				'wash_date' => $washdate,
				'iron_date' => $irondate
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    public function clear_trx() {
        $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');
        $total_net = $this->input->post('total_net');    
        
        $startdate_ = $startdate != '' ? date('Y-m-d', strtotime($startdate)) : '';
        $enddate_ = $enddate != '' ? date('Y-m-d', strtotime($enddate)) : '';
        
        $transactions = $this->settling_model->list_invoice($startdate_, $enddate_, $total_net);
        
        $lists = count($transactions);
        $done = 0;
        foreach ( $transactions as $row ) {
            $clear_order = $this->settling_model->clear_order($row['invoice_number']);
            
            if($clear_order) {
                $clear_trx = $this->settling_model->clear_trx($row['invoice_number']);
            }
            
            $done++;
        }
        
        if ($done === $lists) {
            $result = array(
                'success' => true
            );
        } else {
            $result = array(
                'success' => false
            );
        }
        
        $done = 0;
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function delete_trx() {
        $invoice = $this->input->post('invoice');
        
        $clear_order = $this->settling_model->clear_order($invoice);
        
        $clear_trx = false;    
        if($clear_order) {
            $clear_trx = $this->settling_model->clear_trx($invoice);
        }
            
        if($clear_trx) {
            $result = array(
                'success' => true
            );
        } else {
            $result = array(
                'success' => false
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
