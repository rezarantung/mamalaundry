<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settling_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function list_settling($params = array()) { 
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND c.id_branch = '.$id_branch.' AND b.id_branch = '.$id_branch;
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (a.invoice_number LIKE \'%'.$params['filter'].'%\' OR b.name LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT a.*, b.name AS customer_name, b.customer_phone, b.address, d.name AS wash_emp, e.name AS iron_emp
					FROM invoice a
					LEFT JOIN customer b ON a.customer_phone = b.customer_phone
					LEFT JOIN branch c ON a.id_branch = c.id_branch 
					LEFT JOIN employee d ON a.id_wash_emp = d.id_employee
					LEFT JOIN employee e ON a.id_iron_emp = e.id_employee
					WHERE 1=1 AND (pickup_date != \'0000-00-00\' AND pickup_date IS NOT NULL) '.$branch.' '.$where.'
					GROUP BY a.invoice_number
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM invoice a
									LEFT JOIN customer b ON a.customer_phone = b.customer_phone
									LEFT JOIN branch c ON a.id_branch = c.id_branch 
									LEFT JOIN employee d ON a.id_wash_emp = d.id_employee
									LEFT JOIN employee e ON a.id_iron_emp = e.id_employee
									WHERE 1=1 AND (pickup_date != \'0000-00-00\' AND pickup_date IS NOT NULL) '.$branch.' '.$where.'
									GROUP BY a.invoice_number')->num_rows();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total,
			'total' => $total,
		);
		
		return $return;
	}

	public function list_settling_filter($params = array()) { 
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND c.id_branch = '.$id_branch.' AND b.id_branch = '.$id_branch;
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (a.invoice_number LIKE \'%'.$params['filter'].'%\' OR b.name LIKE \'%'.$params['filter'].'%\')';
		}
		
		if(!empty($params['start']) && !empty($params['end'])) {
            if(strtotime($params['start']) <= strtotime($params['end'])) {
                $fstartdate = $params['start'];
                $fenddate = $params['end'];
                
                $where .= 'AND a.invoice_issue_date BETWEEN CAST(\''.$fstartdate.'\' AS DATETIME) AND CAST(\''.$fenddate.'\' AS DATETIME)';
                if($fstartdate == $fenddate) {
                    $where .= 'AND a.invoice_issue_date = \''.$fstartdate.'\'';
                }   
            }
        }
        
        $total_net = '';
        if($params['total_net'] != null) {
            $total_net = 'AND a.net_price LIKE \'%'.$params['total_net'].'%\'';
        }
		
		$sql 	= '	SELECT a.*, b.name AS customer_name, b.customer_phone, b.address, d.name AS wash_emp, e.name AS iron_emp
					FROM invoice a
					LEFT JOIN customer b ON a.customer_phone = b.customer_phone
					LEFT JOIN branch c ON a.id_branch = c.id_branch 
					LEFT JOIN employee d ON a.id_wash_emp = d.id_employee
					LEFT JOIN employee e ON a.id_iron_emp = e.id_employee
					WHERE 1=1 AND (pickup_date != \'0000-00-00\' AND pickup_date IS NOT NULL) '.$branch.' '.$where.' '.$total_net.'
					GROUP BY a.invoice_number
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM invoice a
									LEFT JOIN customer b ON a.customer_phone = b.customer_phone
									LEFT JOIN branch c ON a.id_branch = c.id_branch 
									LEFT JOIN employee d ON a.id_wash_emp = d.id_employee
									LEFT JOIN employee e ON a.id_iron_emp = e.id_employee
									WHERE 1=1 AND (pickup_date != \'0000-00-00\' AND pickup_date IS NOT NULL) '.$branch.' '.$total_net.' '.$where.'
									GROUP BY a.invoice_number')->num_rows();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total,
			'total' => $total,
		);
		
		return $return;
	}
	
    public function get_detail($invoice) {
		$sql = "SELECT b.name, a.quantity, b.price, a.price AS total, a.packing_date, c.net_price, d.name AS wash_emp, c.wash_date, e.name AS iron_emp, c.iron_date, c.discount
				FROM `order` a
				LEFT JOIN service b ON a.id_service = b.id_service
				LEFT JOIN invoice c ON a.invoice_number = c.invoice_number
				LEFT JOIN employee d ON c.id_wash_emp = d.id_employee
                LEFT JOIN employee e ON c.id_iron_emp = e.id_employee
				WHERE a.invoice_number = '".$invoice."'";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
    
    public function list_invoice($startdate, $enddate, $total_net_) {
        $where = '';
        if(!empty($startdate) && !empty($enddate)) {
            if(strtotime($startdate) <= strtotime($enddate)) {
                $fstartdate = $startdate;
                $fenddate = $enddate;
                
                $where = 'AND invoice_issue_date BETWEEN CAST(\''.$fstartdate.'\' AS DATETIME) AND CAST(\''.$fenddate.'\' AS DATETIME)';
                if($fstartdate == $fenddate) {
                    $where = 'AND invoice_issue_date = \''.$fstartdate.'\'';
                }   
            }
        }
        
        $total_net = '';
        if($total_net_ != null) {
            $total_net = 'AND net_price LIKE \'%'.$total_net_.'%\'';
        }
        
        $sql = "SELECT invoice_number FROM invoice WHERE 1=1 ".$where." ".$total_net." AND pickup_date != '0000-00-00' AND pickup_date IS NOT NULL";
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
    
    public function clear_order($invoice) {
        $sql = "DELETE FROM `order`
                WHERE invoice_number = '".$invoice."'";
        $clear = $this->db->query($sql);
        
        if($clear) {
            return true;
        } else {
            return false;
        }
    }
    
    public function clear_trx($invoice) {
        $sql = "DELETE FROM invoice
                WHERE invoice_number = '".$invoice."'";
                
        $clear = $this->db->query($sql);
        
        if($clear) {
            return true;
        } else {
            return false;
        }
    }
    
}	
