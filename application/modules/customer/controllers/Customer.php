<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
  public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('role') == 2) { redirect('dashboard'); }		
		$this->load->model('customer_model');

	}
	
	public function index()
	{
		$this->template->load('maintemplate', 'customer/views/customer_view');
	}
	
	public function list_customer() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('name', 'customer_phone', 'address');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->customer_model->list_customer($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
		    $active = $this->customer_model->list_trx_active($v['customer_phone']);
            
		    if($this->session->userdata('role') != 0) {
		        if(($this->session->userdata('id_branch') != $this->session->userdata('id_branch_emp'))) {
                    $actions = '-';
                } else {
                    $actions = '<a class="btn btn-success btn-xs" href="#" title"Edit" onclick="edit_(\''.$v['customer_phone'].'\')"><i class="glyphicon glyphicon-edit"></i></a>';
                    
                    if($active['total'] == 0) {
                        $actions .= ' <a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_(\''.$v['customer_phone'].'\', \''.$v['name'].'\')"><i class="glyphicon glyphicon-trash"></i></a>';
                    }
                }
		    } else {
		        $actions = '<a class="btn btn-success btn-xs" href="#" title"Edit" onclick="edit_(\''.$v['customer_phone'].'\')"><i class="glyphicon glyphicon-edit"></i></a>';
		          
                if($active['total'] == 0) {
                    $actions .= ' <a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_(\''.$v['customer_phone'].'\', \''.$v['name'].'\')"><i class="glyphicon glyphicon-trash"></i></a>';
                }  
            }
            
            
            
            
		    
			array_push($data, 
				array(
					$v['name'],
					$v['customer_phone'],
					$v['address'],
					$actions
					//.' '.
					//'<a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_(\''.$v['customer_phone'].'\')"><i class="glyphicon glyphicon-trash"></i></a>'
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_customer() {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Customer Name', 'required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required');
		
        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $name = $this->input->post('name');
            $phone = $this->input->post('phone');
			$address = $this->input->post('address');
			
			$exist = $this->customer_model->check_existing($phone);
			if($exist['total'] != 0) {
				$result = array(
					'success' => false,
					'message' => $name.' is already exist!'
				);
			} else {
				$do_submit = $this->customer_model->add($name, $phone, $address);
	            if ($do_submit) {
	            	$result = array(
						'success' => true,
						'message' => 'Customer added!'
					);
	            } else {
	                $result = array(
						'success' => false,
						'message' => 'Failed to add new customer!'
					);
	            }
			}
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function get_customer() {
		$phone = $this->input->post('phone');
		$data = $this->customer_model->getCustomer($phone);
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function update_customer() {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Customer Name', 'required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required');

        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $name = $this->input->post('name');
            $phone = $this->input->post('phone');
            $old_phone = $this->input->post('phone_old');
			$address = $this->input->post('address');
			
			$do_update = $this->customer_model->update($name, $phone, $address, $old_phone);
            if ($do_update) {
            	$result = array(
					'success' => true,
					'message' => 'Customer updated!'
				);
            } else {
                $result = array(
					'success' => false,
					'message' => 'Failed to update customer!'
				);
            }
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function delete_customer() {
        
        $phone = $this->input->post('phone');
        
        $do_delete = $this->customer_model->delete($phone);
        if ($do_delete) {
            $result = array(
                'success' => true,
                'message' => 'Customer deleted!'
            );
        } else {
            $result = array(
                'success' => false,
                'message' => 'Failed to delete customer!'
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
