<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function list_customer($params = array()) { 
		$branch = $this->session->userdata('id_branch');	
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (name LIKE \'%'.$params['filter'].'%\' OR customer_phone LIKE \'%'.$params['filter'].'%\' OR address LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT *
					FROM customer
					WHERE 1=1 AND id_branch = '.$branch.' '.$where.'
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM customer
									WHERE 1=1 AND id_branch = '.$branch.' '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total'],
		);
		
		return $return;
	}
	
	public function check_existing($phone) { 
		$sql 	= '	SELECT COUNT(*) AS total
					FROM customer
					WHERE customer_phone = \''. $phone .'\'';
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->row_array(); 
		
		$return = array(
			'total' => $result['total'],
		);
		
		return $return;
	}
	
	public function add($name, $phone, $address) {
	    $branch = $this->session->userdata('id_branch');  
		$sql = "INSERT INTO customer (customer_phone, id_branch, name, address)
				VALUES ('".$phone."', ".$branch.", '".$name."', '".$address."')";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}	
	
	public function getCustomer($phone) {
	    $branch = $this->session->userdata('id_branch');
		$sql = "SELECT *
				FROM customer
				WHERE customer_phone = '".$phone."' AND id_branch = ".$branch;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
    
    public function list_trx_active($phone) {
        $sql = "SELECT COUNT(*) AS total FROM invoice WHERE customer_phone = '".$phone."'";
        $query  =  $this->db->query($sql);
        $result = $query->row_array(); 
        
        $return = array(
            'total' => $result['total'],
        );
        
        return $return;
    }
	
	public function update($name, $phone, $address, $old_phone) {
	    $branch = $this->session->userdata('id_branch');
		$sql = "UPDATE customer
				SET customer_phone = '".$phone."', name = '".$name."', address = '".$address."'
				WHERE customer_phone = '".$old_phone."' AND id_branch = ".$branch;
 		$update = $this->db->query($sql);
        
        $sql1 = "UPDATE `order`
                SET customer_phone = '".$phone."'
                WHERE customer_phone = '".$old_phone."' AND id_branch = ".$branch;
        $update1 = $this->db->query($sql1);
        
        $sql2 = "UPDATE invoice
                SET customer_phone = '".$phone."'
                WHERE customer_phone = '".$old_phone."' AND id_branch = ".$branch;
        $update2 = $this->db->query($sql2);
        
		if($update && $update1 && $update2) {
			return true; 
		} else {
			return false; 
		}
	}
    
    public function delete($phone) {
        $sql = "DELETE FROM customer WHERE customer_phone = '".$phone."'";
        $delete = $this->db->query($sql);
        if($delete) {
            return true; 
        } else {
            return false; 
        }
    }
}	
