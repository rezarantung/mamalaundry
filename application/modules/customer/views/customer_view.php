<script>
	var table

	$(document).ready(function(){
		table = $('#customer_list').DataTable({
			"processing": true,
			"serverSide": true,
	        "ajax": {
	        	'type' : 'get',
	        	'url': 'customer/list_customer'     
	        },
	        "columnDefs": [{
				"targets": [3],
				"orderable": false
			}],
			"bAutoWidth": false,
            "aoColumns": [{"sWidth":"20%"},{"sWidth":"40%"},{"sWidth":"40%"}]
	    });
	});
	
	function submit() {
		var r = confirm("Are you sure want to submit?","Ok","Cancel");
		if(r){
			var name = $('#name').val();
			var phone = $('#phone').val();
			var address = $('#address').val();
			
			$.ajax({
				type: 'post',
				url: "customer/add_customer", 
				data: {
					'name': name.toUpperCase(),
					'phone': phone.toUpperCase(),
					'address': address.toUpperCase()
				}, 
				success: function(response){
		        	if(response.success) {
		        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Create Customer : ' + '"' + name + '"');
		        		
		        		$('#name').val('');
						$('#phone').val('');
						$("#address").val('');
						
		        		$('#addCustomer').modal('toggle');
		        		table.ajax.reload();
		        		$("#alert_list").html(response.message);
						$("#alert_list").show();
						setTimeout(function() { $("#alert_list").slideUp(); }, 4000);
		        	} else {
		        		$("#modal_message").html(response.message);
						$("#modal_message").show();
						setTimeout(function() { $("#modal_message").slideUp(); }, 4000);
		        	}
		    	}
		    });	
		}
	}
	
	function edit_(phone) {
		$('#editCustomer').modal('toggle');
		$.ajax({
			type: 'post',
			url: "customer/get_customer", 
			data: { 
				'phone': phone 
			}, 
			success: function(response){
	        	if(response.success) {
	        		$('#edit_name').val(response.data[0].name);
					$('#edit_phone').val(response.data[0].customer_phone);
					$('#edit_phone_old').val(response.data[0].customer_phone);
					$('#edit_address').val(response.data[0].address);
	        	} else {
	        		console.log(response.message);
	        	}
	    	}
	    });
	}
	
	function update() {
		var r = confirm("Are you sure want to update?","Ok","Cancel");
		if(r){
			var name = $('#edit_name').val();
			var phone = $('#edit_phone').val();
			var phone_old = $('#edit_phone_old').val();
			var address = $('#edit_address').val();
			
			$.ajax({
				type: 'post',
				url: "customer/update_customer", 
				data: {
					'name': name.toUpperCase(),
					'phone': phone.toUpperCase(),
					'phone_old': phone_old.toUpperCase(),
					'address': address.toUpperCase()
				}, 
				success: function(response){
		        	if(response.success) {
		        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Update Customer : ' + '"' + name + '"');
		        		
		        		$('#edit_name').val('');
						$('#edit_phone').val('');
						$('#edit_address').val('');
						
		        		$('#editCustomer').modal('toggle');
		        		table.ajax.reload();
		        		$("#alert_list").html(response.message);
						$("#alert_list").show();
						setTimeout(function() { $("#alert_list").slideUp(); }, 4000);
		        	} else {
		        		$("#edit_modal_message").html(response.message);
						$("#edit_modal_message").show();
						setTimeout(function() { $("#edit_modal_message").slideUp(); }, 4000);
		        	}
		    	}
		    });	
		}
	}
	
	function delete_(phone, name) {
        var r = confirm("Are you sure want to delete \""+name+"\"?","Ok","Cancel");
            if(r){
                $.ajax({
                    type: 'post',
                    url: "customer/delete_customer", 
                    data: { 'phone': phone }, 
                    success: function(response){
                        if(response.success) {
                            logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Delete Customer : ' + '"' + name + '"');
                            table.ajax.reload();
                            $("#alert_list").html(response.message);
                            $("#alert_list").show();
                            setTimeout(function() { $("#alert_list").slideUp(); }, 4000);
                        } else {
                            table.ajax.reload();
                            $("#danger_list").html(response.message);
                            $("#danger_list").show();
                            setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
                        }
                    }
                });
            }
    }
</script>
<style type="text/css">
    input[type=text], input[type=tel], input[type=hidden], textarea {
        text-transform: uppercase !important;
    }
    
    #customer_list tbody td {
        word-break: break-word;
    }
    
    .input-group-addon {
        min-width: 100px;
        text-align: left;
        font-weight: lighter;
    }
</style>
<div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Customer</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Customer List</h2>
                <?php
                    if($this->session->userdata('role') != 0) {
                        if(($this->session->userdata('id_branch') == $this->session->userdata('id_branch_emp'))) {
                ?>             
                            <div class="pull-right">
                                <a class="btn btn-primary btn-xs" href="#" data-toggle="modal" data-target="#addCustomer"><i class="glyphicon glyphicon-plus" data-toggle="tooltip" title="" data-original-title="Add"></i> Add</a>
                            </div>
                <?php   }
                    } else {
                ?>  
                            <div class="pull-right">
                                <a class="btn btn-primary btn-xs" href="#" data-toggle="modal" data-target="#addCustomer"><i class="glyphicon glyphicon-plus" data-toggle="tooltip" title="" data-original-title="Add"></i> Add</a>
                            </div>
                <?php } ?>   
            </div>
            <div id="alert_list" class="alert alert-success" style="display: none; text-align: center;"></div>
            <div id="danger_list" class="alert alert-danger" style="display: none; text-align: center;"></div>
            
            <div class="box-content row">
            	<div class="col-lg-12 col-md-12">
                    <table id="customer_list" width="100%" class="table table-striped">
                        <thead>
                        	<tr>
                        		<th>NAME</th>
                        		<th>PHONE</th>
                        		<th>ADDRESS</th>
                        		<th>ACTION</th>
                    		</tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->

	<div class="modal fade" id="addCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Add Customer</h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="" method="post">
                		<div id='modal_message' class="alert alert-danger" style="display: none;"></div>
                    		
                		<div class="input-group input-group-md">
	                        <span class="input-group-addon"><label for="name">NAME</label></span>
	                        <input name="name" id="name" type="text" class="form-control" placeholder="Customer Name">
	                    </div>
	                    <div class="clearfix"></div><br>
	
	                    <div class="input-group input-group-md">
	                        <span class="input-group-addon"><label for="phone">PHONE</label></span>
	                        <input name="phone" id="phone" type="tel" class="form-control" placeholder="Phone Number">
	                    </div>
	                    <div class="clearfix"></div><br>
	                    
	                    <div class="input-group input-group-md">
	                        <span class="input-group-addon"><label for="address">ADDRESS</label></span>
	                        <textarea name="address" id="address" rows="2" cols="50" class="form-control" placeholder="Address"></textarea> 
	                    </div>
	                    <div class="clearfix"></div>
	           	 	</form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
                    <a href="#" class="btn btn-primary" onclick="submit();">SAVE</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="editCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Edit Customer</h3>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="" method="post">
                		<div id='edit_modal_message' class="alert alert-danger" style="display: none;"></div>
                    		
                		<div class="input-group input-group-md">
	                        <span class="input-group-addon"><label for="edit_name">NAME</label></span>
	                        <input name="edit_name" id="edit_name" type="text" class="form-control" placeholder="Customer Name">
	                    </div>
	                    <div class="clearfix"></div><br>
	
	                    <div class="input-group input-group-md">
	                        <span class="input-group-addon"><label for="edit_phone">PHONE</label></span>
	                        <input name="edit_phone" id="edit_phone" type="tel" class="form-control" placeholder="Phone Number">
	                        <input name="edit_phone_old" id="edit_phone_old" type="hidden">
	                    </div>
	                    <div class="clearfix"></div><br>
	                    
	                    <div class="input-group input-group-md">
	                        <span class="input-group-addon"><label for="edit_address">ADDRESS</label></span>
	                        <textarea name="edit_address" id="edit_address" rows="2" cols="50" class="form-control" placeholder="Address"></textarea> 
	                    </div>
	                    <div class="clearfix"></div>
	           	 	</form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
                    <a href="#" class="btn btn-primary" onclick="update();">UPDATE</a>
                </div>
            </div>
        </div>
    </div>
    