<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transaction_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function list_transaction($params = array()) { 
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND c.id_branch = '.$id_branch.' AND b.id_branch = '.$id_branch;
		// if($this->session->userdata('role') == 1) {
			 // $branch = 'AND c.id_branch = '.$id_branch;
		// }
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (a.invoice_number LIKE \'%'.$params['filter'].'%\' OR b.name LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT a.*, b.name AS customer_name, b.customer_phone, b.address, d.name AS wash_emp, e.name AS iron_emp
					FROM invoice a
					LEFT JOIN customer b ON a.customer_phone = b.customer_phone
					LEFT JOIN branch c ON a.id_branch = c.id_branch 
					LEFT JOIN employee d ON a.id_wash_emp = d.id_employee
					LEFT JOIN employee e ON a.id_iron_emp = e.id_employee
					WHERE 1=1 '.$branch.' '.$where.'
					GROUP BY a.invoice_number
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM invoice a
									LEFT JOIN customer b ON a.customer_phone = b.customer_phone
									LEFT JOIN branch c ON a.id_branch = c.id_branch 
									LEFT JOIN employee d ON a.id_wash_emp = d.id_employee
									LEFT JOIN employee e ON a.id_iron_emp = e.id_employee
									WHERE 1=1 '.$branch.' '.$where.'
									GROUP BY a.invoice_number')->num_rows();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total,
			'total' => $total,
		);
		
		return $return;
	}

	public function list_transaction_filter($params = array()) { 
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND c.id_branch = '.$id_branch.' AND b.id_branch = '.$id_branch;
		// if($this->session->userdata('role') == 1) {
			 // $branch = 'AND c.id_branch = '.$id_branch;
		// }
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (a.invoice_number LIKE \'%'.$params['filter'].'%\' OR b.name LIKE \'%'.$params['filter'].'%\')';
		}
		
		if(!empty($params['start']) && !empty($params['end'])) {
            if(strtotime($params['start']) <= strtotime($params['end'])) {
                $fstartdate = $params['start'];
                $fenddate = $params['end'];
                
                $where .= 'AND a.invoice_issue_date BETWEEN CAST(\''.$fstartdate.'\' AS DATETIME) AND CAST(\''.$fenddate.'\' AS DATETIME)';
                if($fstartdate == $fenddate) {
                    $where .= 'AND a.invoice_issue_date = \''.$fstartdate.'\'';
                }   
            }
        }
        
        $category = '';
        if($params['category'] == 1) {
            $category = 'AND a.packing_status = 1 AND (a.pickup_date IS NULL OR a.pickup_date = \'0000-00-00\') AND b.address != \'\'';
        }
		if($params['category'] == 2) {
            $category = 'AND a.pickup_date IS NULL';
        }
        if($params['category'] == 3) {
            $category = 'AND g.name LIKE \'%exp%\' AND (f.packing_date IS NULL OR f.packing_date = \'0000-00-00\')';
        }
        if($params['category'] == 4) {
            $category = 'AND a.packing_status = 0';
        }
        
        $total_net = '';
        if($params['total_net'] != null) {
            $total_net = 'AND a.net_price LIKE \'%'.$params['total_net'].'%\'';
        }
		
		$sql 	= '	SELECT a.*, b.name AS customer_name, b.customer_phone, b.address, d.name AS wash_emp, e.name AS iron_emp
					FROM invoice a
					LEFT JOIN customer b ON a.customer_phone = b.customer_phone
					LEFT JOIN branch c ON a.id_branch = c.id_branch 
					LEFT JOIN employee d ON a.id_wash_emp = d.id_employee
					LEFT JOIN employee e ON a.id_iron_emp = e.id_employee
					LEFT JOIN `order` f ON a.invoice_number = f.invoice_number
					LEFT JOIN service g ON f.id_service = g.id_service
					WHERE 1=1 '.$branch.' '.$where.' '.$category.' '.$total_net.'
					GROUP BY a.invoice_number
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM invoice a
									LEFT JOIN customer b ON a.customer_phone = b.customer_phone
									LEFT JOIN branch c ON a.id_branch = c.id_branch 
									LEFT JOIN employee d ON a.id_wash_emp = d.id_employee
									LEFT JOIN employee e ON a.id_iron_emp = e.id_employee
									LEFT JOIN `order` f ON a.invoice_number = f.invoice_number
									LEFT JOIN service g ON f.id_service = g.id_service
									WHERE 1=1 '.$branch.' '.$category.' '.$total_net.' '.$where.'
									GROUP BY a.invoice_number')->num_rows();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total,
			'total' => $total,
		);
		
		return $return;
	}
	
	public function get_invoice_price($invoice) {
		$sql = "SELECT *
				FROM invoice
				WHERE invoice_number = '".$invoice."'";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}

    public function get_group($invoice) {
        $sql = "SELECT b.group
                FROM `order` a
                LEFT JOIN service b ON a.id_service = b.id_service
                WHERE a.invoice_number = '".$invoice."'
                GROUP BY b.group";
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
	
	public function get_detail($invoice) {
		$sql = "SELECT b.name, a.quantity, b.price, a.price AS total, a.packing_date, c.net_price, d.name AS wash_emp, c.wash_date, e.name AS iron_emp, c.iron_date, c.discount
				FROM `order` a
				LEFT JOIN service b ON a.id_service = b.id_service
				LEFT JOIN invoice c ON a.invoice_number = c.invoice_number
				LEFT JOIN employee d ON c.id_wash_emp = d.id_employee
                LEFT JOIN employee e ON c.id_iron_emp = e.id_employee
				WHERE a.invoice_number = '".$invoice."'";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
    
    public function get_review($invoice) {
        $id_branch = $this->session->userdata('id_branch'); 
        $sql = "SELECT c.status, d.name AS customer_name, d.address, d.`customer_phone`, c.invoice_number, c.`invoice_issue_date`, c.payment_date, c.`note`, c.`total_pcs`, c.`net_price`, c.`discount`, b.`name`, a.`quantity`, b.`price`, a.`price` AS total
                FROM `order` a
                LEFT JOIN service b ON a.id_service = b.id_service
                LEFT JOIN invoice c ON a.invoice_number = c.invoice_number
                LEFT JOIN customer d ON a.customer_phone = d.customer_phone
                WHERE a.invoice_number = '".$invoice."' AND d.id_branch = ".$id_branch." 
                GROUP BY a.id_order";
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
	
	public function updateInvoice($discount, $invoice, $netprice) {
		if(empty($discount)) {
			$discount = 0;
		}

		$fix_netprice = $netprice - $discount;

		$sql = "UPDATE invoice
				SET net_price = ".$fix_netprice.", discount = ".$discount.", status = 2, payment_date = CAST(NOW() AS DATE)
				WHERE invoice_number = '".$invoice."'";
 		$add = $this->db->query($sql);
	}
	
	public function update_pickup($invoice) {
		
		$sql = "UPDATE invoice
				SET pickup_date = NOW()
				WHERE invoice_number = '".$invoice."'";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
	
	public function edit_invoice($invoice, $payment, $pickup, $wash, $iron, $washdate, $irondate, $discount, $fnet_price, $total_pcs) {
	    $status  = '';    
	    if($payment == 'null') {
	        $changed_net = $fnet_price+$discount;
	        $status = 'status = 0, pickup_date = \'null\', discount = 0, net_price = '.$changed_net.',';
	        
	        $sql = "UPDATE invoice
            SET ".$status." payment_date = '".$payment."', id_wash_emp = ".$wash.", id_iron_emp = ".$iron.", wash_date = '".$washdate."', iron_date = '".$irondate."', total_pcs = ".$total_pcs."
            WHERE invoice_number = '".$invoice."'";
	    } else {
	        $sql = "UPDATE invoice
            SET status = 1, payment_date = '".$payment."', pickup_date = '".$pickup."', id_wash_emp = ".$wash.", id_iron_emp = ".$iron.", wash_date = '".$washdate."', iron_date = '".$irondate."', net_price = ".$fnet_price.", discount = ".$discount.", total_pcs = ".$total_pcs." 
            WHERE invoice_number = '".$invoice."'";
	    }
        
 		$add = $this->db->query($sql);
		
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
	
	public function getEmployee() {
		$sql = "SELECT *
				FROM employee";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function update_asign($asign, $invoice) {
		$update1 = false;
		$update2 = false;
 		
		if($asign == 'wash') {
		 	$sql1 = "UPDATE invoice
					SET id_wash_emp = ".$this->session->userdata('id_employee').", wash_date = CAST(NOW() AS DATE)
					WHERE invoice_number = '".$invoice."'";
			$update1 = $this->db->query($sql1);
		} 
		
		if($asign == 'iron') {
		 	$sql2 = "UPDATE invoice
					SET id_iron_emp = ".$this->session->userdata('id_employee').", iron_date = CAST(NOW() AS DATE)
					WHERE invoice_number = '".$invoice."'";
 			$update2 = $this->db->query($sql2);
		} 
 		
		if($update1 || $update2) {
			return true; 
		} else {
			return false; 
		}
	}
    
    public function check_packed($invoice) {
        $sql = "SELECT a.packing_date, b.name
                FROM `order` a
                LEFT JOIN service b ON a.id_service = b.id_service
                WHERE invoice_number = '".$invoice."'";
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
}	
