<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {
  public function __construct()
	{
		parent::__construct();				
		$this->load->model('transaction_model');

	}
	
	public function index()
	{
		$this->template->load('maintemplate', 'transaction/views/transaction_view');
	}
	
	public function list_transaction() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];} else{$order_dir = $this->session->userdata('role') == 2 ? 'asc' : 'desc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 1;}; 
        
        if($this->session->userdata('role') != 2) {
            $order_fields = array('invoice_issue_date', 'invoice_number', 'customer_name', 'net_price', 'total_pcs', 'payment_date', 'pickup_date', '');
        } else {
            $order_fields = array('invoice_issue_date', 'invoice_number', 'customer_name', 'net_price', 'total_pcs', 'id_wash_emp', 'id_iron_emp', '');
        }
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_column == 1 ? "MAX(CAST(SUBSTRING(".$order_fields[$order_column].",LOCATE('-',".$order_fields[$order_column].")+1) AS SIGNED))" : $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->transaction_model->list_transaction($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$no = 1;
		$data = array();
        $fgroup1 = 0; $fgroup2 = 0; $fgroup3 = 0; $fgroup4 = 0;
        			
		foreach ( $list['data'] as $k => $v ) {
		    $pay = '<a class="btn btn-success btn-xs" href="#" title"Pay" onclick="pay_(\''.$v['invoice_number'].'\', \''.$v['customer_phone'].'\')">Bayar</a>';
			$detail = '<a class="btn btn-warning btn-xs" href="#" title"Detail" onclick="detail_(\''.$v['invoice_number'].'\', \''.$v['note'].'\', '.$v['total_pcs'].')">Detail</a>';
			$pickup = '<a class="btn btn-info btn-xs" href="#" title"Pickup" onclick="pickup_(\''.$v['invoice_number'].'\')">Ambil</a>';
			$print = '<a class="btn btn-default btn-xs pull-right" href="#" title"Print" onclick="review_order(\''.$v['invoice_number'].'\')"><i class="glyphicon glyphicon-print"></i></a>';
			
            $washdate = $v['wash_date'] == '' || $v['wash_date'] == 0 || $v['wash_date'] == '0000-00-00' || $v['wash_date'] == null ? '' : date('d-m-Y', strtotime($v['wash_date']));
            $irondate = $v['iron_date'] == '' || $v['iron_date'] == 0 || $v['iron_date'] == '0000-00-00' || $v['iron_date'] == null ? '' : date('d-m-Y', strtotime($v['iron_date']));     
            $pickupdate = $v['pickup_date'] == '' || $v['pickup_date'] == 0 || $v['pickup_date'] == '0000-00-00' || $v['pickup_date'] == null ? '' : date('d M Y', strtotime($v['pickup_date']));
            $paymentdate = $v['payment_date'] == '' || $v['payment_date'] == 0 || $v['payment_date'] == '0000-00-00' || $v['payment_date'] == null ? '' : date('d M Y', strtotime($v['payment_date']));
            
            $done_wash = ''; $done_iron = '';
            if($v['wash_date'] == '' || $v['wash_date'] == '0000-00-00' || $v['wash_date'] == null) {
                $done_wash = '<a class="btn btn-primary btn-xs" href="#" title"Selesai Cuci" onclick="done_(\''.$v['invoice_number'].'\', \'wash\')">Cuci</a>';   
            }
            if($v['iron_date'] == '' || $v['iron_date'] == '0000-00-00' || $v['iron_date'] == null) {
                $done_iron = ' <a class="btn btn-success btn-xs" href="#" title"Selesai Setrika" onclick="done_(\''.$v['invoice_number'].'\', \'iron\')">Setrika</a>';
            }
            
			$actions = '';
			if($this->session->userdata('role') == 2) {
			    
			    $group = $this->transaction_model->get_group($v['invoice_number']);    
			    if(count($group) > 1) {
			        foreach($group as $n ) {
			            if($n['group'] == 4) {
			                $fgroup4 = 1;
			            } else if($n['group'] == 3) {
			                $fgroup3 = 1;
			            } else if($n['group'] == 2) {
                            $fgroup2 = 1;
                        } else if($n['group'] == 1) {
                            $fgroup1 = 1;
                        }
			        }
			    } else {
			        if($group[0]['group'] == 4) {
                        $fgroup4 = 1;
                    } else if($group[0]['group'] == 3) {
                        $fgroup3 = 1;
                    } else if($group[0]['group'] == 2) {
                        $fgroup2 = 1;
                    } else if($group[0]['group'] == 1) {
                        $fgroup1 = 1;
                    }
			    }
                
                if($fgroup4 == 1) {
                    $actions = $done_wash;
                    $actions .= $done_iron;
                } else if($fgroup1 == 1 || $fgroup2 == 1) {
                    if($fgroup1 == 1 && $fgroup2 == 1) {
                        $actions = $done_wash;
                    $actions .= $done_iron;
                    } else if($fgroup1 == 1 && $fgroup2 == 0) {
                        $actions .= $done_iron;
                    } else if($fgroup1 == 0 && $fgroup2 == 1) {
                       $actions = $done_wash;  
                    }
                }
                
			    //if(($this->session->userdata('id_branch') != $this->session->userdata('id_branch_emp'))) {
                //    $actions = '-';
                //} else {
                    // $actions = '<a class="btn btn-success btn-xs" href="#" title"Selesai Cuci" onclick="done_wash(\''.$v['invoice_number'].'\', \''.$v['invoice_issue_date'].'\', \''.$v['wash_date'].'\', \''.$v['iron_date'].'\')">Cuci</a>';
                    // $actions .= ' <a class="btn btn-success btn-xs" href="#" title"Selesai Setrika" onclick="done_iron(\''.$v['invoice_number'].'\', \''.$v['invoice_issue_date'].'\', \''.$v['wash_date'].'\', \''.$v['iron_date'].'\')">Setrika</a>';
                    $actions .= ' '.$detail;
                //}
                $fgroup1 = 0; $fgroup2 = 0; $fgroup3 = 0; $fgroup4 = 0;
            } else {
				if($v['status'] == 0) {
					$actions = $pay .' '; 
				}
				
				if(($v['pickup_date'] == '' || $v['pickup_date'] == null || $v['pickup_date'] == '0000-00-00') && $v['status'] != 0) {
					$actions .= $pickup .' ';
				}
				
				$actions .= $detail .' ';
				$actions .= $print;
				
				if($this->session->userdata('role') != 0) {
					if(($this->session->userdata('id_branch') != $this->session->userdata('id_branch_emp'))) {
						//$actions = '-';
                        $actions = $detail;
					}	
				}
	
				$wash = $v['id_wash_emp'] == '' || $v['id_wash_emp'] == null ? '0' : $v['id_wash_emp'];
				$iron = $v['id_iron_emp'] == '' || $v['id_iron_emp'] == null ? '0' : $v['id_iron_emp'];
				
				if($this->session->userdata('role') == 0) {
					$actions .= ' '.'<a class="btn btn-info btn-xs pull-right" href="#" title"Edit" onclick="edit_(\''.$v['invoice_number'].'\', \''.$paymentdate.'\', \''.$pickupdate.'\', '.$wash.', '.$iron.', \''.$v['wash_date'].'\', \''.$v['iron_date'].'\', '.$v['discount'].', '.$v['net_price'].', '.$v['total_pcs'].')"><i class="glyphicon glyphicon-edit"></i></a>';
				}
            }

            if($this->session->userdata('role') != 2) {
                array_push($data,  
                    array(
                        date('d M Y', strtotime($v['invoice_issue_date'])),
                        $v['invoice_number'],
                        '<a style="cursor: pointer;" title"Detail" onclick="customer_datail(\''.$v['customer_phone'].'\', \''.$v['address'].'\', \''.$v['customer_name'].'\')">'.$v['customer_name'].'</a>',
                        'Rp.'.number_format($v['net_price'] + $v['discount']),
                        $v['total_pcs'],
                        //$v['wash_emp'],
                        //$washdate,
                        //$v['iron_emp'],
                        //$irondate,
                        $paymentdate,
                        $pickupdate,
                        $actions
                    )
                );
            } else {
                array_push($data,  
                    array(
                        date('d M Y', strtotime($v['invoice_issue_date'])),
                        $v['invoice_number'],
                        '<a style="cursor: pointer;" title"Detail" onclick="customer_datail(\''.$v['customer_phone'].'\', \''.$v['address'].'\', \''.$v['customer_name'].'\')">'.$v['customer_name'].'</a>',
                        'Rp.'.number_format($v['net_price'] + $v['discount']),
                        $v['total_pcs'],
                        $v['wash_emp'],
                        //$washdate,
                        $v['iron_emp'],
                        //$irondate,
                        $actions
                    )
                );
            }
			
			$no++;
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function list_transaction_filter() {
		$startdate = $this->input->get('startdate');
		$enddate = $this->input->get('enddate');
        $category = $this->input->get('category');
        $total_net = $this->input->get('total_net');  	
		
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = $this->session->userdata('role') == 2 || $category == 1 ? 'asc' : 'desc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 1;}; 
		
        if($this->session->userdata('role') != 2) {
            $order_fields = array('invoice_issue_date', 'a.invoice_number', 'customer_name', 'net_price', 'total_pcs', 'payment_date', 'pickup_date', '');
        } else {
            $order_fields = array('invoice_issue_date', 'a.invoice_number', 'customer_name', 'net_price', 'total_pcs', 'id_wash_emp', 'id_iron_emp', '');
        }
        
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_column == 1 ? "MAX(CAST(SUBSTRING(".$order_fields[$order_column].",LOCATE('-',".$order_fields[$order_column].")+1) AS SIGNED))" : $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		$params['start'] 		= $startdate != '' ? date('Y-m-d', strtotime($startdate)) : '';
		$params['end'] 			= $enddate != '' ? date('Y-m-d', strtotime($enddate)) : '';
        $params['category']     = $category;
        $params['total_net']    = $total_net;
		
		$list = $this->transaction_model->list_transaction_filter($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$no = 1;
		$data = array();
        $fgroup1 = 0; $fgroup2 = 0; $fgroup3 = 0; $fgroup4 = 0;
        			
		foreach ( $list['data'] as $k => $v ) {
			$pay = '<a class="btn btn-success btn-xs" href="#" title"Pay" onclick="pay_(\''.$v['invoice_number'].'\', \''.$v['customer_phone'].'\')">Bayar</a>';
			$detail = '<a class="btn btn-warning btn-xs" href="#" title"Detail" onclick="detail_(\''.$v['invoice_number'].'\', \''.$v['note'].'\', '.$v['total_pcs'].')">Detail</a>';
			$pickup = '<a class="btn btn-info btn-xs" href="#" title"Pickup" onclick="pickup_(\''.$v['invoice_number'].'\')">Ambil</a>';
			$print = '<a class="btn btn-default btn-xs pull-right" href="#" title"Print" onclick="review_order(\''.$v['invoice_number'].'\')"><i class="glyphicon glyphicon-print"></i></a>';
			
            $washdate = $v['wash_date'] == '' || $v['wash_date'] == 0 || $v['wash_date'] == '0000-00-00' || $v['wash_date'] == null ? '' : date('d-m-Y', strtotime($v['wash_date'])) ;
            $irondate = $v['iron_date'] == '' || $v['iron_date'] == 0 || $v['iron_date'] == '0000-00-00' || $v['iron_date'] == null ? '' : date('d-m-Y', strtotime($v['iron_date'])) ;     
            $pickupdate = $v['pickup_date'] == '' || $v['pickup_date'] == 0 || $v['pickup_date'] == '0000-00-00' || $v['pickup_date'] == null ? '' : date('d M Y', strtotime($v['pickup_date']));
            $paymentdate = $v['payment_date'] == '' || $v['payment_date'] == 0 || $v['payment_date'] == '0000-00-00' || $v['payment_date'] == null ? '' : date('d M Y', strtotime($v['payment_date']));
            
            $done_wash = ''; $done_iron = '';
            if($v['wash_date'] == '' || $v['wash_date'] == '0000-00-00' || $v['wash_date'] == null) {
                $done_wash = '<a class="btn btn-primary btn-xs" href="#" title"Selesai Cuci" onclick="done_(\''.$v['invoice_number'].'\', \'wash\')">Cuci</a>';   
            }
            if($v['iron_date'] == '' || $v['iron_date'] == '0000-00-00' || $v['iron_date'] == null) {
                $done_iron = ' <a class="btn btn-success btn-xs" href="#" title"Selesai Setrika" onclick="done_(\''.$v['invoice_number'].'\', \'iron\')">Setrika</a>';
            }
            
			$actions = '';
			if($this->session->userdata('role') == 2) {
			    
                $group = $this->transaction_model->get_group($v['invoice_number']);    
                if(count($group) > 1) {
                    foreach($group as $n ) {
                        if($n['group'] == 4) {
                            $fgroup4 = 1;
                        } else if($n['group'] == 3) {
                            $fgroup3 = 1;
                        } else if($n['group'] == 2) {
                            $fgroup2 = 1;
                        } else if($n['group'] == 1) {
                            $fgroup1 = 1;
                        }
                    }
                } else {
                    if($group[0]['group'] == 4) {
                        $fgroup4 = 1;
                    } else if($group[0]['group'] == 3) {
                        $fgroup3 = 1;
                    } else if($group[0]['group'] == 2) {
                        $fgroup2 = 1;
                    } else if($group[0]['group'] == 1) {
                        $fgroup1 = 1;
                    }
                }
                
                if($fgroup4 == 1) {
                    $actions = $done_wash;
                    $actions .= $done_iron;
                } else if($fgroup1 == 1 || $fgroup2 == 1) {
                    if($fgroup1 == 1 && $fgroup2 == 1) {
                        $actions = $done_wash;
                    $actions .= $done_iron;
                    } else if($fgroup1 == 1 && $fgroup2 == 0) {
                        $actions .= $done_iron;
                    } else if($fgroup1 == 0 && $fgroup2 == 1) {
                       $actions = $done_wash;  
                    }
                }
                
			    //if(($this->session->userdata('id_branch') != $this->session->userdata('id_branch_emp'))) {
                //    $actions = '-';
                //} else {
                    //$actions = '<a class="btn btn-success btn-xs" href="#" title"Wash/Iron Date" onclick="done_(\''.$v['invoice_number'].'\', \''.$v['invoice_issue_date'].'\', \''.$v['wash_date'].'\', \''.$v['iron_date'].'\')">Done</a>';
                    $actions .= ' '.$detail;  
                //}
                $fgroup1 = 0; $fgroup2 = 0; $fgroup3 = 0; $fgroup4 = 0;
            } else {
				if($v['status'] == 0) {
					$actions = $pay .' '; 
				}
				
				if(($v['pickup_date'] == '' || $v['pickup_date'] == null || $v['pickup_date'] == '0000-00-00') && $v['status'] != 0) {
					$actions .= $pickup .' ';
				}
				
				$actions .= $detail .' ';
				$actions .= $print;
				
				if($this->session->userdata('role') != 0) {
					if($this->session->userdata('id_branch') != $this->session->userdata('id_branch_emp')) {
						//$actions = '-';
                        $actions = $detail;
					}
				}
	
				$wash = $v['id_wash_emp'] == '' || $v['id_wash_emp'] == null ? '0' : $v['id_wash_emp'];
				$iron = $v['id_iron_emp'] == '' || $v['id_iron_emp'] == null ? '0' : $v['id_iron_emp'];
				
				if($this->session->userdata('role') == 0) {
					$actions .= ' '.'<a class="btn btn-info btn-xs pull-right" href="#" title"Edit" onclick="edit_(\''.$v['invoice_number'].'\', \''.$paymentdate.'\', \''.$pickupdate.'\', '.$wash.', '.$iron.', \''.$v['wash_date'].'\', \''.$v['iron_date'].'\', '.$v['discount'].', '.$v['net_price'].', '.$v['total_pcs'].')"><i class="glyphicon glyphicon-edit"></i></a>';
				}
			}

            if($this->session->userdata('role') != 2) {
                array_push($data, 
                    array(
                        date('d M Y', strtotime($v['invoice_issue_date'])),
                        $v['invoice_number'],
                        '<a style="cursor: pointer;" title"Detail" onclick="customer_datail(\''.$v['customer_phone'].'\', \''.$v['address'].'\', \''.$v['customer_name'].'\')">'.$v['customer_name'].'</a>',
                        'Rp.'.number_format($v['net_price'] + $v['discount']),
                        $v['total_pcs'],
                        //v['wash_emp'],
                        //$washdate,
                        //$v['iron_emp'],
                        //$irondate,
                        $paymentdate,
                        $pickupdate,
                        $actions
                    )
                );        
            } else {
                array_push($data, 
                    array(
                        date('d M Y', strtotime($v['invoice_issue_date'])),
                        $v['invoice_number'],
                        '<a style="cursor: pointer;" title"Detail" onclick="customer_datail(\''.$v['customer_phone'].'\', \''.$v['address'].'\', \''.$v['customer_name'].'\')">'.$v['customer_name'].'</a>',
                        'Rp.'.number_format($v['net_price'] + $v['discount']),
                        $v['total_pcs'],
                        $v['wash_emp'],
                        //$washdate,
                        $v['iron_emp'],
                        //$irondate,
                        $actions
                    )
                );        
            }
			
			
			
			$no++;
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_invoice() {
		$invoice = $this->input->get('invoice');
		$phone = $this->input->get('phone');
		
		$invoice_voucher = '';
		$data = $this->transaction_model->get_invoice_price($invoice);
		
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function get_detail() {
		$invoice = $this->input->get('invoice');
		
		$data = $this->transaction_model->get_detail($invoice);
		
        $washdate = $data[0]['wash_date'] == '' || $data[0]['wash_date'] == 0 || $data[0]['wash_date'] == '0000-00-00' || $data[0]['wash_date'] == null ? '-' : date('d-m-Y', strtotime($data[0]['wash_date'])) ;
        $irondate = $data[0]['iron_date'] == '' || $data[0]['iron_date'] == 0 || $data[0]['iron_date'] == '0000-00-00' || $data[0]['iron_date'] == null ? '-' : date('d-m-Y', strtotime($data[0]['iron_date'])) ;  
        
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data,
				'wash_emp' => $data[0]['wash_emp'] == null || $data[0]['wash_emp'] == '' ? '-' : $data[0]['wash_emp'],
				'iron_emp' => $data[0]['iron_emp'] == null || $data[0]['iron_emp'] == '' ? '-' : $data[0]['iron_emp'],
				'wash_date' => $washdate,
				'iron_date' => $irondate
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    public function get_review() {
        $invoice = $this->input->get('invoice');
        
        $data = $this->transaction_model->get_review($invoice);
        $issueDate = date("d M Y", strtotime($data[0]['invoice_issue_date']));
        $paymentDate = $data[0]['payment_date'] == '' || $data[0]['payment_date'] == 0 || $data[0]['payment_date'] == '0000-00-00' || $data[0]['payment_date'] == null ? '-' : date("d M Y", strtotime($data[0]['payment_date']));
        
        if($data) {
            $result = array(
                'success' => true,
                'data' => $data,
                'issue_date' => $issueDate,
                'payment_date' => $paymentDate
            );
        } else {
            $result = array(
                'success' => false,
                'message' => 'Failed!'
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function update_invoice() {
        $netprice = $this->input->post('net_price');
        $discount = $this->input->post('discount');
        $invoice = $this->input->post('invoice');
        
		$do_update = $this->transaction_model->updateInvoice($discount, $invoice, $netprice);
        if ($do_update) {
        	return true;
        } else {
            return false;
        }
    }
	
	public function update_pickup() {
        $invoice = $this->input->post('invoice');
        
        $unpacked = '';
        $packed = $this->transaction_model->check_packed($invoice);
        foreach ( $packed as $list ) {
            if($list['packing_date'] == null || $list['packing_date'] == '0000-00-00' || $list['packing_date'] == '') {
                $unpacked .= $list['name'].' Unpacked! <br/>';  
            }
        }
        
        if($unpacked != '') {
            $result = array(
                'success' => false,
                'message' => $unpacked
            );
        } else {
            $do_update = $this->transaction_model->update_pickup($invoice);
            if ($do_update) {
                $result = array(
                    'success' => true,
                    'message' => 'Invoice updated!'
                );
            } else {
                $result = array(
                    'success' => false,
                    'message' => 'Failed to update Invoice!'
                );
            }  
        }
			
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function edit_invoice() {
        $invoice = $this->input->post('invoice');
        $payment = $this->input->post('paymentdate') != '' ? date('Y-m-d', strtotime($this->input->post('paymentdate'))) : '';
		$pickup = $this->input->post('pickupdate') != '' ? date('Y-m-d', strtotime($this->input->post('pickupdate'))) : '';
		$wash = $this->input->post('wash');
		$iron = $this->input->post('iron');
		$washdate = $this->input->post('washdate') != '' ? date('Y-m-d', strtotime($this->input->post('washdate'))) : '';
		$irondate = $this->input->post('irondate') != '' ? date('Y-m-d', strtotime($this->input->post('irondate'))) : '';
        $discount = $this->input->post('discount');
        $discount_old = $this->input->post('discount_old');
        $net_price = $this->input->post('net_price');
        $total_pcs = $this->input->post('total_pcs');
		
        $fnet_price = ($net_price + $discount_old) - $discount;
        
		if($payment == '') { $payment = 'null'; }
		if($pickup == '') { $pickup = 'null'; }
		if($wash == '') { $wash = 'null'; }
		if($iron == '') { $iron = 'null'; }
		if($washdate == '') { $washdate = 'null'; }
		if($irondate == '') { $irondate = 'null'; }
		
		$do_update = $this->transaction_model->edit_invoice($invoice, $payment, $pickup, $wash, $iron, $washdate, $irondate, $discount, $fnet_price, $total_pcs);
        if ($do_update) {
        	$result = array(
				'success' => true,
				'message' => 'Invoice updated!'
			);
        } else {
            $result = array(
				'success' => false,
				'message' => 'Failed to update Invoice!'
			);
        }
			
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function get_employee() {
		$data = $this->transaction_model->getEmployee();
		$emloyees = '';
		foreach($data as $list) {
			$emloyees .= "<option value='".$list['id_employee']."'>".$list['name']."</option>";
		}
		if($data) {
			$result = array(
				'success' => true,
				'data' => $emloyees
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function update_asign() {
        //$id = $this->input->post('id');
        //$wash = $this->input->post('washdate');
		//$iron = $this->input->post('irondate');
		$invoice = $this->input->post('invoice');
        $asign = $this->input->post('asign');
		
		$do_update = $this->transaction_model->update_asign($asign, $invoice);
        if ($do_update) {
        	$result = array(
				'success' => true,
				'message' => 'Order update!'
			);
        } else {
            $result = array(
				'success' => false,
				'message' => 'Failed to update order!'
			);
        }
			
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
