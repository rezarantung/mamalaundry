<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Report_expense_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function list_expense($params = array()) {
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND a.id_branch = '.$this->session->userdata('id_branch');
		if($this->session->userdata('role') != 0) {
			 $branch .= ' AND a.id_branch = '.$id_branch;
		}
        
        if($this->session->userdata('role') == 0) {
            $branch = '';  
        }
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (b.name LIKE \'%'.$params['filter'].'%\' OR a.description LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT a.*, b.name
                    FROM expense a 
                    LEFT JOIN branch b ON a.id_branch = b.id_branch 
					WHERE 1=1 AND a.date = CAST(NOW() AS DATE) '.$where.' '.$branch.'
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM expense a 
                                    LEFT JOIN branch b ON a.id_branch = b.id_branch 
									WHERE 1=1 '.$branch.' AND a.date = CAST(NOW() AS DATE) '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total']
		);
		
		return $return;
	}

	public function list_expense_filter($params = array()) {
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = '';
		if($this->session->userdata('role') != 0) {
			 $branch = 'AND a.id_branch = '.$id_branch;
		}

		if(!empty($params['branch'])) {
			 $branch = 'AND a.id_branch = '.$params['branch'];
		}
		
		$filter = 'AND a.date = CAST(NOW() AS DATE)';
		if(!empty($params['start']) && !empty($params['end'])) {
		    if(strtotime($params['start']) <= strtotime($params['end'])) {
		        $fstartdate = $params['start'];
                $fenddate = $params['end'];
                
                $filter = 'AND a.date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND a.date = \''.$fstartdate.'\'';
                }   
		    }
		}
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (b.name LIKE \'%'.$params['filter'].'%\' OR a.description LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT a.*, b.name
                    FROM expense a 
                    LEFT JOIN branch b ON a.id_branch = b.id_branch 
					WHERE 1=1 '.$filter.' '.$where.' '.$branch.'
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM expense a 
                                    LEFT JOIN branch b ON a.id_branch = b.id_branch 
									WHERE 1=1 '.$branch.' '.$filter.' '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total']
		);
		
		return $return;
	}

	public function sumExpense() {
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND id_branch = '.$this->session->userdata('id_branch');
		if($this->session->userdata('role') != 0) {
			 $branch .= ' AND id_branch = '.$id_branch;
		}

        if($this->session->userdata('role') == 0) {
            $branch = '';  
        }
			
		$sql 	= '	SELECT SUM(amount) AS `sum`
					FROM expense
					WHERE date = CAST(NOW() AS DATE) '.$branch.'';
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		return $result;
	}
	
	public function sumExpense_filter($startdate, $enddate, $branch) {
		$id_branch = $this->session->userdata('id_branch');   
        
        $fbranch = '';
        if($this->session->userdata('role') != 0) {
             $fbranch = 'AND id_branch = '.$id_branch;
        }

		if(!empty($branch)) {
			 $fbranch = 'AND id_branch = '.$branch;
		}
		
		$filter = 'AND date = CAST(NOW() AS DATE)';
		if(!empty($startdate) && !empty($enddate)) {
		    if(strtotime($startdate) <= strtotime($enddate)) {
		        $fstartdate = $startdate;
                $fenddate = $enddate;
                
                $filter = 'AND date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND date = \''.$fstartdate.'\'';
                }
		    }
		}
		
		$sql 	= '	SELECT SUM(amount) AS `sum`
					FROM expense
					WHERE 1=1 '.$filter.' '.$fbranch.'';
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		return $result;
	}
	
	public function getBranch() {
		$sql = "SELECT * FROM branch";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
    
    public function getBranchName($branch) {
        $sql = "SELECT name FROM branch WHERE id_branch = ".$branch;
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
    
    public function getBranchReport() {
        $id_branch = $this->session->userdata('id_branch'); 
        
        $branch = '';
        if($this->session->userdata('role') != 0) {
             $branch = 'WHERE id_branch = '.$id_branch;
        }
        
        $sql = "SELECT name FROM branch ". $branch;
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
    
    public function insert_expense($branch, $date, $description, $amount) {
        
        $fdate = 'CAST(NOW() AS DATE)';
        if(!empty($date)) {
            $fdate = '\''.$date.'\'';
        }
            
        $fbranch = $branch;       
        if(empty($branch)) {
            if($this->session->userdata('role') != 0) {
                $fbranch = $this->session->userdata('id_branch_emp');
            } else {
                $fbranch = $this->session->userdata('id_branch');
            }
        }
        
        $sql = "INSERT INTO expense (id_branch, date, description, amount) VALUES (".$fbranch.", ".$fdate.", '".$description."', ".$amount.")";
        $add = $this->db->query($sql);
        if($add) {
            return true; 
        } else {
            return false; 
        }
    }

    public function update_expense($id, $branch, $date, $description, $amount) {
        
        $fdate = 'CAST(NOW() AS DATE)';
        if(!empty($date)) {
            $fdate = '\''.$date.'\'';
        }
            
        $fbranch = $branch;       
        if(empty($branch)) {
            if($this->session->userdata('role') != 0) {
                $fbranch = $this->session->userdata('id_branch_emp');
            } else {
                $fbranch = $this->session->userdata('id_branch');
            }
        }
        
        $sql = "UPDATE expense
                SET id_branch = ".$fbranch.", date = ".$fdate.", description = '".$description."', amount = ".$amount."
                WHERE id_expense = ".$id;
        $add = $this->db->query($sql);
        if($add) {
            return true; 
        } else {
            return false; 
        }
    }
    
    public function delete($id) {
        $sql = "DELETE FROM expense WHERE id_expense = ". $id;
        $delete = $this->db->query($sql);
        if($delete) {
            return true; 
        } else {
            return false; 
        }
    }
    
    public function list_expense_pdf($params = array()) {
        
        $id_branch = $this->session->userdata('id_branch'); 
        
        $branch = 'AND a.id_branch = '.$this->session->userdata('id_branch');
        if($this->session->userdata('role') != 0) {
             $branch .= ' AND a.id_branch = '.$id_branch;
        }
        
        if($this->session->userdata('role') == 0) {
            $branch = '';  
        }
        
        if(!empty($params['branch'])) {
             $branch = 'AND a.id_branch = '.$params['branch'];
        }
        
        $filter = 'AND a.date = CAST(NOW() AS DATE)';
        if(!empty($params['start']) && !empty($params['end'])) {
            if(strtotime($params['start']) <= strtotime($params['end'])) {
                $fstartdate = $params['start'];
                $fenddate = $params['end'];
                
                $filter = 'AND a.date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND a.date = \''.$fstartdate.'\'';
                }   
            }
        }
            
        $sql    = ' SELECT a.*, b.name
                    FROM expense a 
                    LEFT JOIN branch b ON a.id_branch = b.id_branch 
                    WHERE 1=1 '.$filter.' '.$branch.'
                    ORDER BY a.date ASC';
        
        
        $query  =  $this->db->query($sql);
        $result = $query->result_array(); 
        
        $return = array(
            'data' => $result
        );
        
        return $return;
    }
}	
