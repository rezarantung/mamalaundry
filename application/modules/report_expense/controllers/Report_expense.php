<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Report_expense extends CI_Controller {
  public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('role') == 2) { redirect('dashboard'); }   
				
		$this->load->model('report_expense_model');

	}
	
	public function index()
	{
		$sum_expense = $this->report_expense_model->sumExpense();
		$branch = $this->report_expense_model->getBranch();
        $branch_report = $this->report_expense_model->getBranchReport();
		
		$data['sum_expense'] = $sum_expense[0]['sum'] < 0 ? '(Rp.'.number_format(abs($sum_expense[0]['sum'])).')' : 'Rp.'.number_format($sum_expense[0]['sum']);
		
		$options = '<option value="">All</option>';
		foreach ($branch as $list) {
			$options .= '<option value="'.$list['id_branch'].'">'.$list['name'].'</option>';
		}
        
        $options1 = '<option value="">-- Select Branch --</option>';
        foreach ($branch as $list) {
            $options1 .= '<option value="'.$list['id_branch'].'">'.$list['name'].'</option>';
        }
        
		$data['branch'] = $options;
        $data['add_expense_branch'] = $options1;
        $data['edit_expense_branch'] = $options1;
        $data['branch_report'] = count($branch_report) > 1 ? 'All' : $branch_report[0]['name'];
		
		$this->template->load('maintemplate', 'report_expense/views/report_expense_view', $data);
	}
	
	public function list_expense() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('date', 'description', 'amount');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->report_expense_model->list_expense($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
		    $actions = '-';
		    //if($this->session->userdata('role') == 0) {
		          $actions = '<a class="btn btn-primary btn-xs" href="#" title"Edit" onclick="edit_('.$v['id_expense'].', \''.$v['description'].'\', '.$v['amount'].', '.$v['id_branch'].', \''.$v['date'].'\')"><i class="glyphicon glyphicon-edit"></i></a>';
    		      $actions .= ' ';  
                  $actions .= '<a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_('.$v['id_expense'].', \''.$v['description'].'\')"><i class="glyphicon glyphicon-trash"></i></a>';  
            //}
            
            if($this->session->userdata('role') != 0) {
                if(($this->session->userdata('id_branch') != $this->session->userdata('id_branch_emp'))) {
                    $actions = '-';
                }   
            }
            
            $amount = $v['amount'] < 0 ? '(Rp.'.number_format(abs($v['amount'])).')' : 'Rp.'.number_format($v['amount']);
            
			array_push($data, 
				array(
					date('d M Y', strtotime($v['date'])),
					//$v['name'],
					$v['description'],
					$amount,
					$actions
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function list_expense_filter() {
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');
		$branch = $this->input->post('branch');
		
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('date', 'description', 'amount');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		$params['start']        = $startdate != '' ? date('Y-m-d', strtotime($startdate)) : '';
        $params['end']          = $enddate != '' ? date('Y-m-d', strtotime($enddate)) : '';
		$params['branch'] 		= $branch;
		
		$list = $this->report_expense_model->list_expense_filter($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
		    $actions = '-';
            //if($this->session->userdata('role') == 0) {
                  $actions = '<a class="btn btn-primary btn-xs" href="#" title"Edit" onclick="edit_('.$v['id_expense'].', \''.$v['description'].'\', '.$v['amount'].', '.$v['id_branch'].', \''.$v['date'].'\')"><i class="glyphicon glyphicon-edit"></i></a>';
                  $actions .= ' ';  
                  $actions .= '<a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_('.$v['id_expense'].', \''.$v['description'].'\')"><i class="glyphicon glyphicon-trash"></i></a>';  
            //}
            
            if($this->session->userdata('role') != 0) {
                if(($this->session->userdata('id_branch') != $this->session->userdata('id_branch_emp'))) {
                    $actions = '-';
                }   
            }
            
            $amount = $v['amount'] < 0 ? '(Rp.'.number_format(abs($v['amount'])).')' : 'Rp.'.number_format($v['amount']);
            
			array_push($data, 
				array(
                    date('d M Y', strtotime($v['date'])),
                    //$v['name'],
                    $v['description'],
                    $amount,
                    $actions
                )
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function sum_expense_filter() {
		$startdate = $this->input->get('startdate') != '' || $this->input->get('startdate') != null ? date('Y-m-d', strtotime($this->input->get('startdate'))) : '';
        $enddate = $this->input->get('enddate') != '' || $this->input->get('enddate') != null ? date('Y-m-d', strtotime($this->input->get('enddate'))) : '';
		$branch = $this->input->get('branch');
		
		$sum = $this->report_expense_model->sumExpense_filter($startdate, $enddate, $branch);
        
        $branch_name = 'All';
        
        $id_branch = $this->session->userdata('id_branch_emp'); 
        if($this->session->userdata('role') != 0) {
             $branch = $id_branch;
        }
        
        if($branch != '') {
            $get_branch_name = $this->report_expense_model->getBranchName($branch);  
            $branch_name = $get_branch_name[0]['name']; 
        }
        
        $result = array(
            'sum' => $sum,
            'start' => date("d M Y", strtotime($startdate)),
            'end' => date("d M Y", strtotime($enddate)),
            'branch' => $branch_name
        );
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    public function add_expense() {
        $branch = $this->input->post('branch');
        $date = $this->input->post('date') != '' ? date('Y-m-d', strtotime($this->input->post('date'))) : '';
        $description = $this->input->post('description');
        $amount = $this->input->post('amount');
        
        $insert_expense = $this->report_expense_model->insert_expense($branch, $date, $description, $amount);
        if($insert_expense) {
            return true;
        } else {
            return false;
        }
    }
    
    public function edit_expense() {
        $id = $this->input->post('id');
        $branch = $this->input->post('branch');
        $date = $this->input->post('date') != '' ? date('Y-m-d', strtotime($this->input->post('date'))) : '';
        $description = $this->input->post('description');
        $amount = $this->input->post('amount');
        
        $update_expense = $this->report_expense_model->update_expense($id, $branch, $date, $description, $amount);
        if($update_expense) {
            return true;
        } else {
            return false;
        }
    }
    
    public function delete_expense() {
        
        $id = $this->input->post('id');
        
        $do_delete = $this->report_expense_model->delete($id);
        if ($do_delete) {
            $result = array(
                'success' => true,
                'message' => 'Expense deleted!'
            );
        } else {
            $result = array(
                'success' => false,
                'message' => 'Failed to delete expense!'
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    public function list_expense_pdf() {
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $branch = $this->input->get('branch');
        
        $params['start']        = $startdate != '' || $startdate != null ? date('Y-m-d', strtotime($startdate)) : '' ;
        $params['end']          = $enddate != '' || $enddate != null ? date('Y-m-d', strtotime($enddate)) : '' ;
        $params['branch']       = $branch;
        
        $list = $this->report_expense_model->list_expense_pdf($params);
        
        $data = array();            
        foreach ( $list['data'] as $k => $v ) {
                
            $amount = $v['amount'] < 0 ? '('.number_format(abs($v['amount'])).')' : number_format($v['amount']);
            
            array_push($data, 
                array(
                    date('d M Y', strtotime($v['date'])),
                    $v['description'],
                    $amount
                )
            );
        }
        
        $result["data"] = $data;
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
