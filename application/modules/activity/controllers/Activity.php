<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {
  public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('role') != 0) { redirect('dashboard'); }		
		$this->load->model('activity_model');

	}
	
	public function index()
	{
		$this->template->load('maintemplate', 'activity/views/activity_view');
	}
	
	public function list_activity() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 2;}; 
		$order_fields = array('username', 'modul', 'date');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->activity_model->list_activity($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			array_push($data, 
				array(
					$v['username'],
					$v['modul'],
					date('d M Y H:i:s', strtotime($v['date']))
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function list_activity_filter() {
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');
		
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 2;}; 
		$order_fields = array('username', 'modul', 'date');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		$params['start']        = $startdate != '' ? date('Y-m-d H:i:s', strtotime($startdate)) : '';
        $params['end']          = $enddate != '' ? date('Y-m-d H:i:s', strtotime($enddate)) : '';
		
		$list = $this->activity_model->list_activity_filter($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			array_push($data, 
				array(
					$v['username'],
					$v['modul'],
					date('d M Y H:i:s', strtotime($v['date']))
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		
	}
    
    public function clear_history() {
        $clear_history = $this->activity_model->clear_history();
          
        if ($clear_history) {
            $result = array(
                'success' => true
            );
        } else {
            $result = array(
                'success' => false
            );
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
