<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Activity_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function list_activity($params = array()) { 
		$branch = $this->session->userdata('id_branch');	
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (b.username LIKE \'%'.$params['filter'].'%\' OR a.modul LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT b.username, a.modul, a.date
					FROM activity a
					LEFT JOIN user b ON b.id_user = a.id_user 
					WHERE 1=1 AND a.id_branch = '.$branch.' '.$where.'
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM activity a
									LEFT JOIN user b ON b.id_user = a.id_user 
									WHERE 1=1 AND a.id_branch = '.$branch.' '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total'],
		);
		
		return $return;
	}
	
	public function list_activity_filter($params = array()) { 
		$branch = $this->session->userdata('id_branch');	
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (b.username LIKE \'%'.$params['filter'].'%\' OR a.modul LIKE \'%'.$params['filter'].'%\') ';
		}
		if(!empty($params['start']) && !empty($params['end'])) {
			$where .= 'AND a.date BETWEEN CAST(\''.$params['start'].'\' AS DATETIME) AND CAST(\''.$params['end'].'\' AS DATETIME)';
		}
		
		$sql 	= '	SELECT b.username, a.modul, a.date
					FROM activity a
					LEFT JOIN user b ON b.id_user = a.id_user 
					WHERE 1=1 AND a.id_branch = '.$branch.' '.$where.'
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM activity a
									LEFT JOIN user b ON b.id_user = a.id_user 
									WHERE 1=1 AND a.id_branch = '.$branch.' '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total'],
		);
		
		return $return;
	}
    
    public function clear_history() {
        $sql = "DELETE FROM activity";
        $clear = $this->db->query($sql);
        
        if($clear) {
            return true;
        } else {
            return false;
        }
    }
}	
