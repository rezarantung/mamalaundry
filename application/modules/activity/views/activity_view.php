<script>
	var table

	$(document).ready(function(){
		table = $('#activity_list').DataTable({
			"processing": true,
			"serverSide": true,
	        "ajax": {
	        	'type' : 'get',
	        	'url': 'activity/list_activity'     
	        },
            "columnDefs": [{
                "targets": 1,
                "orderable": false
            }],
            "order": []
	    });
	    
	    $('#startdate').datetimepicker({
	    	format:'d-m-Y H:i:s'
	    });
	    $('#enddate').datetimepicker({
	    	format:'d-m-Y H:i:s'
	    });
	    
	});
	
	function filter() {
		var start = $('#startdate').val();
		var end = $('#enddate').val();
		
		if(start != '' && end != '') {
		    if(Date.parse(start) > Date.parse(end)) {
		        $("#danger_list").html('Startdate should be lower than Enddate!');
                $("#danger_list").show();
                setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
		    } else {
		        $('#activity_list').DataTable().destroy();
                table = $('#activity_list').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        'type' : 'post',
                        'url': 'activity/list_activity_filter',
                        'data': {
                            'startdate': start,
                            'enddate': end
                        }     
                    }
                });
		    }
		} else {
			$("#danger_list").html('Start date or End Date is empty!');
			$("#danger_list").show();
			setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
		}
		
	}
    
    function delete_all() {
        var r = confirm("Are you sure want clear history?","Ok","Cancel");
        if(r){
            $.ajax({
                type: 'post',
                url: "activity/clear_history", 
                success: function(response){
                    if(response.success) {
                        table.ajax.reload();
                        $("#alert_list").html("History Cleared!");
                        $("#alert_list").show();
                        setTimeout(function() { $("#alert_list").slideUp(); }, 4000);
                    } else {
                        //$("#danger_list").html(response.message);
                        $("#danger_list").html("Clear History Failed!");
                        $("#danger_list").show();
                        setTimeout(function() { $("#danger_list").slideUp(); }, 4000);
                    } 
                },
                async: false
            });
        }
    }
</script>
<style type="text/css">
    input[type=text], input[type=tel], textarea {
        text-transform: uppercase !important;
    }
</style>
<div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Activity</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Log Activity</h2>
                <a class="btn btn-danger btn-xs pull-right" href="#" onclick="delete_all();"><i class="glyphicon glyphicon-remove"></i> Delete All</a>
            </div>
            <div id="alert_list" class="alert alert-success" style="display: none; text-align: center;"></div>
            <div id="danger_list" class="alert alert-danger" style="display: none; text-align: center;"></div>
            
            <div class="box-content row">
            	<div class="col-lg-12 col-md-12">
            		
                        <div class="col-lg-5 col-md-5">
                        	<input name="startdate" id="startdate" type="text" class="form-control" placeholder="Start Date">
                        </div>
                    
                        <div class="col-lg-5 col-md-5">
                        	<input name="enddate" id="enddate" type="text" class="form-control" placeholder="End Date">
                        </div>
                        
                        <div class="col-lg-2 col-md-2">
                        	<input name="filter" id="filter" type="submit" class="form-control btn btn-primary" value="Filter" onclick="filter();">
                        </div>
                        <div class="clearfix"></div><br>
                    
            	</div>
                <div class="col-lg-12 col-md-12">
                    <table id="activity_list" width="100%" class="table table-striped">
                        <thead>
                        	<tr>
                        		<th>USERNAME</th>
                        		<th>ACTIVITY</th>
                        		<th>DATE</th>
                    		</tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->
    