<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Report_sales_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function list_sales($params = array()) {
		
		// $enddate = strtotime(date("Y-m-d"));
		// $startdate = strtotime("-1 months", $enddate); 
// 		
		// $fstartdate = date("Y-m-d", $startdate);
		// $fenddate = date("Y-m-d", $enddate);
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND b.id_branch = '.$this->session->userdata('id_branch');
		if($this->session->userdata('role') != 0) {
			 $branch .= ' AND a.id_branch = '.$id_branch;
		}
        
        if($this->session->userdata('role') == 0) {
            $branch = '';  
        }
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (a.invoice_number LIKE \'%'.$params['filter'].'%\' OR b.name LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT a.*, b.name AS customer_name
					FROM invoice a
					LEFT JOIN customer b ON a.customer_phone = b.customer_phone
					WHERE 1=1 AND invoice_issue_date = CAST(NOW() AS DATE) '.$where.' '.$branch.'
					GROUP BY a.invoice_number
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM invoice a
									LEFT JOIN customer b ON a.customer_phone = b.customer_phone
									WHERE 1=1 '.$branch.' AND invoice_issue_date = CAST(NOW() AS DATE) '.$where.'
									GROUP BY a.invoice_number')->num_rows();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total,
			'total' => $total
		);
		
		return $return;
	}

	public function list_sales_filter($params = array()) {
		
		$id_branch = $this->session->userdata('id_branch_emp');	
		
		$branch = '';
		if($this->session->userdata('role') != 0) {
			 $branch = 'AND a.id_branch = '.$id_branch;
		}

		if(!empty($params['branch'])) {
			 $branch = 'AND a.id_branch = '.$params['branch'];
		}
		
		$filter = 'AND a.invoice_issue_date = CAST(NOW() AS DATE)';
		if(!empty($params['start']) && !empty($params['end'])) {
		    if(strtotime($params['start']) <= strtotime($params['end'])) {
		        $fstartdate = $params['start'];
                $fenddate = $params['end'];
                
                $filter = 'AND a.invoice_issue_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND a.invoice_issue_date = \''.$fstartdate.'\'';
                } 
		    }
		}
			
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (a.invoice_number LIKE \'%'.$params['filter'].'%\' OR b.name LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT a.*, b.name AS customer_name
					FROM invoice a
					LEFT JOIN customer b ON a.customer_phone = b.customer_phone
					WHERE 1=1 '.$filter.' '.$where.' '.$branch.'
					GROUP BY a.invoice_number
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM invoice a
									LEFT JOIN customer b ON a.customer_phone = b.customer_phone
									WHERE 1=1 '.$branch.' '.$filter.' '.$where.'
									GROUP BY a.invoice_number')->num_rows();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total,
			'total' => $total
		);
		
		return $return;
	}

	public function sumSales() {
		
		// $enddate = strtotime(date("Y-m-d"));
		// $startdate = strtotime("-1 months", $enddate); 
// 		
		// $fstartdate = date("Y-m-d", $startdate);
		// $fenddate = date("Y-m-d", $enddate);
		
		$id_branch = $this->session->userdata('id_branch');	
		
		$branch = 'AND id_branch = '.$this->session->userdata('id_branch');
		if($this->session->userdata('role') != 0) {
			 $branch .= ' AND id_branch = '.$id_branch;
		}

        if($this->session->userdata('role') == 0) {
            $branch = '';  
        }
			
		$sql 	= '	SELECT SUM(net_price + discount) AS sum
					FROM invoice
					WHERE invoice_issue_date = CAST(NOW() AS DATE) '.$branch.'';
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		return $result;
	}
	
	public function sumSales_filter($startdate, $enddate, $branch) {
		$id_branch = $this->session->userdata('id_branch');   
        
        $fbranch = '';
        if($this->session->userdata('role') != 0) {
             $fbranch = 'AND id_branch = '.$id_branch;
        }
	
		if(!empty($branch)) {
			 $fbranch = 'AND id_branch = '.$branch;
		}
		
		$filter = 'AND invoice_issue_date = CAST(NOW() AS DATE)';
		if(!empty($startdate) && !empty($enddate)) {
		    if(strtotime($startdate) <= strtotime($enddate)) {
    			$fstartdate = $startdate;
    			$fenddate = $enddate;
    			
    			$filter = 'AND invoice_issue_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
    			if($fstartdate == $fenddate) {
    				$filter = 'AND invoice_issue_date = \''.$fstartdate.'\'';
    			}
            }
		}
		
		$sql 	= '	SELECT SUM(net_price + discount) AS sum
					FROM invoice
					WHERE 1=1 '.$filter.' '.$fbranch.'';
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		return $result;
	}
	
	public function get_detail($invoice) {
		$sql = "SELECT b.name, a.*, DATE_FORMAT(a.packing_date, \"%d %M %Y\") AS packing_date
				FROM `order` a
				LEFT JOIN service b ON a.id_service = b.id_service
				WHERE invoice_number = '".$invoice."'";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function getBranch() {
		$sql = "SELECT * FROM branch";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
    
    public function getBranchName($branch) {
        $sql = "SELECT name FROM branch WHERE id_branch = ".$branch;
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
    
    public function getBranchReport() {
        $id_branch = $this->session->userdata('id_branch'); 
        
        $branch = '';
        if($this->session->userdata('role') != 0) {
             $branch = 'WHERE id_branch = '.$id_branch;
        }
        
        $sql = "SELECT name FROM branch ". $branch;
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
    
    public function list_sales_pdf($params = array()) {
        
        $id_branch = $this->session->userdata('id_branch'); 
        
        $branch = 'AND b.id_branch = '.$this->session->userdata('id_branch');
        if($this->session->userdata('role') != 0) {
             $branch .= ' AND a.id_branch = '.$id_branch;
        }
        
        if($this->session->userdata('role') == 0) {
            $branch = '';  
        }

        if(!empty($params['branch'])) {
             $branch = 'AND a.id_branch = '.$params['branch'];
        }
        
        $filter = 'AND a.invoice_issue_date = CAST(NOW() AS DATE)';
        if(!empty($params['start']) && !empty($params['end'])) {
            if(strtotime($params['start']) <= strtotime($params['end'])) {
                $fstartdate = $params['start'];
                $fenddate = $params['end'];
                
                $filter = 'AND a.invoice_issue_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND a.invoice_issue_date = \''.$fstartdate.'\'';
                } 
            }
        }
            
        $sql    = ' SELECT a.*, b.name AS customer_name
                    FROM invoice a
                    LEFT JOIN customer b ON a.customer_phone = b.customer_phone
                    WHERE 1=1 '.$filter.' '.$branch.'
                    GROUP BY a.invoice_number
                    ORDER BY a.invoice_issue_date ASC';
        
        
        $query  =  $this->db->query($sql);
        $result = $query->result_array(); 
        
        $return = array(
            'data' => $result
        );
        
        return $return;
    }
}	
