<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Report_sales extends CI_Controller {
  public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('role') == 2) { redirect('dashboard'); }
				
		$this->load->model('report_sales_model');

	}
	
	public function index()
	{
		$sum_sales = $this->report_sales_model->sumSales();
		$branch = $this->report_sales_model->getBranch();
        $branch_report = $this->report_sales_model->getBranchReport();
		
		$data['sum_sales'] = $sum_sales[0]['sum'];
		
		$options = '<option value="">All</option>';
		foreach ($branch as $list) {
			$options .= '<option value="'.$list['id_branch'].'">'.$list['name'].'</option>';
		}
		$data['branch'] = $options;
        $data['branch_report'] = count($branch_report) > 1 ? 'All' : $branch_report[0]['name'];
		
		$this->template->load('maintemplate', 'report_sales/views/report_sales_view', $data);
	}
	
	public function list_sales() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('invoice_issue_date', 'invoice_number', 'customer_name', 'net_price');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_column == 1 ? "MAX(CAST(SUBSTRING(".$order_fields[$order_column].",LOCATE('-',".$order_fields[$order_column].")+1) AS SIGNED))" : $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->report_sales_model->list_sales($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			array_push($data, 
				array(
					date('d M Y', strtotime($v['invoice_issue_date'])),
					'<a style="cursor: pointer;" title"Detail" onclick="datail_(\''.$v['invoice_number'].'\')">'.$v['invoice_number'].'</a>',
					$v['customer_name'],
					'Rp.'.number_format($v['net_price'] + $v['discount'])
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function list_sales_filter() {
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');
		$branch = $this->input->post('branch');
		
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('invoice_issue_date', 'invoice_number', 'customer_name', 'net_price');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_column == 1 ? "MAX(CAST(SUBSTRING(".$order_fields[$order_column].",LOCATE('-',".$order_fields[$order_column].")+1) AS SIGNED))" : $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		$params['start']        = $startdate != '' ? date('Y-m-d', strtotime($startdate)) : '';
        $params['end']          = $enddate != '' ? date('Y-m-d', strtotime($enddate)) : '';
		$params['branch'] 		= $branch;
		
		$list = $this->report_sales_model->list_sales_filter($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			array_push($data, 
				array(
					date('d M Y', strtotime($v['invoice_issue_date'])),
					'<a style="cursor: pointer;" title"Detail" onclick="datail_(\''.$v['invoice_number'].'\')">'.$v['invoice_number'].'</a>',
					$v['customer_name'],
					'Rp.'.number_format($v['net_price'] + $v['discount'])
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function sum_sales_filter() {
		$startdate = $this->input->get('startdate') != '' ? date('Y-m-d', strtotime($this->input->get('startdate'))) : '';
        $enddate = $this->input->get('enddate') != '' ? date('Y-m-d', strtotime($this->input->get('enddate'))) : '';
		$branch = $this->input->get('branch');
		
		$sum = $this->report_sales_model->sumSales_filter($startdate, $enddate, $branch);
        
        $branch_name = 'All';
        
        $id_branch = $this->session->userdata('id_branch_emp'); 
        if($this->session->userdata('role') != 0) {
             $branch = $id_branch;
        }
        
        if($branch != '') {
            $get_branch_name = $this->report_sales_model->getBranchName($branch);  
            $branch_name = $get_branch_name[0]['name']; 
        }
        
        $result = array(
            'sum' => $sum,
            'start' => date("d M Y", strtotime($startdate)),
            'end' => date("d M Y", strtotime($enddate)),
            'branch' => $branch_name
        );
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function get_detail() {
		$invoice = $this->input->get('invoice');
		
		$data = $this->report_sales_model->get_detail($invoice);
		
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    public function list_sales_pdf() {
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $branch = $this->input->get('branch');
        
        $params['start']        = $startdate != '' || $startdate != null ? date('Y-m-d', strtotime($startdate)) : '' ;
        $params['end']          = $enddate != '' || $enddate != null ? date('Y-m-d', strtotime($enddate)) : '' ;
        $params['branch']       = $branch;
        
        $list = $this->report_sales_model->list_sales_pdf($params);
        
        $data = array();            
        foreach ( $list['data'] as $k => $v ) {
            array_push($data, 
                array(
                    date('d M Y', strtotime($v['invoice_issue_date'])),
                    $v['invoice_number'],
                    $v['customer_name'],
                    number_format($v['net_price'] + $v['discount'])
                )
            );
        }
        
        $result["data"] = $data;
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
