<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Report_salary_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function list_salary($params = array()) {
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (name LIKE \'%'.$params['filter'].'%\')';
		}
		
		// $sql 	= '	SELECT *
					// FROM employee 
					// WHERE 1=1 '.$where.'
					// ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					// LIMIT '.$params['limit'].'
					// OFFSET ' . $params['offset'];  
		
        $sql    = ' SELECT emp.*, (SELECT CASE WHEN SUM(b.wash_salary_emp) IS NULL THEN 0 ELSE SUM(b.wash_salary_emp) END
                            FROM invoice a
                            LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
                            LEFT JOIN service c ON b.id_service = c.id_service
                            WHERE a.id_wash_emp = emp.id_employee AND a.wash_date = CAST(NOW() AS DATE)) +
                        (SELECT CASE WHEN SUM(b.iron_salary_emp) IS NULL THEN 0 ELSE SUM(b.iron_salary_emp) END
                        FROM invoice a
                        LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
                        LEFT JOIN service c ON b.id_service = c.id_service
                        WHERE a.id_iron_emp = emp.id_employee AND a.iron_date = CAST(NOW() AS DATE)) AS total_salary,
                    (SELECT SUM(b.price)
                    FROM invoice a
                    LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
                    LEFT JOIN service c ON b.id_service = c.id_service
                    WHERE a.id_wash_emp = emp.id_employee AND a.wash_date = CAST(NOW() AS DATE)) AS wash_basis,
                        (SELECT SUM(b.price)
                        FROM invoice a
                        LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
                        LEFT JOIN service c ON b.id_service = c.id_service
                        WHERE a.id_iron_emp = emp.id_employee AND a.iron_date = CAST(NOW() AS DATE)) AS iron_basis
                    FROM employee emp 
                    WHERE 1=1 '.$where.'
                    ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
                    LIMIT '.$params['limit'].'
                    OFFSET ' . $params['offset']; 
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM employee
									WHERE 1=1 '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total']
		);
		
		return $return;
	}
	
	public function list_salary_filter($params = array()) {
	    $where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (name LIKE \'%'.$params['filter'].'%\')';
		}

        $branch = '';
        if(!empty($params['branch'])) {
             $branch = 'AND b.id_branch = '.$params['branch'];
        }
        
        $filterW = 'AND a.wash_date = CAST(NOW() AS DATE)';
		$filterI = 'AND a.iron_date = CAST(NOW() AS DATE)';
        if(!empty($params['start']) && !empty($params['end'])) {
            if(strtotime($params['start']) <= strtotime($params['end'])) {
                $fstartdate = $params['start'];
                $fenddate = $params['end'];
                
                $filterW = 'AND a.wash_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
				$filterI = 'AND a.iron_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filterW = 'AND a.wash_date = \''.$fstartdate.'\'';
					$filterI = 'AND a.iron_date = \''.$fstartdate.'\'';
                }   
            }
        }
		
		$sql 	= '	SELECT emp.*, (SELECT CASE WHEN SUM(b.wash_salary_emp) IS NULL THEN 0 ELSE SUM(b.wash_salary_emp) END
                            FROM invoice a
                            LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
                            LEFT JOIN service c ON b.id_service = c.id_service
                            WHERE a.id_wash_emp = emp.id_employee '.$filterW.' '.$branch.') +
                        (SELECT CASE WHEN SUM(b.iron_salary_emp) IS NULL THEN 0 ELSE SUM(b.iron_salary_emp) END
                        FROM invoice a
                        LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
                        LEFT JOIN service c ON b.id_service = c.id_service
                        WHERE a.id_iron_emp = emp.id_employee '.$filterI.' '.$branch.') AS total_salary,
                    (SELECT SUM(b.price)
                    FROM invoice a
                    LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
                    LEFT JOIN service c ON b.id_service = c.id_service
                    WHERE a.id_wash_emp = emp.id_employee '.$filterW.' '.$branch.') AS wash_basis,
                        (SELECT SUM(b.price)
                        FROM invoice a
                        LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
                        LEFT JOIN service c ON b.id_service = c.id_service
                        WHERE a.id_iron_emp = emp.id_employee '.$filterI.' '.$branch.') AS iron_basis
                    FROM employee emp 
					WHERE 1=1 '.$where.'
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM employee
									WHERE 1=1 '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total']
		);
		
		return $return;
	}

	public function detail_wash_salary($employee) {
		//$from = date("Y-m-d", strtotime( "previous saturday" ));
		//$to = date("Y-m-d");
		
		$sql = "SELECT SUM(b.wash_salary_emp) AS wash_salary, SUM(b.price) AS basis
				FROM invoice a
				LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
				LEFT JOIN service c ON b.id_service = c.id_service
				WHERE a.id_wash_emp = ".$employee." AND a.wash_date = CAST(NOW() AS DATE)";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}

	public function detail_wash_salary_filter($employee, $startdate, $enddate, $branch_) {
	    $branch = '';
        if(!empty($branch_)) {
             $branch = 'AND b.id_branch = '.$branch_;
        }
        
        $filter = 'AND a.wash_date = CAST(NOW() AS DATE)';
        if(!empty($startdate) && !empty($enddate)) {
            if(strtotime($startdate) <= strtotime($enddate)) {
                $fstartdate = $startdate;
                $fenddate = $enddate;
                
                $filter = 'AND a.wash_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND a.wash_date = \''.$fstartdate.'\'';
                }   
            }
        }
        
		$sql = "SELECT SUM(b.wash_salary_emp) AS wash_salary, SUM(b.price) AS basis
				FROM invoice a
				LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
				LEFT JOIN service c ON b.id_service = c.id_service
				WHERE 1=1 ".$branch." AND a.id_wash_emp = ".$employee." ".$filter." ";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function detail_iron_salary($employee) {
		//$from = date("Y-m-d", strtotime( "previous saturday" ));
		//$to = date("Y-m-d");
		
		$sql = "SELECT SUM(b.iron_salary_emp) AS iron_salary, SUM(b.price) AS basis
				FROM invoice a
				LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
				LEFT JOIN service c ON b.id_service = c.id_service
				WHERE a.id_iron_emp = ".$employee." AND a.iron_date = CAST(NOW() AS DATE)";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function detail_iron_salary_filter($employee, $startdate, $enddate, $branch_) {
	    $branch = '';
        if(!empty($branch_)) {
             $branch = 'AND b.id_branch = '.$branch_;
        }  
        
        $filter = 'AND a.iron_date = CAST(NOW() AS DATE)';
        if(!empty($startdate) && !empty($enddate)) {
            if(strtotime($startdate) <= strtotime($enddate)) {
                $fstartdate = $startdate;
                $fenddate = $enddate;
                
                $filter = 'AND a.iron_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND a.iron_date = \''.$fstartdate.'\'';
                }   
            }
        }  
	    
		$sql = "SELECT SUM(b.iron_salary_emp) AS iron_salary, SUM(b.price) AS basis
				FROM invoice a
				LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
				LEFT JOIN service c ON b.id_service = c.id_service
				WHERE 1=1 ".$branch." AND a.id_iron_emp = ".$employee." ".$filter." ";
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function update_payday($employee) {
		$from = date("Y-m-d", strtotime( "previous saturday" ));
		$to = date("Y-m-d");
		
		$sql = "UPDATE employee
				SET last_paydate = NOW()
				WHERE id_employee = ".$employee;
 		$update = $this->db->query($sql);
 		
 		$sql1 = "UPDATE `order` a
				LEFT JOIN invoice b ON a.invoice_number = b.invoice_number
				SET wash_salary_status = 1
				WHERE b.id_wash_emp = ".$employee." AND b.wash_date BETWEEN '".$from."' AND '".$to."'";
 		$update1 = $this->db->query($sql1);
 		
		$sql2 = "UPDATE `order` a
				LEFT JOIN invoice b ON a.invoice_number = b.invoice_number
				SET iron_salary_status = 1
				WHERE b.id_iron_emp = ".$employee." AND b.iron_date BETWEEN '".$from."' AND '".$to."'";
 		$update2 = $this->db->query($sql2);
		
		if($update) {
			return true; 
		} else {
			return false; 
		}
	}
	
	public function list_salary_pdf() {
        $sql    = ' SELECT *
                    FROM employee
                    ORDER BY name ASC';
        
        
        $query  =  $this->db->query($sql);
        $result = $query->result_array(); 
        
        $return = array(
            'data' => $result
        );
        
        return $return;
    }
    
    public function detail_wash_salary_pdf($employee, $startdate, $enddate, $branch_) {
        // $from = date("Y-m-d", strtotime( "previous saturday" ));
        // $to = date("Y-m-d");  
        
        $branch = '';
        if(!empty($branch_)) {
             $branch = 'AND b.id_branch = '.$branch_;
        }
        
        $filter = 'AND a.wash_date = CAST(NOW() AS DATE)';
        if(!empty($startdate) && !empty($enddate)) {
            if(strtotime($startdate) <= strtotime($enddate)) {
                $fstartdate = $startdate;
                $fenddate = $enddate;
                
                $filter = 'AND a.wash_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND a.wash_date = \''.$fstartdate.'\'';
                }   
            }
        }
        
        $sql = "SELECT SUM(b.wash_salary_emp) AS wash_salary, SUM(b.price) AS basis
                FROM invoice a
                LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
                LEFT JOIN service c ON b.id_service = c.id_service
                WHERE 1=1 ".$branch." AND a.id_wash_emp = ".$employee." ".$filter."";
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
    
    public function detail_iron_salary_pdf($employee, $startdate, $enddate, $branch_) {
        // $from = date("Y-m-d", strtotime( "previous saturday" ));
        // $to = date("Y-m-d");
        
        $branch = '';
        if(!empty($branch_)) {
             $branch = 'AND b.id_branch = '.$branch_;
        }
        
        $filter = 'AND a.iron_date = CAST(NOW() AS DATE)';
        if(!empty($startdate) && !empty($enddate)) {
            if(strtotime($startdate) <= strtotime($enddate)) {
                $fstartdate = $startdate;
                $fenddate = $enddate;
                
                $filter = 'AND a.iron_date BETWEEN \''.$fstartdate.'\' AND \''.$fenddate.'\'';
                if($fstartdate == $fenddate) {
                    $filter = 'AND a.iron_date = \''.$fstartdate.'\'';
                }   
            }
        }
        
        $sql = "SELECT SUM(b.iron_salary_emp) AS iron_salary, SUM(b.price) AS basis
                FROM invoice a
                LEFT JOIN `order` b ON a.invoice_number = b.invoice_number
                LEFT JOIN service c ON b.id_service = c.id_service
                WHERE 1=1 ".$branch." AND a.id_iron_emp = ".$employee." ".$filter."";
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }

    public function getBranch() {
        $sql = "SELECT * FROM branch";
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
    
    public function getBranchReport() {
        $id_branch = $this->session->userdata('id_branch_emp'); 
        
        $branch = '';
        if($this->session->userdata('role') != 0) {
             $branch = 'WHERE id_branch = '.$id_branch;
        }
        
        $sql = "SELECT name FROM branch ". $branch;
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
    
    public function getBranchName($branch) {
        $sql = "SELECT name FROM branch WHERE id_branch = ".$branch;
        $hasil = $this->db->query($sql);
        return $hasil->result_array(); 
    }
}	
