<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');

class Report_salary extends CI_Controller {
  public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('role') != 0) { redirect('dashboard'); }
				
		$this->load->model('report_salary_model');

	}
	
	public function index()
	{
	    $branch = $this->report_salary_model->getBranch();
        $branch_report = $this->report_salary_model->getBranchReport();
		
        $options = '<option value="">All</option>';
        foreach ($branch as $list) {
            $options .= '<option value="'.$list['id_branch'].'">'.$list['name'].'</option>';
        }
        $data['branch'] = $options;
        $data['branch_report'] = count($branch_report) > 1 ? 'All' : $branch_report[0]['name'];
        
		$this->template->load('maintemplate', 'report_salary/views/report_salary_view', $data);
	}
	
	public function list_salary() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('name', 'total_salary', '');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->report_salary_model->list_salary($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			// $total_salary_wash = 0;
			// $total_salary_iron = 0;
// 			
			// $id_emp = $v['id_employee'];
			// $wash_salary = $this->report_salary_model->detail_wash_salary($id_emp);
			// $iron_salary = $this->report_salary_model->detail_iron_salary($id_emp);
// 			
			// $total_salary_wash = $wash_salary[0]['wash_salary'];
			// $total_salary_iron = $iron_salary[0]['iron_salary'];
// 			
			// $total_salary = $total_salary_wash + $total_salary_iron;
            $total_salary = $v['total_salary'];
			
			$devided_salary = $total_salary/1000;
		    $round_iron = ceil($devided_salary);
		    $ftotal_salary = $round_iron*1000;
			
			$checkbox = '<input data-id_emp="'.$v['id_employee'].'" data-name_emp="'.$v['name'].'" type="checkbox" class="items">';
			
			array_push($data, 
				array(
					//$checkbox,
					$v['name'],
					'Rp.'.number_format($ftotal_salary),
					//date('d M Y', strtotime($v['last_paydate'])),
					'<button class="btn btn-primary btn-xs" href="#" title"Detail" onclick="datail_('.$v['id_employee'].', \''.$v['name'].'\', '.floor($total_salary).')">Detail</button>'
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_detail() {
		$id = $this->input->get('id');
		
		$total_salary_wash = 0;
		$total_salary_iron = 0;
		
		$wash_salary = $this->report_salary_model->detail_wash_salary($id);
		$iron_salary = $this->report_salary_model->detail_iron_salary($id);
		
		$total_salary_wash = $wash_salary[0]['wash_salary'] == null || $wash_salary[0]['wash_salary'] == 0 || $wash_salary[0]['wash_salary'] == '' ? 0 : $wash_salary[0]['wash_salary'];
		$total_salary_iron = $iron_salary[0]['iron_salary'] == null || $iron_salary[0]['iron_salary'] == 0 || $iron_salary[0]['iron_salary'] == '' ? 0 : $iron_salary[0]['iron_salary'];
		$basis_wash = $wash_salary[0]['basis'] == null || $wash_salary[0]['basis'] == 0 || $wash_salary[0]['basis'] == '' ? 0 : $wash_salary[0]['basis'];
		$basis_iron = $iron_salary[0]['basis'] == null || $iron_salary[0]['basis'] == 0 || $iron_salary[0]['basis'] == '' ? 0 : $iron_salary[0]['basis'];
		
		
		
		$data = array(
			'wash_salary' => $total_salary_wash,
			'iron_salary' => $total_salary_iron,
			'wash_basis' => $basis_wash,
			'iron_basis' => $basis_iron
		);
		
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function get_detail_filter() {
		$id = $this->input->get('id');
		$start = $this->input->get('start');
		$end = $this->input->get('end');
        $branch = $this->input->get('branch');
		
		$total_salary_wash = 0;
		$total_salary_iron = 0;
		
		$wash_salary = $this->report_salary_model->detail_wash_salary_filter($id, $start, $end, $branch);
		$iron_salary = $this->report_salary_model->detail_iron_salary_filter($id, $start, $end, $branch);
		
		$total_salary_wash = $wash_salary[0]['wash_salary'] == null || $wash_salary[0]['wash_salary'] == 0 || $wash_salary[0]['wash_salary'] == '' ? 0 : $wash_salary[0]['wash_salary'];
		$total_salary_iron = $iron_salary[0]['iron_salary'] == null || $iron_salary[0]['iron_salary'] == 0 || $iron_salary[0]['iron_salary'] == '' ? 0 : $iron_salary[0]['iron_salary'];
		$basis_wash = $wash_salary[0]['basis'] == null || $wash_salary[0]['basis'] == 0 || $wash_salary[0]['basis'] == '' ? 0 : $wash_salary[0]['basis'];
		$basis_iron = $iron_salary[0]['basis'] == null || $iron_salary[0]['basis'] == 0 || $iron_salary[0]['basis'] == '' ? 0 : $iron_salary[0]['basis'];
		
		
		
		$data = array(
			'wash_salary' => $total_salary_wash,
			'iron_salary' => $total_salary_iron,
			'wash_basis' => $basis_wash,
			'iron_basis' => $basis_iron
		);
		
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function list_salary_filter() {
		$startdate = $this->input->get('startdate') != '' ? date('Y-m-d', strtotime($this->input->get('startdate'))) : '' ;
		$enddate = $this->input->get('enddate') != '' ? date('Y-m-d', strtotime($this->input->get('enddate'))) : '' ;
        $branch = $this->input->get('branch');
		
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('name', 'total_salary', '');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
        $params['start']        = $startdate;
        $params['end']          = $enddate;
        $params['branch']       = $branch;
        
		$list = $this->report_salary_model->list_salary_filter($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			// $total_salary_wash = 0;
			// $total_salary_iron = 0;
// 			
			// $id_emp = $v['id_employee'];
			// $wash_salary = $this->report_salary_model->detail_wash_salary_filter($id_emp, $startdate, $enddate, $branch);
			// $iron_salary = $this->report_salary_model->detail_iron_salary_filter($id_emp, $startdate, $enddate, $branch);
// 			
			// $total_salary_wash = $wash_salary[0]['wash_salary'];
			// $total_salary_iron = $iron_salary[0]['iron_salary'];
// 			
			// $total_salary = $total_salary_wash + $total_salary_iron;
            $total_salary = $v['total_salary'];
			
			$devided_salary = $total_salary/1000;
		    $round_iron = ceil($devided_salary);
		    $ftotal_salary = $round_iron*1000;
			
			$checkbox = '<input data-id_emp="'.$v['id_employee'].'" data-name_emp="'.$v['name'].'" type="checkbox" class="items" disabled="disabled">';

			array_push($data, 
				array(
					//$checkbox,
					$v['name'],
					'Rp.'.number_format($ftotal_salary),
					//date('d M Y', strtotime($v['last_paydate'])),
					'<button class="btn btn-primary btn-xs" href="#" title"Detail" onclick="datail_filter_('.$v['id_employee'].', \''.$v['name'].'\', '.floor($total_salary).', \''.$startdate.'\', \''.$enddate.'\', '.$branch.')">Detail</button>'
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function update_payday() {
        $employee = $this->input->post('employee');
		
		$do_update = $this->report_salary_model->update_payday($employee);
        if ($do_update) {
        	$result = array(
				'success' => true
			);
        } else {
            $result = array(
				'success' => false
			);
        }
			
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    public function list_salary_pdf() {
        $startdate = $this->input->get('startdate') != '' || $this->input->get('startdate') != null ? date('Y-m-d', strtotime($this->input->get('startdate'))) : '' ;
        $enddate = $this->input->get('enddate') != '' || $this->input->get('enddate') != null ? date('Y-m-d', strtotime($this->input->get('enddate'))) : '' ;
        $branch = $this->input->get('branch');
        
        $list = $this->report_salary_model->list_salary_pdf();
        
        $data = array();            
        foreach ( $list['data'] as $k => $v ) {
            $total_salary_wash = 0;
            $total_salary_iron = 0;
            
            $id_emp = $v['id_employee'];
            $wash_salary = $this->report_salary_model->detail_wash_salary_pdf($id_emp, $startdate, $enddate, $branch);
            $iron_salary = $this->report_salary_model->detail_iron_salary_pdf($id_emp, $startdate, $enddate, $branch);
            
            $total_salary_wash = $wash_salary[0]['wash_salary'];
            $total_salary_iron = $iron_salary[0]['iron_salary'];
            
            $total_salary = $total_salary_wash + $total_salary_iron;
            
            $devided_salary = $total_salary/1000;
            $round_iron = ceil($devided_salary);
            $ftotal_salary = $round_iron*1000;
            
            array_push($data, 
                array(
                    $v['name'],
                    number_format($ftotal_salary),
                    date('d M Y', strtotime($v['last_paydate']))
                )
            );
        }
        
        $result["data"] = $data;
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function data_filter() {
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $branch = $this->input->get('branch');
        
        $branch_name = 'All';
        
        $id_branch = $this->session->userdata('id_branch_emp'); 
        if($this->session->userdata('role') != 0) {
             $branch = $id_branch;
        }
        
        if($branch != '') {
            $get_branch_name = $this->report_salary_model->getBranchName($branch);  
            $branch_name = $get_branch_name[0]['name']; 
        }
        
        $result = array(
            'start' => date("d M Y", strtotime($startdate)),
            'end' => date("d M Y", strtotime($enddate)),
            'branch' => $branch_name
        );
        
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}
