<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->model('login_model');	
		
		if ($this->session->userdata('id_user')) {
                redirect('dashboard');
        }
	}
	
	public function index() {
		
		$data['branch'] = $this->login_model->getBranch();
		$this->load->view('login_view', $data);
		
	}

}
