<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends MX_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->model('log_model');	
	}
	
	public function set_activity() {
		$string = $this->input->post('string');
		$user = $this->input->post('user');
        
        if($this->session->userdata('id_user') != 999999) {
            $do_submit = $this->log_model->add($user, strtoupper($string));
            if ($do_submit) {
                $result = array(
                    'success' => true,
                    'message' => 'Activity added!'
                );
            } else {
                $result = array(
                    'success' => false,
                    'message' => 'Failed to added new activity!'
                );
            }     
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

}
