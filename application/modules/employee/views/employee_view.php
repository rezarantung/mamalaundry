<script>
	var table

	$(document).ready(function(){
		table = $('#employee_list').DataTable({
			"processing": true,
			"serverSide": true,
	        "ajax": {
	        	'type' : 'get',
	        	'url': 'employee/list_employee'     
	        },
	        "columnDefs": [{
				"targets": [2,3],
				"orderable": false
			}]
	    });

	});
	
	function submit() {
		var r = confirm("Are you sure want to submit?","Ok","Cancel");
		if(r){
			var name = $('#name').val();
			var occup = $('#occupation').val();
			var phone = $('#phone').val();
			
			$.ajax({
				type: 'post',
				url: "employee/add_employee", 
				data: {
					'name': name.toUpperCase(),
					'occupation': occup.toUpperCase(),
					'phone': phone.toUpperCase()
				}, 
				success: function(response){
		        	if(response.success) {
		        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Create Employee : ' + '"' + name + '"');
		        		
		        		$('#name').val('');
						$('#occupation').val('');
						$('#phone').val('');
						
		        		$('#addEmployee').modal('toggle');
		        		table.ajax.reload();
		        		$("#alert_employee_list").html(response.message);
						$("#alert_employee_list").show();
						setTimeout(function() { $("#alert_employee_list").slideUp(); }, 4000);
		        	} else {
		        		$("#modal_message").html(response.message);
						$("#modal_message").show();
						setTimeout(function() { $("#modal_message").slideUp(); }, 4000);
		        	}
		    	}
		    });	
		}
	}
	
	function delete_(id, employee) {
		var r = confirm("Are you sure want to delete \""+employee+"\"?","Ok","Cancel");
			if(r){
				$.ajax({
					type: 'post',
					url: "employee/delete_employee", 
					data: { 'id': id }, 
					success: function(response){
			        	if(response.success) {
			        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Delete Employee : ' + '"' + employee + '"');
			        		table.ajax.reload();
			        		$("#alert_employee_list").html(response.message);
							$("#alert_employee_list").show();
							setTimeout(function() { $("#alert_employee_list").slideUp(); }, 4000);
			        	} else {
			        		table.ajax.reload();
			        		$("#danger_employee_list").html(response.message);
							$("#danger_employee_list").show();
							setTimeout(function() { $("#danger_employee_list").slideUp(); }, 4000);
			        	}
			    	}
			    });
			}
	}	
	
	function edit_(id) {
		$('#editEmployee').modal('toggle');
		$.ajax({
			type: 'post',
			url: "employee/get_employee", 
			data: { 'id': id }, 
			success: function(response){
	        	if(response.success) {
	        		$('#edit_name').val(response.data[0].name);
					$('#edit_occupation').val(response.data[0].occupation);
					$('#edit_phone').val(response.data[0].phone_number);
					$('#edit_wash_salary').val(response.data[0].wash_salary);
					$('#edit_iron_salary').val(response.data[0].iron_salary);
					$('#edit_id_employee').val(id);
	        	} else {
	        		console.log(response.message);
	        	}
	    	}
	    });
	}	
	
	function update() {
		var r = confirm("Are you sure want to update?","Ok","Cancel");
		if(r){
			var name = $('#edit_name').val();
			var occup = $('#edit_occupation').val();
			var phone = $('#edit_phone').val();
			var id = $('#edit_id_employee').val();
			
			$.ajax({
				type: 'post',
				url: "employee/update_employee", 
				data: {
					'id': id,
					'name': name.toUpperCase(),
					'occupation': occup.toUpperCase(),
					'phone': phone.toUpperCase()
				}, 
				success: function(response){
		        	if(response.success) {
		        		logActivity(<?php echo $this->session->userdata('id_user'); ?> ,'Update Employee : ' + '"' + name + '"');
		        		
		        		$('#edit_name').val('');
						$('#edit_occupation').val('');
						$('#edit_phone').val('');
		        		
		        		$('#editEmployee').modal('toggle');
		        		table.ajax.reload();
		        		$("#alert_employee_list").html(response.message);
						$("#alert_employee_list").show();
						setTimeout(function() { $("#alert_employee_list").slideUp(); }, 4000);
		        	} else {
		        		$("#edit_modal_message").html(response.message);
						$("#edit_modal_message").show();
						setTimeout(function() { $("#edit_modal_message").slideUp(); }, 4000);
		        	}
		    	}
		    });	
		}
	}

</script>
<style type="text/css">
    input[type=text], input[type=tel], input[type=hidden], textarea {
        text-transform: uppercase !important;
    }
    
    .input-group-addon {
        min-width: 150px;
        text-align: left;
        font-weight: lighter;
    }
</style>
<div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Employee</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Employee List</h2>
				<div class="pull-right">
					<a class="btn btn-primary btn-xs" href="#" data-toggle="modal" data-target="#addEmployee"><i class="glyphicon glyphicon-plus" data-toggle="tooltip" title="" data-original-title="Add"></i> Add</a>
                </div>
            </div>
            <div id="alert_employee_list" class="alert alert-success" style="display: none; text-align: center;"></div>
            <div id="danger_employee_list" class="alert alert-danger" style="display: none; text-align: center;"></div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12">
                    <table id="employee_list" width="100%" class="table table-striped">
                        <thead>
                        	<tr>
                        		<th>NAME</th>
                        		<th>OCCUPATION</th>
                        		<th>PHONE NUMBER</th>
                        		<th>ACTION</th>
                    		</tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->

	<div class="modal fade" id="addEmployee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">×</button>
	                    <h3>Add Employee</h3>
	                </div>
	                <div class="modal-body">
	                    <form class="form-horizontal" action="" method="post">
                    		<div id='modal_message' class="alert alert-danger" style="display: none;"></div>
                    		
                    		<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="name">NAME</label></span>
		                        <input name="name" id="name" type="text" class="form-control" placeholder="Name">
		                    </div>
		                    <div class="clearfix"></div><br>
		
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="occupation">OCCUPATION</label></span>
		                        <input name="occupation" id="occupation" type="text" class="form-control" placeholder="Occupation">
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="phone">PHONE</label></span>
		                        <input name="phone" id="phone" type="tel" class="form-control" placeholder="Phone number">
		                    </div>
		                    <div class="clearfix"></div>
		           	 	</form>
	                </div>
	                <div class="modal-footer">
	                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
	                    <a href="#" class="btn btn-primary" onclick="submit();">SAVE</a>
	                </div>
	            </div>
	        </div>
	    </div>
	    
	    <div class="modal fade" id="editEmployee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">×</button>
	                    <h3>Edit Employee</h3>
	                </div>
	                <div class="modal-body">
	                    <form class="form-horizontal" action="" method="post">
                    		<div id='edit_modal_message' class="alert alert-danger" style="display: none;"></div>
                    		
                    		<div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_name">NAME</label></span>
		                        <input name="edit_name" id="edit_name" type="text" class="form-control" placeholder="Name">
		                    </div>
		                    <div class="clearfix"></div><br>
		
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_occupation">OCCUPATION</label></span>
		                        <input name="edit_occupation" id="edit_occupation" type="text" class="form-control" placeholder="Occupation">
		                    </div>
		                    <div class="clearfix"></div><br>
		                    
		                    <div class="input-group input-group-md">
		                        <span class="input-group-addon"><label for="edit_phone">PHONE</label></span>
		                        <input name="edit_phone" id="edit_phone" type="tel" class="form-control" placeholder="Phone number">
		                    </div>
		                    <div class="clearfix"></div>
							<input name="edit_id_employee" id="edit_id_employee" type="hidden" >
		           	 	</form>
	                </div>
	                <div class="modal-footer">
	                    <a href="#" class="btn btn-default" data-dismiss="modal">CLOSE</a>
	                    <a href="#" class="btn btn-primary" onclick="update();">UPDATE</a>
	                </div>
	            </div>
	        </div>
	    </div>
    