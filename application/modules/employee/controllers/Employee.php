<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {
  public function __construct()
	{
		parent::__construct();	
		if($this->session->userdata('role') != 0) { redirect('dashboard'); }		
		$this->load->model('employee_model');

	}
	
	public function index()
	{
		$this->template->load('maintemplate', 'employee/views/employee_view');
	}
	
	public function get_employee() {
		$id = $this->input->post('id');
		$data = $this->employee_model->getEmployee($id);
		if($data) {
			$result = array(
				'success' => true,
				'data' => $data
			);
		} else {
			$result = array(
				'success' => false,
				'message' => 'Failed!'
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function list_employee() {
		if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 		
		
		$order = $this->input->get_post('order');
		if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'asc';}; 
		if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 
		$order_fields = array('name', 'occupation');
		
		$search = $this->input->get_post('search');
		
		if( ! empty($search['value']) ) {
			$search_value = $search['value'];
		} else {
			$search_value = null;
		}
		
		// Build params for calling model 
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['filter'] 		= $search_value;
		
		$list = $this->employee_model->list_employee($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();			
		foreach ( $list['data'] as $k => $v ) {
			array_push($data, 
				array(
					$v['name'],
					$v['occupation'],
					$v['phone_number'],
					'<a class="btn btn-primary btn-xs" href="#" title"Edit" onclick="edit_('.$v['id_employee'].')"><i class="glyphicon glyphicon-edit"></i></a>'
					.' '.
					'<a class="btn btn-danger btn-xs" href="#" title"Delete" onclick="delete_('.$v['id_employee'].', \''.$v['name'].'\')"><i class="glyphicon glyphicon-trash"></i></a>'
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_employee() {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('occupation', 'Occupation', 'required');
        $this->form_validation->set_rules('phone', 'Phone number', 'required|numeric');
		
        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $name = $this->input->post('name');
            $occupation = $this->input->post('occupation');
            $phone = $this->input->post('phone');
            
			$exist = $this->employee_model->check_existing($name);
			if($exist['total'] != 0) {
				$result = array(
					'success' => false,
					'message' => $name.' is already exist!'
				);
			} else {
				$do_submit = $this->employee_model->add($name, $occupation, $phone);
	            if ($do_submit) {
	            	$result = array(
						'success' => true,
						'message' => 'Employee added!'
					);
	            } else {
	                $result = array(
						'success' => false,
						'message' => 'Failed to add new employee!'
					);
	            }
			}
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function update_employee() {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('occupation', 'Occupation', 'required');
        $this->form_validation->set_rules('phone', 'Phone number', 'required|numeric');

        if ($this->form_validation->run() == FALSE)  {
        	$result = array(
				'success' => false,
				'message' => validation_errors()
			);
        } else {
            $name = $this->input->post('name');
            $occupation = $this->input->post('occupation');
            $phone = $this->input->post('phone');
            $id = $this->input->post('id');
			
			$do_update = $this->employee_model->update($name, $occupation, $phone, $id);
            if ($do_update) {
            	$result = array(
					'success' => true,
					'message' => 'Employee updated!'
				);
            } else {
                $result = array(
					'success' => false,
					'message' => 'Failed to update employee!'
				);
            }
        }
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	public function delete_employee() {
        
        $id = $this->input->post('id');
        
        $do_delete = $this->employee_model->delete($id);
        if ($do_delete) {
        	$result = array(
				'success' => true,
				'message' => 'Employee deleted!'
			);
        } else {
            $result = array(
				'success' => false,
				'message' => 'Failed to delete employee!'
			);
        }
        
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
}
