<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Employee_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function check_existing($name) { 
		$branch = $this->session->userdata('id_branch');	
		$sql 	= '	SELECT COUNT(*) AS total
					FROM employee
					WHERE name = \''. $name .'\' AND id_branch ='. $branch;
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->row_array(); 
		
		$return = array(
			'total' => $result['total'],
		);
		
		return $return;
	}
	
	public function getEmployee($id) {
		$sql = "SELECT *
				FROM employee
				WHERE id_employee = ".$id;
 		$hasil = $this->db->query($sql);
		return $hasil->result_array(); 
	}
	
	public function list_employee($params = array()) { 
		$branch = $this->session->userdata('id_branch');	
		$where = '';
		if(!empty($params['filter'])) {
			$where = 'AND (name LIKE \'%'.$params['filter'].'%\' OR occupation LIKE \'%'.$params['filter'].'%\' OR phone_number LIKE \'%'.$params['filter'].'%\')';
		}
		
		$sql 	= '	SELECT *
					FROM employee
					WHERE 1=1 '.$where.'
					ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
					LIMIT '.$params['limit'].'
					OFFSET ' . $params['offset'];
		
		
		$query 	=  $this->db->query($sql);
		$result = $query->result_array(); 
		
		$total = $this->db->query('SELECT COUNT(*) AS total
									FROM employee
									WHERE 1=1 '.$where)->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['total'],
			'total' => $total['total'],
		);
		
		return $return;
	}
	
	public function add($name, $occupation, $phone) {
	    $branch = $this->session->userdata('id_branch');
		$sql = "INSERT INTO employee (id_branch, name, occupation, phone_number)
				VALUES (".$branch.", '".$name."', '".$occupation."', '".$phone."')";
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}	
	
	public function update($name, $occupation, $phone, $id) {
		$sql = "UPDATE employee
				SET name = '".$name."', occupation = '".$occupation."', phone_number = '".$phone."'   
				WHERE id_employee = ".$id;
 		$add = $this->db->query($sql);
		if($add) {
			return true; 
		} else {
			return false; 
		}
	}
	
	public function delete($id) {
		$sql = "DELETE FROM employee WHERE id_employee = ". $id;
 		$delete = $this->db->query($sql);
		if($delete) {
			return true; 
		} else {
			return false; 
		}
	}

}	
